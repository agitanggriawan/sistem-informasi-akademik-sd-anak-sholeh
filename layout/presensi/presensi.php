<div class="page-titles">
  <div class="d-flex align-items-center">
    <h5 class="font-medium m-b-0">Presensi</h5>
  </div>
</div>
<div class="container-fluid">
  <?php 
  if ($md->isPresence(base64_decode($_SESSION['id']), base64_decode($_SESSION['kelas']))) { 
  ?>
  <div class="row">
    <div class="col s12">
      <div class="card">
        <div class="card-content">
          <div hidden id="idGuru"><?php echo base64_decode($_SESSION['id']) ?></div>
          <div hidden id="idKelas"><?php echo base64_decode($_SESSION['kelas']) ?></div>
          <p>
            Nama Kelas: <b><?php echo $md->namaKelas(base64_decode($_SESSION['kelas'])) ?></b> | Nama Guru: <b><?php echo base64_decode($_SESSION['nama']) ?></b> | Tanggal: <b><?php echo date('d - m - Y') ?></b>
          </p>
          <table id="table-presensi" class="responsive-table highlight" style="width:100%">
            <thead>
              <th>Nomor</th>
              <th>Nomor Induk</th>
              <th>Nama Siswa</th>
              <th width="300px">Aksi</th>
            </thead>
          </table>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="input-field col s1">
      <button class="btn blue waves-effect waves-light right" type="button" id="btn-presensi" onclick="savePresensi()">Simpan</button>
    </div>
  </div>
  <?php
  } else {
  ?>
  <div class="row">
    <div class="col s12">
      <div class="card">
        <div class="card-content">
          <p>
            Nama Kelas: <b><?php echo $md->namaKelas(base64_decode($_SESSION['kelas'])) ?></b> | Nama Guru: <b><?php echo base64_decode($_SESSION['nama']) ?></b> | Tanggal: <b><?php echo date('d - m - Y') ?></b>
          </p>
          <center style="margin-top:35px">
            <h2>Guru kelas <b><?php echo $md->namaKelas(base64_decode($_SESSION['kelas'])) ?></b> sudah melakukan presensi</h2>
          </center>
        </div>
      </div>
    </div>
  </div>
  <?php
  }
  ?>
</div>