<div class="page-titles">
  <div class="d-flex align-items-center">
    <h5 class="font-medium m-b-0">Master Presensi</h5>
  </div>
</div>
<div class="container-fluid">
  <?php
    if (isset($_GET['k']) && isset($_GET['d'])){
    ?>
    <div class="row"><div class="row">
      <div class="col s12">
        <div class="card">
          <div class="card-content">
            <p>
              Nama Kelas: <b><?php echo $md->namaKelas(base64_decode($_SESSION['kelas'])) ?></b> | Nama Guru: <b><?php echo base64_decode($_SESSION['nama']) ?></b> | Tanggal: <b><?php echo $_GET['d'] ?></b>
            </p>
            <table class="responsive-table highlight" style="width:100%">
              <thead>
                <tr>
                  <th>No.</th>
                  <th>Nomor Induk</th>
                  <th>Nama Siswa</th>
                  <th>Status</th>
                </tr>
              </thead>
              <tbody>
              <?php
                include_once './config/config.php';
                $kelas = $_GET['k'];
                $date = $_GET['d'];

                $query = mysqli_query($connect, "SELECT nomor_induk, nama_siswa FROM tb_siswa WHERE id_kelas = $kelas ");
                $i = 0;
                while($data = mysqli_fetch_array($query)){
                  $qPresence = mysqli_query($connect, "SELECT value from tb_presensi WHERE JSON_CONTAINS(value, '{\"id\": \"$data[0]\"}') AND DATE(tgl) = ('$date')");
                  $result = mysqli_fetch_array($qPresence);
              ?>
                <tr>
                  <td><?php echo $i + 1 . '.'?></td>
                  <td><?php echo $data['nomor_induk']?></td>
                  <td><?php echo $data['nama_siswa']?></td>
                  <td><?php echo json_decode($result[0])->status?></td>
                </tr>
              <?php
              $i++;
                }
              ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="input-field col s1">
        <a href="main?module=master-presensi" class="btn grey waves-effect waves-light right" >Kembali</a>
      </div>
    </div>
    <?php
    } else {
    ?>
    <div class="row">
      <div class="col s12">
        <ul class="tabs">
          <li class="tab col s6"><a href="#today" class="active">Presensi Hari Ini</a></li>
          <li class="tab col s6"><a href="#date">Presensi Berdasarkan Tanggal</a></li>
        </ul>
      </div>
      <div id="today" class="col s12">
        <div class="row">
          <div class="col s12">
            <div class="card">
              <div class="card-content">
              <p>
                Nama Kelas: <b><?php echo $md->namaKelas(base64_decode($_SESSION['kelas'])) ?></b> | Nama Guru: <b><?php echo base64_decode($_SESSION['nama']) ?></b> | Tanggal: <b><?php echo date('d - m - Y') ?></b>
              </p>
              <table id="table-current-presensi" class="bordered highlight" width="100%">
                <thead>
                  <tr>
                    <th>No.</th>
                    <th>Nomor Induk</th>
                    <th>Nama Siswa</th>
                    <th>Status</th>
                  </tr>
                </thead>
              </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div id="date" class="col s12">
        
        <div class="row">
          <div class="col s12">
            <div class="card">
              <div class="card-content">
              <table id="table-date-presensi" class="bordered highlight" width="100%">
                <thead>
                  <tr>
                    <th>No.</th>
                    <th>Tanggal</th>
                    <th>Kelas</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
              </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php
    }
    ?>
</div>