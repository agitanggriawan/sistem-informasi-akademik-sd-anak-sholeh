<div class="page-titles">
  <div class="d-flex align-items-center">
    <h5 class="font-medium m-b-0">Form Periode Tahun Ajaran</h5>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col s12">
    <ul class="tabs">
      <li class="tab col s6"><a href="#input" class="active">Input Data</a></li>
      <li class="tab col s6"><a href="#list">List Data</a></li>
    </ul>
  </div>
  <div id="input" class="col s12">
    <div class="row">
      <div class="col s12">
        <div class="card">
          <div class="card-content">
            <form class="formValidate" id="validateTahunAjaran">
              <div class="row">
                <div class="input-field col s12 m6 l6">
                  <input id="namaTahunAjaran" name="namaTahunAjaran" type="text" placeholder="Nama Tahun Ajaran" autofocus>
                </div>
                <div class="input-field col s12 m3 l3">
                  <input id="awalTahunAjaran" name="awalTahunAjaran" type="text" class="datepicker" placeholder="Tanggal Periode Awal">
                </div>
                <div class="input-field col s12 m3 l3">
                  <input id="akhirTahunAjaran" name="akhirTahunAjaran" type="text" class="datepicker" placeholder="Tanggal Periode Akhir">
                </div>
                <div class="input-field col s12">
                  <button class="waves-effect waves-light btn btn-round blue
                  btn-simpan" type="submit">Simpan</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="list" class="col s12">
    <div class="row">
      <div class="col s12">
        <div class="card">
          <div class="card-content">
          <table id="table-get-tahun-ajaran" class="bordered highlight" width="100%">
            <thead>
              <tr>
                <th>No.</th>
                <th>Nama Periode</th>
                <th>Periode Awal</th>
                <th>Periode Akhir</th>
                <th>Status</th>
                <th>Aksi</th>
              </tr>
            </thead>
          </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</div>