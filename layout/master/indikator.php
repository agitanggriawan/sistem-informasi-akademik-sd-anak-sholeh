<div class="page-titles">
  <div class="d-flex align-items-center">
    <h5 class="font-medium m-b-0">Form Indikator</h5>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col s12">
    <ul class="tabs">
      <li class="tab col s6"><a href="#input" class="active">Input Data</a></li>
      <li class="tab col s6"><a href="#list">List Data</a></li>
    </ul>
  </div>
  <div id="input" class="col s12">
    <div class="row">
      <div class="col s12">
        <div class="card">
          <div class="card-content">
            <form class="formValidate" id="validateIndikator">
              <div class="row">
              <div class="input-field col s12 m6 l6">
                  <select name="namaKarakter" id="nama-karakter" required>
                    <option value="" disabled selected>Nama Karakter</option>
                    <?php echo $md->getKarakter(); ?>
                  </select>
                  <label>Nama Karakter</label>
                </div>
                <div class="input-field col s12 m6 l6">
                  <input id="nama-indikator" name="namaIndikator" type="text" placeholder="Nama Indikator" autofocus>
                </div>
                <div class="input-field col s12">
                  <button class="waves-effect waves-light btn btn-round blue 
                  btn-simpan" type="submit">Simpan</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="list" class="col s12">
    <div class="row">
      <div class="col s12">
        <div class="card">
          <div class="card-content">
          <table id="table-get-indikator" class="bordered highlight" width="100%">
            <thead>
              <tr>
                <th>No.</th>
                <th>Nama Karakter</th>
                <th>Nama Indikator</th>
              </tr>
            </thead>
          </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</div>