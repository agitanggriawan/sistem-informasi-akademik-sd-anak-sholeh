<div class="page-titles">
  <div class="d-flex align-items-center">
    <h5 class="font-medium m-b-0">Master Nilai Karakter Disiplin</h5>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col s12">
    </div>
    <div class="col s12">
      <div class="row">
        <div class="col s12">
          <div class="card">
            <div class="card-content">
            <div style="padding-left: 0px !important; width: 200px; top: 66px; left: 25px; position: absolute;">
              <select id="getNilaiDisiplinKepsek">
                <option value="" disabled selected>Kelas</option>
                <?php echo $md->getClass() ?>
              </select>
              <label style="left: 0px !important">Kelas</label>
            </div>
            <table id="table-master-nilai-displin-kepsek" class="responsive-table highlight" style="width:100%">
              <thead>
                <tr>
                  <th>No.</th>
                  <th>Nomor Induk Siswa</th>
                  <th>Nama Siswa</th>
                  <th>Total Nilai</th>
                  <th>Aksi</th>
                </tr>
              </thead>
            </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="myModal" class="modal"></div>