<div class="page-titles">
  <div class="d-flex align-items-center">
  <h5 class="font-medium m-b-0">Penilaian Poin</h5>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col s12">
      <div class="card">
        <div class="card-content">
            <table id="input-nilai-point" class="responsive-table display" style="width:100%">
              <thead>
                <tr>
                  <th>No</th>
                  <th>NIS</th>
                  <th>Nama</th>
                  <th>Kelas</th>
                  <th>Nilai 1</th>
                  <th>Nilai 2</th>
                  <th>Nilai 3</th>
                  <th>Aksi</th>
                </tr>
              </thead>
            </table>
        </div>
      </div>
    </div>
  </div>
  <div hidden>
    <input type="text" id="idGuru" value="<?php echo base64_decode($_SESSION['id']) ?>" />
    <input type="text" id="idTahunAjaran" value="<?php echo base64_decode($_SESSION['tahunAjaran']) ?>" />
    <input type="text" id="idKelas" value="<?php echo base64_decode($_SESSION['kelas']) ?>" />
  </div>
</div>
