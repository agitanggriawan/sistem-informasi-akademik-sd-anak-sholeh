<div class="page-titles">
  <div class="d-flex align-items-center">
    <h5 class="font-medium m-b-0">Penilaian Karakter Disiplin</h5>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col s12">
      <div class="card">
        <!-- For testing input nilai -->
        <?php if ($md->isInput(base64_decode($_SESSION['id']), 1) == true) { ?>
        <div class="card-content">
          <i class="material-icons right tooltipped" data-position="left" data-delay="50" data-tooltip="Jika nilai dari masing-masing indikator tidak diisi, maka secara otomatis sistem akan menyimpan sebagai 'Tidak'">info_outline</i>
          <p>
            Nama Kelas: <b><?php echo $md->namaKelas(base64_decode($_SESSION['kelas'])) ?></b> | Nama Guru: <b><?php echo base64_decode($_SESSION['nama']) ?></b> | Tanggal: <b><?php echo date('d - m - Y') ?></b>
          </p>
          <div class="row">
            <table class="responsive-table highlight" style="width:100%">
              <thead>
                <tr>
                  <td class="styleTable" rowspan="2" width="50px">No</td>
                  <td class="styleTable" rowspan="2" width="300px">Nama</td>
                  <td class="styleTable" colspan="<?php echo $md->getCountIndikator(1) ?>">Indikator</td>
                </tr>
                <tr>
                  <?php echo $md->getIndikator(1) ?>
                </tr>
              </thead>
              <tbody>
                <?php
                  $totalSiswa = $md->jumlahSiswa(base64_decode($_SESSION['kelas']));
                  $siswaKelas = $md->siswaKelas(base64_decode($_SESSION['kelas']));
                  for($i = 1; $i <= $totalSiswa; $i++) {
                ?>
                <tr class="styleTable" id="siswa<?php echo $i?>">
                  <td><?php echo $i.'.'; ?></td>
                  <td id="namaSiswa<?php echo $i ?>"><?php echo $siswaKelas[$i-1] ?></td>
                  <?php
                  for ($j=0; $j < $md->getCountIndikator(1); $j++) {
                  ?>
                  <td>
                    <select class="nilaiKarakter" required width="25px">
                      <option value="" disabled selected>Pilih</option>
                      <option value="Ya">Ya</option>
                      <option value="Tidak">Tidak</option>
                    </select>
                  </td>
                  <?php
                  }
                  ?>
                </tr>
                <?php
                }
                ?>
              </tbody>
            </table>
          </div>
          <div hidden id="totalSiswa"><?php echo $totalSiswa ?></div>
          <div hidden id="idGuru"><?php echo base64_decode($_SESSION['id']) ?></div>
          <div hidden id="idKelas"><?php echo base64_decode($_SESSION['kelas']) ?></div>
          <div hidden id="idTahunAjaran"><?php echo base64_decode($_SESSION['tahunAjaran']) ?></div>
          <div hidden id="idKarakter">1</div>
          <div class="row">
            <div class="input-field col s1">
              <button class="btn blue waves-effect waves-light right" type="button" onclick="saveNilaiKarakter()">Simpan</button>
            </div>
          </div>
        </div>
        <?php } else { ?>
        <div class="card-content">
          <p>
            Nama Kelas: <b><?php echo $md->namaKelas(base64_decode($_SESSION['kelas'])) ?></b> | Nama Guru: <b><?php echo base64_decode($_SESSION['nama']) ?></b> | Tanggal: <b><?php echo date('d - m - Y') ?></b> | Status: <b> Sudah Input Nilai Disiplin <i class="material-icons">check</i></b>
          </p>
          <table class="responsive-table highlight" style="width:100%">
            <thead>
              <tr>
                <td class="styleTable" rowspan="2" width="50px">No</td>
                <td class="styleTable" rowspan="2" width="300px">Nama</td>
                <td class="styleTable" colspan="<?php echo $md->getCountIndikator(1) + 1 ?>">Indikator</td>
              </tr>
              <tr>
              <?php echo $md->getIndikator(1) ?>
                <td width="90px">Total Nilai</td>
              </tr>
            </thead>
            <tbody>
              <?php
                $query = $md->currentNilai(base64_decode($_SESSION['kelas']), base64_decode($_SESSION['id']), 1);
                $i = 1;
                while($result = mysqli_fetch_array($query)){
                  $total = 0;
                  foreach (json_decode($result['nilai']) as $el) {
                    echo '
                    <tr>
                      <td>'.$i.'.</td>
                      <td>'.$el->id_siswa.'</td>
                    ';
                    foreach($el->nilai as $elChild) {
                      $elChild->nilai === 'Ya' ? $total = $total + 1 : null;
                      echo '<td>'. $elChild->nilai . '</td>';
                    }
                    echo '<td>'. $total .'</td>';
                    echo '
                    </tr>
                    ';
                    $i++;
                  }
                }
              ?>
              </tbody>
          </table>
        </div>
        <?php } ?>
      </div>
    </div>
  </div>
</div>

<!--
const itemA = $('#siswa1').find('.nilaiKarakter') ==> Map Class
$(itemA[1]).val() ==> Get Value

var siswa = $('#siswa1').find('#namaSiswa1');
siswa[0].textContent
-->