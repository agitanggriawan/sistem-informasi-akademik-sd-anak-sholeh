<div class="page-titles">
  <div class="d-flex align-items-center">
    <h5 class="font-medium m-b-0">Master Nilai Karakter Disiplin</h5>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col s12">
      <ul class="tabs">
        <li class="tab col s6"><a href="#date" class="active">Per Tanggal</a></li>
        <li class="tab col s6"><a href="#siswa">Per Siswa</a></li>
      </ul>
    </div>
    <div id="date" class="col s12">
      <?php
      if (isset($_GET['k']) && isset($_GET['d']) && isset($_GET['n'])){
      ?>
      <div class="row">
        <div class="col s12">
          <div class="card">
            <div class="card-content">
              <table class="responsive-table highlight" style="width:100%">
                <thead>
                  <tr>
                    <td class="styleTable" rowspan="2" width="50px">No</td>
                    <td class="styleTable" rowspan="2" width="300px">Nama</td>
                    <td class="styleTable" colspan="11">Indikator</td>
                  </tr>
                  <tr>
                    <td width="90px">
                      <a class="tooltipped" data-position="top" data-delay="50" data-tooltip="Datang ke sekolah tepat dan masuk kelas pada waktunya.">A</a>
                    </td>
                    <td width="90px">
                      <a class="tooltipped" data-position="top" data-delay="50" data-tooltip="Melaksanakan tugas-tugas kelas yang menjadi tanggung jawabnya.">B</a>
                    </td>
                    <td width="90px">
                      <a class="tooltipped" data-position="top" data-delay="50" data-tooltip="Duduk pada tempat yang telah ditetapkan.">C</a>
                    </td>
                    <td width="90px">
                      <a class="tooltipped" data-position="top" data-delay="50" data-tooltip="Menaati peraturan sekolah dan kelas.">D</a>
                    </td>
                    <td width="90px">
                      <a class="tooltipped" data-position="top" data-delay="50" data-tooltip="Berpakaian sopan dan rapi.">E</a>
                    </td>
                    <td width="90px">
                      <a class="tooltipped" data-position="top" data-delay="50" data-tooltip="Mematuhi aturan permainan.">F</a>
                    </td>
                    <td width="90px">
                      <a class="tooltipped" data-position="top" data-delay="50" data-tooltip="Menyelesaikan tugas pada waktunya.">G</a>
                    </td>
                    <td width="90px">
                      <a class="tooltipped" data-position="top" data-delay="50" data-tooltip="Saling menjaga dengan teman agar semua tugas-tugas kelas terlaksana dengan baik.">H</a>
                    </td>
                    <td width="90px">
                      <a class="tooltipped" data-position="top" data-delay="50" data-tooltip="Selalu mengajak teman menjaga ketertiban kelas.">I</a>
                    </td>
                    <td width="90px">
                      <a class="tooltipped" data-position="top" data-delay="50" data-tooltip="Mengingatkan teman yang melanggar peraturan dengan kata-kata sopan dan tidak menyinggung.">J</a>
                    </td>
                    <td width="90px">Total Nilai</td>
                  </tr>
                </thead>
                <tbody>
                  <?php
                    include_once './config/config.php';
                    $kelas = $_GET['k'];
                    $date = $_GET['d'];
                    $karakter = $_GET['n'];

                    $dataKarakter = mysqli_query($connect, "SELECT nilai FROM tb_nilai_karakter  WHERE id_kelas = $kelas AND id_karakter = $karakter AND DATE(create_at) = '$date'");
                    $i = 1;
                    while($result = mysqli_fetch_array($dataKarakter)){
                      $total = 0;
                      foreach (json_decode($result['nilai']) as $el) {
                        echo '
                        <tr>
                          <td>'.$i.'</td>
                          <td>'.$el->id_siswa.'</td>
                        ';
                        foreach($el->nilai as $elChild) {
                          $elChild->nilai === 'Ya' ? $total = $total + 1 : null;
                          echo '<td>'. $elChild->nilai . '</td>';
                        }
                        echo '<td>'. $total .'</td>';
                        echo '
                        </tr>
                        ';
                        $i++;
                      }
                    }
                  ?>
                  </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s1">
          <a href="main?module=master-penilaian-disiplin" class="btn grey waves-effect waves-light right" >Kembali</a>
        </div>
      </div>
      <?php
      } else {
      ?>
      <div class="row">
        <div class="col s12">
          <div class="card">
            <div class="card-content">
            <div style="padding-left: 0px !important; width: 200px; top: 66px; left: 25px; position: absolute;">
              <select id="filterNilaiDisiplinGuru">
                <option value="" disabled selected>Pilih Periode Nilai</option>
                <?php echo $md->getNilaiByMonth(base64_decode($_SESSION['kelas']), base64_decode($_SESSION['tahunAjaran']),  1) ?>
              </select>
              <label style="left: 0px !important">Bulan Ke</label>
            </div>
            <table id="table-master-nilai-displin" class="bordered highlight" width="100%">
              <thead>
                <tr>
                  <th>No.</th>
                  <th>Tanggal</th>
                  <th>Nama Kelas</th>
                  <th>Aksi</th>
                </tr>
              </thead>
            </table>
            </div>
          </div>
        </div>
      </div>
      <?php
      }
      ?>
    </div>
    <div id="siswa" class="col s12">
      <div class="row">
        <div class="col s12">
          <div class="card">
            <div class="card-content">
              <table id="table-nilai-disiplin-by-siswa" class="responsive-table highlight" style="width:100%">
                <thead>
                  <tr>
                    <th>No.</th>
                    <th>Nomor Induk Siswa</th>
                    <th>Nama Siswa</th>
                    <th>Total Nilai</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>

<div id="myModal" class="modal"></div>