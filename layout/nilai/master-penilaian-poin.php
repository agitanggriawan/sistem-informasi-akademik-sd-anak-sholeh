<div class="page-titles">
  <div class="d-flex align-items-center">
    <h5 class="font-medium m-b-0">Nilai Karakter Poin</h5>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col s12">
      <div class="card">
        <div class="card-content">
          <table id="table-nilai-poin-by-guru" class="bordered highlight" width="100%">
          </table>
        </div>
      </div>
    </div>
  </div>
</div>