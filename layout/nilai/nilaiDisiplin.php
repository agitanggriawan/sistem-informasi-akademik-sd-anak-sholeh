<style>
  .styleTable {
    text-align: center;
    font-weight: bold;
    color: black;
  }
</style>
<div class="page-titles">
  <div class="d-flex align-items-center">
    <h5 class="font-medium m-b-0">Nilai Karakter Disiplin</h5>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col s12">
      <div class="card">
        <div class="card-content">
          <p>
            <b>Nama Kelas:</b> Kelas 1 | <b>Nama Guru:</b> Guru A | <b>Tanggal:</b> <?php echo date('d - m - Y') ?>
          </p>
          <div class="row">
            <table class="responsive-table highlight" style="width:100%">
              <thead>
                <tr>
                  <td class="styleTable" rowspan="2" width="50px">No</td>
                  <td class="styleTable" rowspan="2" width="300px">Nama</td>
                  <td class="styleTable" colspan="11">Indikator</td>
                </tr>
                <tr>
                  <td width="90px">
                    <a class="tooltipped" data-position="top" data-delay="50" data-tooltip="Datang ke sekolah tepat dan masuk kelas pada waktunya.">A</a>
                  </td>
                  <td width="90px">
                    <a class="tooltipped" data-position="top" data-delay="50" data-tooltip="Melaksanakan tugas-tugas kelas yang menjadi tanggung jawabnya.">B</a>
                  </td>
                  <td width="90px">
                    <a class="tooltipped" data-position="top" data-delay="50" data-tooltip="Duduk pada tempat yang telah ditetapkan.">C</a>
                  </td>
                  <td width="90px">
                    <a class="tooltipped" data-position="top" data-delay="50" data-tooltip="Menaati peraturan sekolah dan kelas.">D</a>
                  </td>
                  <td width="90px">
                    <a class="tooltipped" data-position="top" data-delay="50" data-tooltip="Berpakaian sopan dan rapi.">E</a>
                  </td>
                  <td width="90px">
                    <a class="tooltipped" data-position="top" data-delay="50" data-tooltip="Mematuhi aturan permainan.">F</a>
                  </td>
                  <td width="90px">
                    <a class="tooltipped" data-position="top" data-delay="50" data-tooltip="Menyelesaikan tugas pada waktunya.">G</a>
                  </td>
                  <td width="90px">
                    <a class="tooltipped" data-position="top" data-delay="50" data-tooltip="Saling menjaga dengan teman agar semua tugas-tugas kelas terlaksana dengan baik.">H</a>
                  </td>
                  <td width="90px">
                    <a class="tooltipped" data-position="top" data-delay="50" data-tooltip="Selalu mengajak teman menjaga ketertiban kelas.">I</a>
                  </td>
                  <td width="90px">
                    <a class="tooltipped" data-position="top" data-delay="50" data-tooltip="Mengingatkan teman yang melanggar peraturan dengan kata-kata sopan dan tidak menyinggung.">J</a>
                  </td>
                  <td width="90px">Total Nilai</td>
                </tr>
              </thead>
              <tbody>
                <?php
                  include_once './config/config.php';
                  $dataKarakter = mysqli_query($connect, "SELECT nilai FROM tb_nilai_karakter");
                  $i = 1;
                  while($result = mysqli_fetch_array($dataKarakter)){
                    $total = 0;
                    foreach (json_decode($result['nilai']) as $el) {
                      echo '
                      <tr>
                        <td>'.$i.'</td>
                        <td>'.$el->id_siswa.'</td>
                      ';
                      foreach($el->nilai as $elChild) {
                        $elChild->nilai === 'Ya' ? $total = $total + 1 : null;
                        echo '<td>'. $elChild->nilai . '</td>';
                      }
                      echo '<td>'. $total .'</td>';
                      echo '
                      </tr>
                      ';
                      $i++;
                    }
                  }
                ?>
                </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- 
const itemA = $('#siswa1').find('.nilaiKarakter') ==> Map Class
$(itemA[1]).val() ==> Get Value

var siswa = $('#siswa1').find('#namaSiswa1');
siswa[0].textContent
-->