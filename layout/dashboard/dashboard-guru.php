<div class="container-fluid">
  <div class="row">
  <div class="col s12 m4 l4">
    <div class="card">
    <div class="card-content">
      <div class="center-align m-t-30"> <img src="./dist/img/d1.jpg" class="circle" width="150" />
      <h4 class="card-title m-t-10"><?php echo base64_decode($_SESSION['nama']) ?></h4>
      <h6 class="card-subtitle"><?php echo base64_decode($_SESSION['level']) ?></h6>
      </div>
    </div>
    <hr>
    <div class="card-content">
      <small>Tanggal</small>
      <h6><b><?php echo date('d - m - Y') ?></h6>
      <small>Nomor Induk</small>
      <h6><?php $my = $md->myProfile(base64_decode($_SESSION['id'])); echo $my[1]; ?></h6>
      <small>Alamat</small>
      <h6><?php $my = $md->myProfile(base64_decode($_SESSION['id'])); echo $my[5]; ?></h6>
      <small>Guru Kelas</small>
      <h6><?php $my = $md->myProfile(base64_decode($_SESSION['id'])); echo 'Kelas ' . $my[10]; ?></h6>
    </div>
    </div>
  </div>
  <div>
    <div class="row">
      <div class="col l4 m4 s12">
        <div class="card">
          <div class="card-content center-align">
            <div>
              <span class="blue-text display-6"><i class="ti-pencil-alt"></i></span>
            </div>
            <div>
              <h4>Guru kelas <?php echo $my[10] ?> belum mengisi nilai karakter hari ini</h4>
            </div>
          </div>
        </div>
      </div>
      <div class="col l4 m4 s12">
        <div class="card">
          <div class="card-content center-align">
            <div>
              <span class="blue-text display-6"><i class="ti-notepad"></i></span>
            </div>
            <div>
            <?php 
            if (!$md->isPresence(base64_decode($_SESSION['id']), base64_decode($_SESSION['kelas']))) { 
            ?>
              <h4>Guru kelas <?php echo $my[10] ?> sudah presensi hari ini</h4>
            <?php } else {
            ?>
              <h4>Guru kelas <?php echo $my[10] ?> <b>belum</b> mengisi presensi hari ini</h4>
            <?php } ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</div>