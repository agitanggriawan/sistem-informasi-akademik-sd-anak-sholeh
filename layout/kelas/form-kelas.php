<div class="page-titles">
  <div class="d-flex align-items-center">
    <h5 class="font-medium m-b-0">Form Kelas</h5>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col s12">
    <ul class="tabs">
      <li class="tab col s6"><a href="#input" class="active">Input Data</a></li>
      <li class="tab col s6"><a href="#list">List Data</a></li>
    </ul>
  </div>
  <div id="input" class="col s12">
    <div class="row">
      <div class="col s12">
        <div class="card">
          <div class="card-content">
            <form class="formValidate" id="validateKelas">
              <div class="row">
                <div class="input-field col s6 offset-s3">
                  <select name="tingkatKelas" id="tingkat-kelas" required>
                    <option value="" disabled selected>Tingkat Kelas</option>
                    <option value="1">Kelas 1</option>
                    <option value="2">Kelas 2</option>
                    <option value="3">Kelas 3</option>
                    <option value="4">Kelas 4</option>
                    <option value="5">Kelas 5</option>
                    <option value="6">Kelas 6</option>
                  </select>
                  <label>Jenis Kelas</label>
                </div>
                <div class="input-field col s6 offset-s3">
                  <select name="jenisKelas" id="jenis-kelas" required>
                    <option value="" disabled selected>Jenis Kelas</option>
                    <option value="A">A</option>
                    <option value="B">B</option>
                    <option value="C">C</option>
                  </select>
                  <label>Jenis Kelas</label>
                </div>
                <div class="input-field col s6 offset-s3">
                  <button class="waves-effect waves-light btn btn-round blue 
                  btn-simpan" type="submit">Simpan</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="list" class="col s12">
    <div class="row">
      <div class="col s12">
        <div class="card">
          <div class="card-content">
          <table id="table-get-kelas" class="bordered highlight" width="100%">
            <thead>
              <tr>
                <th>No.</th>
                <th>ID Kelas</th>
                <th>Nama Kelas</th>
              </tr>
            </thead>
          </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</div>