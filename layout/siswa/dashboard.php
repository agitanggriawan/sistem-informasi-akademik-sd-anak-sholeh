<div class="container-fluid">
  <div class="row">
    <div class="col s12 m4 l4" id="first-card">
      <div class="card">
        <div class="card-content">
          <div class="center-align m-t-30"> <img src="./dist/img/d1.jpg" class="circle" width="150" />
          <h4 class="card-title m-t-10"><?php echo base64_decode($_SESSION['nama']) ?></h4>
          <h6 class="card-subtitle"><?php echo base64_decode($_SESSION['level']) ?></h6>
          </div>
        </div>
        <hr>
        <div class="card-content">
          <small>Nomor Induk</small>
          <h6><?php $my = $md->myProfileSiswa(base64_decode($_SESSION['id'])); echo $my[0]; ?></h6>
          <small>Nama Siswa</small>
          <h6><?php echo $my[1]?></h6>
          <small>Kelas</small>
          <h6><?php echo $my[4]?></h6>
          <small>Alamat</small>
          <h6><?php echo $my[2]?></h6>
          <small>Jenis Kelamin</small>
          <h6><?php echo $my[3]?></h6>
        </div>
      </div>
    </div>
    <div class="col l8 m8 s12">
      <div class="card">
        <div class="card-content center-align">
          <div id="LineChartNilaiKarakter" style="min-width: 310px; max-width: 800px; margin: 0 auto"></div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col l4 m4 s12">
        <div class="card">
          <div class="card-content center-align">
            <div id="PieChartNilaiPoin" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
          </div>
        </div>
      </div>
      <div class="col l8 m8 s12">
        <div class="card">
          <div class="card-content center-align">
            <div id="BarChartNilaiKarakter" style="min-width: 310px; height: 400px; max-width: 800px; margin: 0 auto"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>