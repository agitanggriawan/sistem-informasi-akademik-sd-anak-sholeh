<div class="page-titles">
  <div class="d-flex align-items-center">
    <h5 class="font-medium m-b-0">Form Siswa</h5>
    <!-- <div class="custom-breadcrumb ml-auto">
      <a href="#!" class="breadcrumb">Home</a>
      <a href="#!" class="breadcrumb">Input Grid</a>
    </div> -->
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col s12">
      <ul class="tabs">
          <li class="tab col s6"><a href="#transaksi" class="active">Transaksi</a></li>
          <li class="tab col s6"><a href="#history">History</a></li>
      </ul>
    </div>
      <div id="transaksi" class="col s12">
        <div class="row">
          <div class="col s12">
            <div class="card">
              <div class="card-content">
                <h5 class="card-title">Transaksi Penjualan</h5>
                <div class="row">
                  <div class="col s6">
                    <div class="input-field">Nama Barang</div>
                  </div>
                  <div class="col s1">
                    <div class="input-field">Qty</div>
                  </div>
                  <div class="col s2">
                    <div class="input-field">Harga</div>
                  </div>
                  <div class="col s2">
                    <div class="input-field">Sub Total</div>
                  </div>
                </div>
                <div class="parentTable">
                  <div class="row classTable" id="childTable">
                    <div class="col s6">
                      <div class="input-field">
                        <input id="namaBarang" type="text" placeholder="Nama Barang" autofocus>
                      </div>
                    </div>
                    <div class="col s1">
                      <div class="input-field">
                        <input id="qty" type="number" placeholder="Qty" value="">
                      </div>
                    </div>
                    <div class="col s2">
                      <div class="input-field">
                        <input id="harga" type="number" placeholder="Harga" value="">
                      </div>
                    </div>
                    <div class="col s3">
                      <div class="input-field">
                        <input id="total" type="text" placeholder="Sub Total" onfocus="countTotal('qty', 'harga', 'total')">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="input-field col s7">
                    <button id="addButton" class="btn green waves-effect waves-light left" type="button" onclick="tambahBarang()">Tambah</button>
                  </div>
                  <div class="col s2">
                    <div class="input-field right" style="margin-top: 30px">Grand Total</div>
                  </div>
                  <div class="col s2">
                    <div class="input-field">
                      <input id="grandTotal" type="text" placeholder="Grand Total" onfocus="grandTotal('grandTotal')" readonly>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col s2 offset-s7">
                    <div class="input-field right" style="margin-top: 30px">Total Bayar</div>
                  </div>
                  <div class="col s2">
                    <div class="input-field">
                      <input id="totalBayar" type="text" placeholder="Total Bayar" onmouseleave="totalBayar('grandTotal', 'totalBayar')">
                    </div>
                  </div>
                  <div class="input-field col s1">
                    <button id="saveAll" class="btn blue waves-effect waves-light right" disabled type="button" onclick="saveAll()">Simpan</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div id="history" class="col s12">
        <div class="row">
          <div class="col s12">
            <div class="card">
              <div class="card-content">
                <table id="tableHistories" class="table table-striped table-bordered display" style="width:100%">
                  <thead>
                      <tr>
                        <th>No</th>
                        <th>Tanggal Transaksi</th>
                        <th>Total Bayar</th>
                        <th>Grand Total</th>
                        <th>Kembalian</th>
                        <th width="150px">Aksi</th>
                      </tr>
                  </thead>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
  </div>
</div>