<div class="page-titles">
  <div class="d-flex align-items-center">
    <h5 class="font-medium m-b-0">Nilai Poin</h5>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col s12">
      <div class="card">
        <div class="card-content">
        <p>
          Nomor Induk: <b><?php echo base64_decode($_SESSION['nomorInduk']) ?></b>
          | Nama Siswa: <b><?php echo base64_decode($_SESSION['nama']) ?></b>
          | Nama Kelas: <b><?php echo $md->namaKelas(base64_decode($_SESSION['kelas'])) ?></b>
          | Periode Nilai Bulan: <b><?php echo date('M') ?></b>
        </p>
          <table id="table-nilai-poin-by-siswa" class="bordered highlight" width="100%">
            <thead>
              <tr>
                <th>No.</th>
                <th>Tanggal Penilaian</th>
                <th>Nama Guru</th>
                <th>Nilai 1</th>
                <th>Nilai 2</th>
                <th>Nilai 3</th>
              </tr>
            </thead>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>