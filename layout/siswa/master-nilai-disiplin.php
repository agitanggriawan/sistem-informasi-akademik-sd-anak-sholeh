<div class="page-titles">
  <div class="d-flex align-items-center">
    <h5 class="font-medium m-b-0">Nilai Karakter Disiplin</h5>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col s12">
      <div class="card">
        <div class="card-content">
        <p>
          Nomor Induk: <b><?php echo base64_decode($_SESSION['nomorInduk']) ?></b>
          | Nama Siswa: <b><?php echo base64_decode($_SESSION['nama']) ?></b>
          | Nama Kelas: <b><?php echo $md->namaKelas(base64_decode($_SESSION['kelas'])) ?></b>
          | Periode Nilai Bulan: <b><?php
            if (isset($_GET['month'])) {
              $m = $_GET['month'];
            } else {
              $m = date('m');
            }
            echo $md->getMonth($m);
          ?></b>
        </p>
        <div class="input-field col s3" style="padding-left: 0px !important">
          <select id="filterNilaiKarakter">
            <option value="" disabled selected>Pilih Periode Nilai</option>
            <?php echo $md->getDataNilaiByMonth(base64_decode($_SESSION['kelas']), base64_decode($_SESSION['tahunAjaran']), base64_decode($_SESSION['nama']), 1) ?>
          </select>
          <label style="left: 0px !important">Bulan Ke</label>
        </div>
        <table class="responsive-table highlight" style="width:100%">
          <thead>
            <tr>
              <td class="styleTable" rowspan="2" width="50px">No</td>
              <td class="styleTable" rowspan="2" width="250px">Tanggal Penilaian</td>
              <td class="styleTable" colspan="11">Indikator</td>
            </tr>
            <tr>
              <td width="90px">
                <a class="tooltipped" data-position="top" data-delay="50" data-tooltip="Datang ke sekolah tepat dan masuk kelas pada waktunya.">A</a>
              </td>
              <td width="90px">
                <a class="tooltipped" data-position="top" data-delay="50" data-tooltip="Melaksanakan tugas-tugas kelas yang menjadi tanggung jawabnya.">B</a>
              </td>
              <td width="90px">
                <a class="tooltipped" data-position="top" data-delay="50" data-tooltip="Duduk pada tempat yang telah ditetapkan.">C</a>
              </td>
              <td width="90px">
                <a class="tooltipped" data-position="top" data-delay="50" data-tooltip="Menaati peraturan sekolah dan kelas.">D</a>
              </td>
              <td width="90px">
                <a class="tooltipped" data-position="top" data-delay="50" data-tooltip="Berpakaian sopan dan rapi.">E</a>
              </td>
              <td width="90px">
                <a class="tooltipped" data-position="top" data-delay="50" data-tooltip="Mematuhi aturan permainan.">F</a>
              </td>
              <td width="90px">
                <a class="tooltipped" data-position="top" data-delay="50" data-tooltip="Menyelesaikan tugas pada waktunya.">G</a>
              </td>
              <td width="90px">
                <a class="tooltipped" data-position="top" data-delay="50" data-tooltip="Saling menjaga dengan teman agar semua tugas-tugas kelas terlaksana dengan baik.">H</a>
              </td>
              <td width="90px">
                <a class="tooltipped" data-position="top" data-delay="50" data-tooltip="Selalu mengajak teman menjaga ketertiban kelas.">I</a>
              </td>
              <td width="90px">
                <a class="tooltipped" data-position="top" data-delay="50" data-tooltip="Mengingatkan teman yang melanggar peraturan dengan kata-kata sopan dan tidak menyinggung.">J</a>
              </td>
              <td width="90px"><b>Total Nilai</b></td>
            </tr>
          </thead>
          <tbody>
            <?php
              include_once './config/config.php';
              $kelas = base64_decode($_SESSION['kelas']);
              $nama = base64_decode($_SESSION['nama']);
              $id_tahun_ajaran = base64_decode($_SESSION['tahunAjaran']);
              if (isset($_GET['month'])) {
                $month = $_GET['month'];
                $dataKarakter = mysqli_query($connect, "SELECT nilai, create_at FROM tb_nilai_karakter  WHERE id_kelas = $kelas AND id_karakter = 1 AND JSON_CONTAINS(nilai, '{\"id_siswa\": \"$nama\"}') AND id_tahun_ajaran = $id_tahun_ajaran AND MONTH(create_at) = $month ORDER BY 2 ASC");
              } else {
                $dataKarakter = mysqli_query($connect, "SELECT nilai, create_at FROM tb_nilai_karakter  WHERE id_kelas = $kelas AND id_karakter = 1 AND JSON_CONTAINS(nilai, '{\"id_siswa\": \"$nama\"}') AND id_tahun_ajaran = $id_tahun_ajaran AND MONTH(create_at) = MONTH(CURRENT_DATE()) ORDER BY 2 ASC");
              }

              $i = 1;
              $grandTotal = 0;
              while($result = mysqli_fetch_array($dataKarakter)){
                $date = strtotime($result['create_at']);
                $current_date = date( 'd - m - Y', $date );
                $total = 0;
                foreach (json_decode($result['nilai']) as $el) {
                  echo '
                  <tr>
                    <td>'.$i.'.</td>
                    <td>'.$current_date.'</td>
                  ';
                  foreach($el->nilai as $elChild) {
                    $elChild->nilai === 'Ya' ? $total = $total + 1 : null;
                    $elChild->nilai === 'Ya' ? $grandTotal = $grandTotal + 1 : null;
                    echo '<td>'. $elChild->nilai . '</td>';
                  }
                  echo '<td>'. $total .'</td>';
                  echo '
                  </tr>
                  ';
                  $i++;
                }
              }
            ?>
            <tr>
              <td class="styleTable" colspan="12">TOTAL</td>
              <td><b><?php echo $grandTotal ?></b></td>
            </tr>
            </tbody>
        </table>
        </div>
      </div>
    </div>
  </div>
</div>