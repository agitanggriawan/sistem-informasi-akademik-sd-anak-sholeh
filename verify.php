<?php
  session_start();
  require 'config/config.php';

  $nomorInduk = mysqli_real_escape_string($connect, $_POST['nomorInduk']);
  $password = mysqli_real_escape_string($connect, $_POST['password']);

  $queryUser = mysqli_query($connect, "select id_user, nomor_induk, nama_user, level, kelas from tb_user where nomor_induk = '$nomorInduk' and password = $password");
  $rowUser = mysqli_num_rows($queryUser);
  $resultUser = mysqli_fetch_array($queryUser);

  $queryTahunAjaran = mysqli_query($connect, "select id from tb_tahun_ajaran where aktif = 1 LIMIT 1");
  $resultTahunAjaran = mysqli_fetch_array($queryTahunAjaran);

  if ($rowUser > 0) {
    $_SESSION['id'] = base64_encode($resultUser['id_user']);
    $_SESSION['nomorInduk'] = base64_encode($nomorInduk);
    $_SESSION['nama'] = base64_encode($resultUser['nama_user']);
    $_SESSION['level'] = base64_encode($resultUser['level']);
    $_SESSION['kelas'] = base64_encode($resultUser['kelas']);
    $_SESSION['tahunAjaran'] = base64_encode($resultTahunAjaran['id']);
    header('Location: main?module=dashboard');
  } else {
    $querySiswa = mysqli_query($connect, "select id_siswa, nomor_induk, nama_siswa, id_kelas from tb_siswa where nomor_induk = '$nomorInduk' and password = $password");
    $rowSiswa = mysqli_num_rows($querySiswa);
    $resultUser = mysqli_fetch_array($querySiswa);

    if ($rowSiswa > 0) {
      $_SESSION['id'] = base64_encode($resultUser['id_siswa']);
      $_SESSION['nomorInduk'] = base64_encode($nomorInduk);
      $_SESSION['nama'] = base64_encode($resultUser['nama_siswa']);
      $_SESSION['kelas'] = base64_encode($resultUser['id_kelas']);
      $_SESSION['level'] = base64_encode('Siswa');
      $_SESSION['tahunAjaran'] = base64_encode($resultTahunAjaran['id']);
      header('Location: main?module=dashboard');
    } else {
      header('Location: index?n=f1');
    }
  }
?>