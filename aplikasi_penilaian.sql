-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 07, 2019 at 09:32 AM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 5.6.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `aplikasi_penilaian`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_detail_presensi`
--

CREATE TABLE `tb_detail_presensi` (
  `ID_DETAIL_PRESENSI` int(11) NOT NULL,
  `ID_SISWA` int(11) DEFAULT NULL,
  `ID_PRESENSI` int(11) DEFAULT NULL,
  `STATUS` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_gambar`
--

CREATE TABLE `tb_gambar` (
  `ID_GAMBAR` int(11) NOT NULL,
  `GAMBAR` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_kelas`
--

CREATE TABLE `tb_kelas` (
  `ID_KELAS` int(11) NOT NULL,
  `ID_SISWA` int(11) DEFAULT NULL,
  `NAMA_KELAS` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_m_indikator`
--

CREATE TABLE `tb_m_indikator` (
  `ID_INDIKATOR` int(11) NOT NULL,
  `ID_KARAKTER` int(11) DEFAULT NULL,
  `NAMA_INDIKATOR` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_m_karakter`
--

CREATE TABLE `tb_m_karakter` (
  `ID_KARAKTER` int(11) NOT NULL,
  `NAMA_KARAKTER` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_nilai_karakter`
--

CREATE TABLE `tb_nilai_karakter` (
  `ID_NILAI` int(11) NOT NULL,
  `ID_SISWA` int(11) DEFAULT NULL,
  `ID_USER` int(11) DEFAULT NULL,
  `ID_INDIKATOR` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_nilai_point`
--

CREATE TABLE `tb_nilai_point` (
  `ID_POINT` int(11) NOT NULL,
  `ID_SISWA` int(11) DEFAULT NULL,
  `ID_USER` int(11) DEFAULT NULL,
  `NILAI1` int(11) DEFAULT NULL,
  `NILAI2` int(11) DEFAULT NULL,
  `NILAI3` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_presensi`
--

CREATE TABLE `tb_presensi` (
  `ID_PRESENSI` int(11) NOT NULL,
  `ID_USER` int(11) DEFAULT NULL,
  `ID_KELAS` int(11) DEFAULT NULL,
  `TGL` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_siswa`
--

CREATE TABLE `tb_siswa` (
  `ID_SISWA` int(11) NOT NULL,
  `NAMA_SISWA` varchar(100) DEFAULT NULL,
  `NOMOR_INDUK` varchar(11) DEFAULT NULL,
  `ALAMAT` varchar(100) DEFAULT NULL,
  `JENIS_KELAMIN` varchar(20) DEFAULT NULL,
  `PASSWORD` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `ID_USER` int(11) NOT NULL,
  `NOMOR_INDUK` varchar(255) NOT NULL,
  `NAMA_USER` varchar(100) DEFAULT NULL,
  `PASSWORD` int(11) DEFAULT NULL,
  `ALAMAT` varchar(100) DEFAULT NULL,
  `LEVEL` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_detail_presensi`
--
ALTER TABLE `tb_detail_presensi`
  ADD PRIMARY KEY (`ID_DETAIL_PRESENSI`),
  ADD KEY `ID_SISWA` (`ID_SISWA`),
  ADD KEY `ID_PRESENSI` (`ID_PRESENSI`);

--
-- Indexes for table `tb_gambar`
--
ALTER TABLE `tb_gambar`
  ADD PRIMARY KEY (`ID_GAMBAR`);

--
-- Indexes for table `tb_kelas`
--
ALTER TABLE `tb_kelas`
  ADD PRIMARY KEY (`ID_KELAS`),
  ADD KEY `ID_SISWA` (`ID_SISWA`);

--
-- Indexes for table `tb_m_indikator`
--
ALTER TABLE `tb_m_indikator`
  ADD PRIMARY KEY (`ID_INDIKATOR`),
  ADD KEY `ID_KARAKTER` (`ID_KARAKTER`);

--
-- Indexes for table `tb_m_karakter`
--
ALTER TABLE `tb_m_karakter`
  ADD PRIMARY KEY (`ID_KARAKTER`);

--
-- Indexes for table `tb_nilai_karakter`
--
ALTER TABLE `tb_nilai_karakter`
  ADD PRIMARY KEY (`ID_NILAI`),
  ADD KEY `ID_USER` (`ID_USER`),
  ADD KEY `ID_SISWA` (`ID_SISWA`),
  ADD KEY `ID_INDIKATOR` (`ID_INDIKATOR`);

--
-- Indexes for table `tb_nilai_point`
--
ALTER TABLE `tb_nilai_point`
  ADD PRIMARY KEY (`ID_POINT`),
  ADD KEY `ID_USER` (`ID_USER`),
  ADD KEY `ID_SISWA` (`ID_SISWA`);

--
-- Indexes for table `tb_presensi`
--
ALTER TABLE `tb_presensi`
  ADD PRIMARY KEY (`ID_PRESENSI`),
  ADD KEY `ID_USER` (`ID_USER`),
  ADD KEY `ID_KELAS` (`ID_KELAS`);

--
-- Indexes for table `tb_siswa`
--
ALTER TABLE `tb_siswa`
  ADD PRIMARY KEY (`ID_SISWA`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`ID_USER`),
  ADD UNIQUE KEY `NOMOR_INDUK` (`NOMOR_INDUK`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_detail_presensi`
--
ALTER TABLE `tb_detail_presensi`
  MODIFY `ID_DETAIL_PRESENSI` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_gambar`
--
ALTER TABLE `tb_gambar`
  MODIFY `ID_GAMBAR` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_kelas`
--
ALTER TABLE `tb_kelas`
  MODIFY `ID_KELAS` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_m_indikator`
--
ALTER TABLE `tb_m_indikator`
  MODIFY `ID_INDIKATOR` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_m_karakter`
--
ALTER TABLE `tb_m_karakter`
  MODIFY `ID_KARAKTER` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_nilai_karakter`
--
ALTER TABLE `tb_nilai_karakter`
  MODIFY `ID_NILAI` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_nilai_point`
--
ALTER TABLE `tb_nilai_point`
  MODIFY `ID_POINT` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_presensi`
--
ALTER TABLE `tb_presensi`
  MODIFY `ID_PRESENSI` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_siswa`
--
ALTER TABLE `tb_siswa`
  MODIFY `ID_SISWA` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `ID_USER` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tb_detail_presensi`
--
ALTER TABLE `tb_detail_presensi`
  ADD CONSTRAINT `tb_detail_presensi_ibfk_1` FOREIGN KEY (`ID_SISWA`) REFERENCES `tb_siswa` (`ID_SISWA`),
  ADD CONSTRAINT `tb_detail_presensi_ibfk_2` FOREIGN KEY (`ID_PRESENSI`) REFERENCES `tb_presensi` (`ID_PRESENSI`);

--
-- Constraints for table `tb_kelas`
--
ALTER TABLE `tb_kelas`
  ADD CONSTRAINT `tb_kelas_ibfk_1` FOREIGN KEY (`ID_SISWA`) REFERENCES `tb_siswa` (`ID_SISWA`);

--
-- Constraints for table `tb_m_indikator`
--
ALTER TABLE `tb_m_indikator`
  ADD CONSTRAINT `tb_m_indikator_ibfk_1` FOREIGN KEY (`ID_KARAKTER`) REFERENCES `tb_m_karakter` (`ID_KARAKTER`);

--
-- Constraints for table `tb_nilai_karakter`
--
ALTER TABLE `tb_nilai_karakter`
  ADD CONSTRAINT `tb_nilai_karakter_ibfk_1` FOREIGN KEY (`ID_USER`) REFERENCES `tb_user` (`ID_USER`),
  ADD CONSTRAINT `tb_nilai_karakter_ibfk_2` FOREIGN KEY (`ID_SISWA`) REFERENCES `tb_siswa` (`ID_SISWA`),
  ADD CONSTRAINT `tb_nilai_karakter_ibfk_3` FOREIGN KEY (`ID_INDIKATOR`) REFERENCES `tb_m_karakter` (`ID_KARAKTER`);

--
-- Constraints for table `tb_nilai_point`
--
ALTER TABLE `tb_nilai_point`
  ADD CONSTRAINT `tb_nilai_point_ibfk_1` FOREIGN KEY (`ID_USER`) REFERENCES `tb_user` (`ID_USER`),
  ADD CONSTRAINT `tb_nilai_point_ibfk_2` FOREIGN KEY (`ID_SISWA`) REFERENCES `tb_siswa` (`ID_SISWA`);

--
-- Constraints for table `tb_presensi`
--
ALTER TABLE `tb_presensi`
  ADD CONSTRAINT `tb_presensi_ibfk_1` FOREIGN KEY (`ID_USER`) REFERENCES `tb_user` (`ID_USER`),
  ADD CONSTRAINT `tb_presensi_ibfk_2` FOREIGN KEY (`ID_KELAS`) REFERENCES `tb_kelas` (`ID_KELAS`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
