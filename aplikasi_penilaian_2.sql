-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 01, 2019 at 04:47 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `aplikasi_penilaian`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_detail_presensi`
--

CREATE TABLE `tb_detail_presensi` (
  `ID_DETAIL_PRESENSI` int(11) NOT NULL,
  `ID_SISWA` int(11) DEFAULT NULL,
  `ID_PRESENSI` int(11) DEFAULT NULL,
  `STATUS` varchar(100) DEFAULT NULL,
  `CREATE_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UPDATE_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_gambar`
--

CREATE TABLE `tb_gambar` (
  `ID_GAMBAR` int(11) NOT NULL,
  `GAMBAR` text,
  `CREATE_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UPDATE_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_kelas`
--

CREATE TABLE `tb_kelas` (
  `ID_KELAS` int(11) NOT NULL,
  `NAMA_KELAS` varchar(30) DEFAULT NULL,
  `CREATE_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UPDATE_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_kelas`
--

INSERT INTO `tb_kelas` (`ID_KELAS`, `NAMA_KELAS`, `CREATE_AT`, `UPDATE_AT`) VALUES
(1, '1A', '2019-05-11 11:28:34', '2019-05-11 11:28:34'),
(2, '1B', '2019-05-11 11:28:34', '2019-05-11 11:28:34'),
(3, '1C', '2019-05-11 11:28:34', '2019-05-11 11:28:34'),
(4, '2A', '2019-05-11 11:28:34', '2019-05-11 11:28:34'),
(5, '2B', '2019-05-11 11:28:34', '2019-05-11 11:28:34'),
(6, '2C', '2019-05-11 11:28:34', '2019-05-11 11:28:34'),
(7, '3A', '2019-05-11 11:28:34', '2019-05-11 11:28:34'),
(8, '3B', '2019-05-11 11:28:34', '2019-05-11 11:28:34'),
(9, '3C', '2019-05-11 11:28:34', '2019-05-11 11:28:34'),
(10, '4A', '2019-05-11 11:28:34', '2019-05-11 11:28:34'),
(11, '4B', '2019-05-11 11:28:34', '2019-05-11 11:28:34'),
(12, '4C', '2019-05-11 11:28:34', '2019-05-11 11:28:34'),
(13, '5A', '2019-05-11 11:28:34', '2019-05-11 11:28:34'),
(14, '5B', '2019-05-11 11:28:34', '2019-05-11 11:28:34'),
(15, '5C', '2019-05-11 11:28:34', '2019-05-11 11:28:34'),
(16, '6A', '2019-05-11 11:28:34', '2019-05-11 11:28:34'),
(17, '6B', '2019-05-11 11:28:34', '2019-05-11 11:28:34');

-- --------------------------------------------------------

--
-- Table structure for table `tb_m_indikator`
--

CREATE TABLE `tb_m_indikator` (
  `ID_INDIKATOR` int(11) NOT NULL,
  `ID_KARAKTER` int(11) DEFAULT NULL,
  `NAMA_INDIKATOR` varchar(200) DEFAULT NULL,
  `CREATE_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UPDATE_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_m_indikator`
--

INSERT INTO `tb_m_indikator` (`ID_INDIKATOR`, `ID_KARAKTER`, `NAMA_INDIKATOR`, `CREATE_AT`, `UPDATE_AT`) VALUES
(1, 1, 'Datang ke sekolah tepat dan masuk kelas pada waktunyan', '2019-05-12 07:33:28', '2019-05-12 07:33:28'),
(2, 1, 'Melaksanakan tugas-tugas kelas yang menjadi tanggung jawabnya', '2019-05-12 07:33:28', '2019-05-12 07:33:28'),
(3, 1, 'Duduk pada tempat yang telah di tetapkan', '2019-05-12 07:33:28', '2019-05-12 07:33:28'),
(4, 1, 'menaati peraturan sekolah dan kelas', '2019-05-12 07:33:28', '2019-05-12 07:33:28'),
(5, 1, 'berpakaian sopan dan rapi', '2019-05-12 07:33:28', '2019-05-12 07:33:28'),
(6, 1, 'Mematuhi aturan permainan', '2019-05-12 07:33:28', '2019-05-12 07:33:28'),
(7, 1, 'Menyelesaikan tugas pada waktunya', '2019-05-12 07:33:28', '2019-05-12 07:33:28'),
(8, 1, 'Saling menjaga dengan teman agar semua tugas-tugas kelas terlaksana dengan baik', '2019-05-12 07:33:28', '2019-05-12 07:33:28'),
(9, 1, 'Selalu mengajak teman menjaga ketertiban kelas', '2019-05-12 07:33:28', '2019-05-12 07:33:28'),
(10, 1, 'Mengingatkan teman yang melanggar praturan dengan kata-kata sopan dan tidak menyinggung', '2019-05-12 07:33:28', '2019-05-12 07:33:28'),
(11, 2, 'Melakukan tugas rutin tanpa harus selalu diberi tahu', '2019-05-12 08:02:18', '2019-05-12 08:02:18'),
(12, 2, 'Dapat menjelaskan alasan atas apa yang dilaukannya ', '2019-05-12 08:02:18', '2019-05-12 08:02:18'),
(13, 2, 'Tidak menyalahkan orang lain dengan berlebihan', '2019-05-12 08:02:18', '2019-05-12 08:02:18'),
(14, 2, 'Mampu menentukan pilihan dari beberapa alternatif', '2019-05-12 08:02:18', '2019-05-12 08:02:18'),
(15, 2, 'Dapat bermain atau bekerja sendiri dengan senang hati', '2019-05-12 08:02:18', '2019-05-12 08:02:18'),
(16, 2, 'Dapat mengambil keputusan yang berbeda dari orang lain dalam kelompok', '2019-05-12 08:02:18', '2019-05-12 08:02:18'),
(17, 2, 'Mempunyai bermacam-macam tujuan atau minat yang ia tekuni', '2019-05-12 08:02:18', '2019-05-12 08:02:18'),
(18, 2, 'Menghormati dan menghargai aturan yang ditetapkan orang tua, tidak mendebatnya secara berlebihan', '2019-05-12 08:02:18', '2019-05-12 08:02:18'),
(19, 2, 'Dapat berkonsentrasi pada tugas-tugas yang rumit (sesuai dengan umurnya) untuk satu jangka waktu, tanpa rasa frustasi yang berlebihan', '2019-05-12 08:02:18', '2019-05-12 08:02:18'),
(20, 2, 'Mengerjakan apa yang dikatakan akan dilakukannya', '2019-05-12 08:02:18', '2019-05-12 08:02:18'),
(21, 2, 'Mengakui kesalahan tanpa mengajukan alasan yang dibuat-buat', '2019-05-12 08:02:18', '2019-05-12 08:02:18');

-- --------------------------------------------------------

--
-- Table structure for table `tb_m_karakter`
--

CREATE TABLE `tb_m_karakter` (
  `ID_KARAKTER` int(11) NOT NULL,
  `NAMA_KARAKTER` varchar(100) DEFAULT NULL,
  `CREATE_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UPDATE_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LEVEL` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_m_karakter`
--

INSERT INTO `tb_m_karakter` (`ID_KARAKTER`, `NAMA_KARAKTER`, `CREATE_AT`, `UPDATE_AT`, `LEVEL`) VALUES
(1, 'Disiplin', '2019-05-12 06:58:26', '2019-05-12 06:58:26', '1'),
(2, 'Tanggung Jawab', '2019-05-12 06:58:26', '2019-05-12 06:58:26', '2'),
(3, 'Jujur', '2019-05-12 07:15:08', '2019-05-12 07:15:08', '3'),
(4, 'Toleransi', '2019-05-12 07:15:08', '2019-05-12 07:15:08', '4'),
(5, 'Religius', '2019-05-12 07:15:08', '2019-05-12 07:15:08', '5');

-- --------------------------------------------------------

--
-- Table structure for table `tb_nilai_karakter`
--

CREATE TABLE `tb_nilai_karakter` (
  `ID_NILAI` int(11) NOT NULL,
  `ID_KELAS` int(11) DEFAULT NULL,
  `ID_USER` int(11) DEFAULT NULL,
  `ID_KARAKTER` int(11) DEFAULT NULL,
  `CREATE_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UPDATE_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `NILAI` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_nilai_karakter`
--

INSERT INTO `tb_nilai_karakter` (`ID_NILAI`, `ID_KELAS`, `ID_USER`, `ID_KARAKTER`, `CREATE_AT`, `UPDATE_AT`, `NILAI`) VALUES
(98, 1, 8, 1, '2019-06-01 08:55:46', '2019-06-01 08:55:46', '[{\"id_siswa\":\"Ahmad Fatikh Ramdhani\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Ya\"},{\"id_indikator\":2,\"nilai\":\"Ya\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Ya\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Ya\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(99, 1, 8, 1, '2019-06-01 08:55:46', '2019-06-01 08:55:46', '[{\"id_siswa\":\"Ahsani Taqwim Sucahyono\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Tidak\"},{\"id_indikator\":2,\"nilai\":\"Tidak\"},{\"id_indikator\":3,\"nilai\":\"Ya\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Ya\"},{\"id_indikator\":8,\"nilai\":\"Ya\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(100, 1, 8, 1, '2019-06-01 08:55:46', '2019-06-01 08:55:46', '[{\"id_siswa\":\"Almira Azalia Marga\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Tidak\"},{\"id_indikator\":2,\"nilai\":\"Tidak\"},{\"id_indikator\":3,\"nilai\":\"Ya\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Ya\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(101, 1, 8, 1, '2019-06-01 08:55:46', '2019-06-01 08:55:46', '[{\"id_siswa\":\"Almira Vidi Zafira\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Ya\"},{\"id_indikator\":2,\"nilai\":\"Tidak\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(102, 1, 8, 1, '2019-06-01 08:55:46', '2019-06-01 08:55:46', '[{\"id_siswa\":\"Anggito Abimanyu\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Tidak\"},{\"id_indikator\":2,\"nilai\":\"Tidak\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Ya\"},{\"id_indikator\":5,\"nilai\":\"Ya\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Ya\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Ya\"}]}]'),
(103, 1, 8, 1, '2019-06-01 08:55:46', '2019-06-01 08:55:46', '[{\"id_siswa\":\"Aqilah Nadhifa Putri Ibrahim\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Tidak\"},{\"id_indikator\":2,\"nilai\":\"Tidak\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Ya\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(104, 1, 8, 1, '2019-06-01 08:55:46', '2019-06-01 08:55:46', '[{\"id_siswa\":\"Arrayyan Muhammad Ghifarry D\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Ya\"},{\"id_indikator\":2,\"nilai\":\"Ya\"},{\"id_indikator\":3,\"nilai\":\"Ya\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(105, 1, 8, 1, '2019-06-01 08:55:46', '2019-06-01 08:55:46', '[{\"id_siswa\":\"Athallah Rayhanunnabih Rakha Mahakrisnan\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Tidak\"},{\"id_indikator\":2,\"nilai\":\"Tidak\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Ya\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(106, 1, 8, 1, '2019-06-01 08:55:46', '2019-06-01 08:55:46', '[{\"id_siswa\":\"Azka Bari Prasetya\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Tidak\"},{\"id_indikator\":2,\"nilai\":\"Tidak\"},{\"id_indikator\":3,\"nilai\":\"Ya\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Ya\"}]}]'),
(107, 1, 8, 1, '2019-06-01 08:55:46', '2019-06-01 08:55:46', '[{\"id_siswa\":\"Bintang Dhanayaksa P\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Tidak\"},{\"id_indikator\":2,\"nilai\":\"Ya\"},{\"id_indikator\":3,\"nilai\":\"Ya\"},{\"id_indikator\":4,\"nilai\":\"Ya\"},{\"id_indikator\":5,\"nilai\":\"Ya\"},{\"id_indikator\":6,\"nilai\":\"Ya\"},{\"id_indikator\":7,\"nilai\":\"Ya\"},{\"id_indikator\":8,\"nilai\":\"Ya\"},{\"id_indikator\":9,\"nilai\":\"Ya\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(108, 16, 35, 1, '2019-06-01 09:01:01', '2019-06-01 09:01:01', '[{\"id_siswa\":\"Achmad Tri Alfariz\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Ya\"},{\"id_indikator\":2,\"nilai\":\"Ya\"},{\"id_indikator\":3,\"nilai\":\"Ya\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(109, 16, 35, 1, '2019-06-01 09:01:01', '2019-06-01 09:01:01', '[{\"id_siswa\":\"Alexandra Dear Maris\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Ya\"},{\"id_indikator\":2,\"nilai\":\"Tidak\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Ya\"}]}]'),
(110, 16, 35, 1, '2019-06-01 09:01:01', '2019-06-01 09:01:01', '[{\"id_siswa\":\"Alifia Gusmiar\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Tidak\"},{\"id_indikator\":2,\"nilai\":\"Tidak\"},{\"id_indikator\":3,\"nilai\":\"Ya\"},{\"id_indikator\":4,\"nilai\":\"Ya\"},{\"id_indikator\":5,\"nilai\":\"Ya\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(111, 16, 35, 1, '2019-06-02 09:02:04', '2019-06-01 09:02:04', '[{\"id_siswa\":\"Achmad Tri Alfariz\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Tidak\"},{\"id_indikator\":2,\"nilai\":\"Tidak\"},{\"id_indikator\":3,\"nilai\":\"Ya\"},{\"id_indikator\":4,\"nilai\":\"Ya\"},{\"id_indikator\":5,\"nilai\":\"Ya\"},{\"id_indikator\":6,\"nilai\":\"Ya\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(112, 16, 35, 1, '2019-06-02 09:02:04', '2019-06-01 09:02:04', '[{\"id_siswa\":\"Alexandra Dear Maris\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Ya\"},{\"id_indikator\":2,\"nilai\":\"Tidak\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Ya\"}]}]'),
(113, 16, 35, 1, '2019-06-02 09:02:04', '2019-06-01 09:02:04', '[{\"id_siswa\":\"Alifia Gusmiar\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Ya\"},{\"id_indikator\":2,\"nilai\":\"Tidak\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Ya\"},{\"id_indikator\":6,\"nilai\":\"Ya\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Ya\"}]}]'),
(114, 16, 35, 1, '2019-06-03 09:03:24', '2019-06-01 09:03:24', '[{\"id_siswa\":\"Achmad Tri Alfariz\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Ya\"},{\"id_indikator\":2,\"nilai\":\"Ya\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(115, 16, 35, 1, '2019-06-03 09:03:24', '2019-06-01 09:03:24', '[{\"id_siswa\":\"Alexandra Dear Maris\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Tidak\"},{\"id_indikator\":2,\"nilai\":\"Tidak\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Ya\"},{\"id_indikator\":10,\"nilai\":\"Ya\"}]}]'),
(116, 16, 35, 1, '2019-06-03 09:03:24', '2019-06-01 09:03:24', '[{\"id_siswa\":\"Alifia Gusmiar\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Ya\"},{\"id_indikator\":2,\"nilai\":\"Ya\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(117, 16, 35, 1, '2019-06-04 09:04:53', '2019-06-01 09:04:53', '[{\"id_siswa\":\"Achmad Tri Alfariz\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Ya\"},{\"id_indikator\":2,\"nilai\":\"Tidak\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Ya\"},{\"id_indikator\":6,\"nilai\":\"Ya\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Ya\"}]}]'),
(118, 16, 35, 1, '2019-06-04 09:04:53', '2019-06-01 09:04:53', '[{\"id_siswa\":\"Alexandra Dear Maris\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Tidak\"},{\"id_indikator\":2,\"nilai\":\"Tidak\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Ya\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(119, 16, 35, 1, '2019-06-04 09:04:53', '2019-06-01 09:04:53', '[{\"id_siswa\":\"Alifia Gusmiar\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Tidak\"},{\"id_indikator\":2,\"nilai\":\"Ya\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Ya\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(120, 16, 35, 1, '2019-06-05 09:06:03', '2019-06-01 09:06:03', '[{\"id_siswa\":\"Achmad Tri Alfariz\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Tidak\"},{\"id_indikator\":2,\"nilai\":\"Tidak\"},{\"id_indikator\":3,\"nilai\":\"Ya\"},{\"id_indikator\":4,\"nilai\":\"Ya\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(121, 16, 35, 1, '2019-06-05 09:06:04', '2019-06-01 09:06:04', '[{\"id_siswa\":\"Alexandra Dear Maris\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Tidak\"},{\"id_indikator\":2,\"nilai\":\"Tidak\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Ya\"},{\"id_indikator\":6,\"nilai\":\"Ya\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(122, 16, 35, 1, '2019-06-05 09:06:04', '2019-06-01 09:06:04', '[{\"id_siswa\":\"Alifia Gusmiar\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Tidak\"},{\"id_indikator\":2,\"nilai\":\"Tidak\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Ya\"},{\"id_indikator\":8,\"nilai\":\"Ya\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(123, 16, 35, 1, '2019-06-06 09:07:22', '2019-06-01 09:07:22', '[{\"id_siswa\":\"Achmad Tri Alfariz\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Ya\"},{\"id_indikator\":2,\"nilai\":\"Ya\"},{\"id_indikator\":3,\"nilai\":\"Ya\"},{\"id_indikator\":4,\"nilai\":\"Ya\"},{\"id_indikator\":5,\"nilai\":\"Ya\"},{\"id_indikator\":6,\"nilai\":\"Ya\"},{\"id_indikator\":7,\"nilai\":\"Ya\"},{\"id_indikator\":8,\"nilai\":\"Ya\"},{\"id_indikator\":9,\"nilai\":\"Ya\"},{\"id_indikator\":10,\"nilai\":\"Ya\"}]}]'),
(124, 16, 35, 1, '2019-06-06 09:07:23', '2019-06-01 09:07:23', '[{\"id_siswa\":\"Alexandra Dear Maris\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Tidak\"},{\"id_indikator\":2,\"nilai\":\"Tidak\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Ya\"},{\"id_indikator\":5,\"nilai\":\"Ya\"},{\"id_indikator\":6,\"nilai\":\"Ya\"},{\"id_indikator\":7,\"nilai\":\"Ya\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(125, 16, 35, 1, '2019-06-06 09:07:23', '2019-06-01 09:07:23', '[{\"id_siswa\":\"Alifia Gusmiar\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Tidak\"},{\"id_indikator\":2,\"nilai\":\"Tidak\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Ya\"},{\"id_indikator\":6,\"nilai\":\"Ya\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(126, 16, 35, 1, '2019-06-07 09:07:42', '2019-06-01 09:07:42', '[{\"id_siswa\":\"Achmad Tri Alfariz\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Ya\"},{\"id_indikator\":2,\"nilai\":\"Tidak\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(127, 16, 35, 1, '2019-06-07 09:07:43', '2019-06-01 09:07:43', '[{\"id_siswa\":\"Alexandra Dear Maris\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Ya\"},{\"id_indikator\":2,\"nilai\":\"Tidak\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(128, 16, 35, 1, '2019-06-07 09:07:43', '2019-06-01 09:07:43', '[{\"id_siswa\":\"Alifia Gusmiar\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Ya\"},{\"id_indikator\":2,\"nilai\":\"Tidak\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(135, 16, 35, 1, '2019-06-10 09:16:08', '2019-06-01 09:16:08', '[{\"id_siswa\":\"Achmad Tri Alfariz\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Ya\"},{\"id_indikator\":2,\"nilai\":\"Ya\"},{\"id_indikator\":3,\"nilai\":\"Ya\"},{\"id_indikator\":4,\"nilai\":\"Ya\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(136, 16, 35, 1, '2019-06-10 09:16:08', '2019-06-01 09:16:08', '[{\"id_siswa\":\"Alexandra Dear Maris\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Ya\"},{\"id_indikator\":2,\"nilai\":\"Ya\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(137, 16, 35, 1, '2019-06-10 09:16:08', '2019-06-01 09:16:08', '[{\"id_siswa\":\"Alifia Gusmiar\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Ya\"},{\"id_indikator\":2,\"nilai\":\"Tidak\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(138, 16, 35, 1, '2019-06-11 09:18:40', '2019-06-01 09:18:40', '[{\"id_siswa\":\"Achmad Tri Alfariz\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Tidak\"},{\"id_indikator\":2,\"nilai\":\"Tidak\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(139, 16, 35, 1, '2019-06-11 09:18:40', '2019-06-01 09:18:40', '[{\"id_siswa\":\"Alexandra Dear Maris\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Tidak\"},{\"id_indikator\":2,\"nilai\":\"Tidak\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(140, 16, 35, 1, '2019-06-11 09:18:40', '2019-06-01 09:18:40', '[{\"id_siswa\":\"Alifia Gusmiar\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Tidak\"},{\"id_indikator\":2,\"nilai\":\"Tidak\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(141, 16, 35, 1, '2019-06-12 09:19:50', '2019-06-01 09:19:50', '[{\"id_siswa\":\"Achmad Tri Alfariz\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Tidak\"},{\"id_indikator\":2,\"nilai\":\"Tidak\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(142, 16, 35, 1, '2019-06-12 09:19:51', '2019-06-01 09:19:51', '[{\"id_siswa\":\"Alexandra Dear Maris\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Tidak\"},{\"id_indikator\":2,\"nilai\":\"Tidak\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(143, 16, 35, 1, '2019-06-12 09:19:51', '2019-06-01 09:19:51', '[{\"id_siswa\":\"Alifia Gusmiar\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Tidak\"},{\"id_indikator\":2,\"nilai\":\"Tidak\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(144, 16, 35, 1, '2019-06-13 09:21:23', '2019-06-01 09:21:23', '[{\"id_siswa\":\"Achmad Tri Alfariz\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Ya\"},{\"id_indikator\":2,\"nilai\":\"Tidak\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(145, 16, 35, 1, '2019-06-13 09:21:23', '2019-06-01 09:21:23', '[{\"id_siswa\":\"Alexandra Dear Maris\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Ya\"},{\"id_indikator\":2,\"nilai\":\"Ya\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(146, 16, 35, 1, '2019-06-13 09:21:23', '2019-06-01 09:21:23', '[{\"id_siswa\":\"Alifia Gusmiar\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Ya\"},{\"id_indikator\":2,\"nilai\":\"Ya\"},{\"id_indikator\":3,\"nilai\":\"Ya\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(147, 16, 35, 1, '2019-06-14 09:22:36', '2019-06-01 09:22:36', '[{\"id_siswa\":\"Achmad Tri Alfariz\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Ya\"},{\"id_indikator\":2,\"nilai\":\"Ya\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(148, 16, 35, 1, '2019-06-14 09:22:36', '2019-06-01 09:22:36', '[{\"id_siswa\":\"Alexandra Dear Maris\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Ya\"},{\"id_indikator\":2,\"nilai\":\"Ya\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(149, 16, 35, 1, '2019-06-14 09:22:36', '2019-06-01 09:22:36', '[{\"id_siswa\":\"Alifia Gusmiar\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Ya\"},{\"id_indikator\":2,\"nilai\":\"Ya\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(150, 16, 35, 1, '2019-06-17 09:23:43', '2019-06-01 09:23:43', '[{\"id_siswa\":\"Achmad Tri Alfariz\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Ya\"},{\"id_indikator\":2,\"nilai\":\"Ya\"},{\"id_indikator\":3,\"nilai\":\"Ya\"},{\"id_indikator\":4,\"nilai\":\"Ya\"},{\"id_indikator\":5,\"nilai\":\"Ya\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Ya\"},{\"id_indikator\":9,\"nilai\":\"Ya\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(151, 16, 35, 1, '2019-06-17 09:23:43', '2019-06-01 09:23:43', '[{\"id_siswa\":\"Alexandra Dear Maris\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Ya\"},{\"id_indikator\":2,\"nilai\":\"Ya\"},{\"id_indikator\":3,\"nilai\":\"Ya\"},{\"id_indikator\":4,\"nilai\":\"Ya\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(152, 16, 35, 1, '2019-06-17 09:23:43', '2019-06-01 09:23:43', '[{\"id_siswa\":\"Alifia Gusmiar\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Ya\"},{\"id_indikator\":2,\"nilai\":\"Tidak\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(153, 16, 35, 1, '2019-06-15 09:26:14', '2019-06-01 09:26:14', '[{\"id_siswa\":\"Achmad Tri Alfariz\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Tidak\"},{\"id_indikator\":2,\"nilai\":\"Tidak\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(154, 16, 35, 1, '2019-06-15 09:26:14', '2019-06-01 09:26:14', '[{\"id_siswa\":\"Alexandra Dear Maris\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Tidak\"},{\"id_indikator\":2,\"nilai\":\"Tidak\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(155, 16, 35, 1, '2019-06-15 09:26:14', '2019-06-01 09:26:14', '[{\"id_siswa\":\"Alifia Gusmiar\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Tidak\"},{\"id_indikator\":2,\"nilai\":\"Tidak\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(156, 16, 35, 1, '2019-06-16 09:26:24', '2019-06-01 09:26:24', '[{\"id_siswa\":\"Achmad Tri Alfariz\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Tidak\"},{\"id_indikator\":2,\"nilai\":\"Tidak\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(157, 16, 35, 1, '2019-06-16 09:26:24', '2019-06-01 09:26:24', '[{\"id_siswa\":\"Alexandra Dear Maris\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Tidak\"},{\"id_indikator\":2,\"nilai\":\"Tidak\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(158, 16, 35, 1, '2019-06-16 09:26:24', '2019-06-01 09:26:24', '[{\"id_siswa\":\"Alifia Gusmiar\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Tidak\"},{\"id_indikator\":2,\"nilai\":\"Tidak\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(159, 16, 35, 1, '2019-06-18 09:28:11', '2019-06-01 09:28:11', '[{\"id_siswa\":\"Achmad Tri Alfariz\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Ya\"},{\"id_indikator\":2,\"nilai\":\"Ya\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(160, 16, 35, 1, '2019-06-18 09:28:11', '2019-06-01 09:28:11', '[{\"id_siswa\":\"Alexandra Dear Maris\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Ya\"},{\"id_indikator\":2,\"nilai\":\"Ya\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(161, 16, 35, 1, '2019-06-18 09:28:11', '2019-06-01 09:28:11', '[{\"id_siswa\":\"Alifia Gusmiar\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Ya\"},{\"id_indikator\":2,\"nilai\":\"Ya\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(162, 16, 35, 1, '2019-06-19 09:28:36', '2019-06-01 09:28:36', '[{\"id_siswa\":\"Achmad Tri Alfariz\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Ya\"},{\"id_indikator\":2,\"nilai\":\"Ya\"},{\"id_indikator\":3,\"nilai\":\"Ya\"},{\"id_indikator\":4,\"nilai\":\"Ya\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(163, 16, 35, 1, '2019-06-19 09:28:36', '2019-06-01 09:28:36', '[{\"id_siswa\":\"Alexandra Dear Maris\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Tidak\"},{\"id_indikator\":2,\"nilai\":\"Ya\"},{\"id_indikator\":3,\"nilai\":\"Ya\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(164, 16, 35, 1, '2019-06-19 09:28:36', '2019-06-01 09:28:36', '[{\"id_siswa\":\"Alifia Gusmiar\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Tidak\"},{\"id_indikator\":2,\"nilai\":\"Tidak\"},{\"id_indikator\":3,\"nilai\":\"Ya\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(165, 16, 35, 1, '2019-06-20 09:31:30', '2019-06-01 09:31:30', '[{\"id_siswa\":\"Achmad Tri Alfariz\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Ya\"},{\"id_indikator\":2,\"nilai\":\"Tidak\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(166, 16, 35, 1, '2019-06-20 09:31:31', '2019-06-01 09:31:31', '[{\"id_siswa\":\"Alexandra Dear Maris\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Ya\"},{\"id_indikator\":2,\"nilai\":\"Tidak\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(167, 16, 35, 1, '2019-06-20 09:31:31', '2019-06-01 09:31:31', '[{\"id_siswa\":\"Alifia Gusmiar\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Ya\"},{\"id_indikator\":2,\"nilai\":\"Tidak\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(168, 16, 35, 1, '2019-06-21 09:32:39', '2019-06-01 09:32:39', '[{\"id_siswa\":\"Achmad Tri Alfariz\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Tidak\"},{\"id_indikator\":2,\"nilai\":\"Ya\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(169, 16, 35, 1, '2019-06-21 09:32:39', '2019-06-01 09:32:39', '[{\"id_siswa\":\"Alexandra Dear Maris\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Tidak\"},{\"id_indikator\":2,\"nilai\":\"Ya\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Ya\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(170, 16, 35, 1, '2019-06-21 09:32:39', '2019-06-01 09:32:39', '[{\"id_siswa\":\"Alifia Gusmiar\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Tidak\"},{\"id_indikator\":2,\"nilai\":\"Tidak\"},{\"id_indikator\":3,\"nilai\":\"Ya\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Ya\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(171, 16, 35, 1, '2019-06-22 09:32:51', '2019-06-01 09:32:51', '[{\"id_siswa\":\"Achmad Tri Alfariz\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Tidak\"},{\"id_indikator\":2,\"nilai\":\"Tidak\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(172, 16, 35, 1, '2019-06-22 09:32:51', '2019-06-01 09:32:51', '[{\"id_siswa\":\"Alexandra Dear Maris\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Tidak\"},{\"id_indikator\":2,\"nilai\":\"Tidak\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(173, 16, 35, 1, '2019-06-22 09:32:51', '2019-06-01 09:32:51', '[{\"id_siswa\":\"Alifia Gusmiar\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Tidak\"},{\"id_indikator\":2,\"nilai\":\"Tidak\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(174, 16, 35, 1, '2019-06-23 09:33:02', '2019-06-01 09:33:02', '[{\"id_siswa\":\"Achmad Tri Alfariz\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Tidak\"},{\"id_indikator\":2,\"nilai\":\"Tidak\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(175, 16, 35, 1, '2019-06-23 09:33:02', '2019-06-01 09:33:02', '[{\"id_siswa\":\"Alexandra Dear Maris\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Tidak\"},{\"id_indikator\":2,\"nilai\":\"Tidak\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(176, 16, 35, 1, '2019-06-23 09:33:02', '2019-06-01 09:33:02', '[{\"id_siswa\":\"Alifia Gusmiar\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Tidak\"},{\"id_indikator\":2,\"nilai\":\"Tidak\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(177, 16, 35, 1, '2019-06-24 09:33:27', '2019-06-01 09:33:27', '[{\"id_siswa\":\"Achmad Tri Alfariz\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Ya\"},{\"id_indikator\":2,\"nilai\":\"Tidak\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Ya\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(178, 16, 35, 1, '2019-06-24 09:33:27', '2019-06-01 09:33:27', '[{\"id_siswa\":\"Alexandra Dear Maris\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Tidak\"},{\"id_indikator\":2,\"nilai\":\"Tidak\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Ya\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Ya\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(179, 16, 35, 1, '2019-06-24 09:33:27', '2019-06-01 09:33:27', '[{\"id_siswa\":\"Alifia Gusmiar\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Tidak\"},{\"id_indikator\":2,\"nilai\":\"Tidak\"},{\"id_indikator\":3,\"nilai\":\"Ya\"},{\"id_indikator\":4,\"nilai\":\"Ya\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(180, 16, 35, 1, '2019-06-25 09:36:21', '2019-06-01 09:36:21', '[{\"id_siswa\":\"Achmad Tri Alfariz\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Ya\"},{\"id_indikator\":2,\"nilai\":\"Tidak\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(181, 16, 35, 1, '2019-06-25 09:36:21', '2019-06-01 09:36:21', '[{\"id_siswa\":\"Alexandra Dear Maris\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Ya\"},{\"id_indikator\":2,\"nilai\":\"Tidak\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(182, 16, 35, 1, '2019-06-25 09:36:21', '2019-06-01 09:36:21', '[{\"id_siswa\":\"Alifia Gusmiar\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Ya\"},{\"id_indikator\":2,\"nilai\":\"Tidak\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(183, 16, 35, 1, '2019-06-26 09:36:45', '2019-06-01 09:36:45', '[{\"id_siswa\":\"Achmad Tri Alfariz\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Ya\"},{\"id_indikator\":2,\"nilai\":\"Tidak\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Ya\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(184, 16, 35, 1, '2019-06-26 09:36:45', '2019-06-01 09:36:45', '[{\"id_siswa\":\"Alexandra Dear Maris\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Tidak\"},{\"id_indikator\":2,\"nilai\":\"Tidak\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Ya\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Ya\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(185, 16, 35, 1, '2019-06-26 09:36:45', '2019-06-01 09:36:45', '[{\"id_siswa\":\"Alifia Gusmiar\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Tidak\"},{\"id_indikator\":2,\"nilai\":\"Tidak\"},{\"id_indikator\":3,\"nilai\":\"Ya\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Ya\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(186, 16, 35, 1, '2019-06-27 09:37:07', '2019-06-01 09:37:07', '[{\"id_siswa\":\"Achmad Tri Alfariz\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Ya\"},{\"id_indikator\":2,\"nilai\":\"Ya\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(187, 16, 35, 1, '2019-06-27 09:37:08', '2019-06-01 09:37:08', '[{\"id_siswa\":\"Alexandra Dear Maris\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Ya\"},{\"id_indikator\":2,\"nilai\":\"Tidak\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(188, 16, 35, 1, '2019-06-27 09:37:08', '2019-06-01 09:37:08', '[{\"id_siswa\":\"Alifia Gusmiar\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Tidak\"},{\"id_indikator\":2,\"nilai\":\"Ya\"},{\"id_indikator\":3,\"nilai\":\"Ya\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(189, 16, 35, 1, '2019-06-28 09:38:38', '2019-06-01 09:38:38', '[{\"id_siswa\":\"Achmad Tri Alfariz\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Ya\"},{\"id_indikator\":2,\"nilai\":\"Tidak\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(190, 16, 35, 1, '2019-06-28 09:38:38', '2019-06-01 09:38:38', '[{\"id_siswa\":\"Alexandra Dear Maris\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Tidak\"},{\"id_indikator\":2,\"nilai\":\"Tidak\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Ya\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(191, 16, 35, 1, '2019-06-28 09:38:38', '2019-06-01 09:38:38', '[{\"id_siswa\":\"Alifia Gusmiar\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Tidak\"},{\"id_indikator\":2,\"nilai\":\"Tidak\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Ya\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(192, 16, 35, 1, '2019-06-29 09:38:49', '2019-06-01 09:38:49', '[{\"id_siswa\":\"Achmad Tri Alfariz\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Tidak\"},{\"id_indikator\":2,\"nilai\":\"Tidak\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(193, 16, 35, 1, '2019-06-29 09:38:50', '2019-06-01 09:38:50', '[{\"id_siswa\":\"Alexandra Dear Maris\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Tidak\"},{\"id_indikator\":2,\"nilai\":\"Tidak\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(194, 16, 35, 1, '2019-06-29 09:38:50', '2019-06-01 09:38:50', '[{\"id_siswa\":\"Alifia Gusmiar\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Tidak\"},{\"id_indikator\":2,\"nilai\":\"Tidak\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(195, 16, 35, 1, '2019-06-30 09:39:09', '2019-06-01 09:39:09', '[{\"id_siswa\":\"Achmad Tri Alfariz\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Tidak\"},{\"id_indikator\":2,\"nilai\":\"Tidak\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]');
INSERT INTO `tb_nilai_karakter` (`ID_NILAI`, `ID_KELAS`, `ID_USER`, `ID_KARAKTER`, `CREATE_AT`, `UPDATE_AT`, `NILAI`) VALUES
(196, 16, 35, 1, '2019-06-30 09:39:09', '2019-06-01 09:39:09', '[{\"id_siswa\":\"Alexandra Dear Maris\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Tidak\"},{\"id_indikator\":2,\"nilai\":\"Tidak\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(197, 16, 35, 1, '2019-06-30 09:39:09', '2019-06-01 09:39:09', '[{\"id_siswa\":\"Alifia Gusmiar\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Tidak\"},{\"id_indikator\":2,\"nilai\":\"Tidak\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(201, 16, 35, 1, '2019-06-08 09:42:19', '2019-06-01 09:42:19', '[{\"id_siswa\":\"Achmad Tri Alfariz\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Tidak\"},{\"id_indikator\":2,\"nilai\":\"Tidak\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(202, 16, 35, 1, '2019-06-08 09:42:19', '2019-06-01 09:42:19', '[{\"id_siswa\":\"Alexandra Dear Maris\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Tidak\"},{\"id_indikator\":2,\"nilai\":\"Tidak\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(203, 16, 35, 1, '2019-06-08 09:42:19', '2019-06-01 09:42:19', '[{\"id_siswa\":\"Alifia Gusmiar\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Tidak\"},{\"id_indikator\":2,\"nilai\":\"Tidak\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(204, 16, 35, 1, '2019-06-09 09:42:30', '2019-06-01 09:42:30', '[{\"id_siswa\":\"Achmad Tri Alfariz\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Tidak\"},{\"id_indikator\":2,\"nilai\":\"Tidak\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(205, 16, 35, 1, '2019-06-08 17:00:00', '2019-06-01 09:42:30', '[{\"id_siswa\":\"Alexandra Dear Maris\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Tidak\"},{\"id_indikator\":2,\"nilai\":\"Tidak\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]'),
(206, 16, 35, 1, '2019-06-09 09:42:30', '2019-06-01 09:42:30', '[{\"id_siswa\":\"Alifia Gusmiar\",\"id_karakter\":1,\"nilai\":[{\"id_indikator\":1,\"nilai\":\"Tidak\"},{\"id_indikator\":2,\"nilai\":\"Tidak\"},{\"id_indikator\":3,\"nilai\":\"Tidak\"},{\"id_indikator\":4,\"nilai\":\"Tidak\"},{\"id_indikator\":5,\"nilai\":\"Tidak\"},{\"id_indikator\":6,\"nilai\":\"Tidak\"},{\"id_indikator\":7,\"nilai\":\"Tidak\"},{\"id_indikator\":8,\"nilai\":\"Tidak\"},{\"id_indikator\":9,\"nilai\":\"Tidak\"},{\"id_indikator\":10,\"nilai\":\"Tidak\"}]}]');

-- --------------------------------------------------------

--
-- Table structure for table `tb_nilai_point`
--

CREATE TABLE `tb_nilai_point` (
  `ID_POINT` int(11) NOT NULL,
  `ID_SISWA` int(11) DEFAULT NULL,
  `ID_USER` int(11) DEFAULT NULL,
  `ID_KELAS` int(11) DEFAULT NULL,
  `NILAI1` int(11) DEFAULT NULL,
  `NILAI2` int(11) DEFAULT NULL,
  `NILAI3` int(11) DEFAULT NULL,
  `CREATE_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UPDATE_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_presensi`
--

CREATE TABLE `tb_presensi` (
  `ID_PRESENSI` int(11) NOT NULL,
  `ID_USER` int(11) DEFAULT NULL,
  `ID_KELAS` int(11) DEFAULT NULL,
  `TGL` date DEFAULT NULL,
  `CREATE_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UPDATE_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_siswa`
--

CREATE TABLE `tb_siswa` (
  `ID_SISWA` int(11) NOT NULL,
  `NOMOR_INDUK` varchar(11) DEFAULT NULL,
  `ID_KELAS` int(11) DEFAULT NULL,
  `NAMA_SISWA` varchar(100) DEFAULT NULL,
  `ALAMAT` varchar(100) DEFAULT NULL,
  `JENIS_KELAMIN` varchar(20) DEFAULT NULL,
  `PASSWORD` int(11) DEFAULT NULL,
  `CREATE_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UPDATE_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_siswa`
--

INSERT INTO `tb_siswa` (`ID_SISWA`, `NOMOR_INDUK`, `ID_KELAS`, `NAMA_SISWA`, `ALAMAT`, `JENIS_KELAMIN`, `PASSWORD`, `CREATE_AT`, `UPDATE_AT`) VALUES
(1, '318006', 1, 'Ahmad Fatikh Ramdhani', 'Dungus RT.11 RW.03 sukodono,sidoarjo', 'L', 318006, '2019-05-11 09:48:23', '2019-05-11 09:48:23'),
(2, '318007', 1, 'Ahsani Taqwim Sucahyono', 'Perum Oma Pesona Buduran i1o No.03 Buduran, \r\nsidoarjo ', 'P', 318007, '2019-05-11 09:48:23', '2019-05-11 09:48:23'),
(3, '318012', 1, 'Almira Azalia Marga', 'Griya Kebon Agung 1 C5\r\nRT.33 RW.08 Kebon Agung, Sukodono', 'P', 318012, '2019-05-11 09:48:23', '2019-05-11 09:48:23'),
(4, '318013', 1, 'Almira Vidi Zafira', 'Almira Vidi Zafira ', 'P', 318013, '2019-05-11 09:48:23', '2019-05-11 09:48:23'),
(5, '318015', 1, 'Anggito Abimanyu', 'Perum Wahyu Taman Sari Rogo \r\nrt 27 rw 06 Sumput, Sukdomo.', 'L', 318015, '2019-05-11 09:48:23', '2019-05-11 09:48:23'),
(6, '318016', 1, 'Aqilah Nadhifa Putri Ibrahim', 'Graha Mutiara Blok B2 12A RT.41 Kebon Agung,Sukodono', 'P', 318016, '2019-05-11 09:48:23', '2019-05-11 09:48:23'),
(7, '318018', 1, 'Arrayyan Muhammad Ghifarry D', 'Perum Surya Asri 2 D9-51\r\nRT.36 RW.11 Jumputrejo, Sukodono', 'L', 318018, '2019-05-11 09:48:23', '2019-05-11 09:48:23'),
(8, '318020', 1, 'Athallah Rayhanunnabih Rakha Mahakrisnan', 'Perum Mutiara Residence RT.02 RW.1 Anggaswangi,Sukodono', 'L', 318020, '2019-05-11 09:48:23', '2019-05-11 09:48:23'),
(9, '318021', 1, 'Azka Bari Prasetya', 'Perum Graha Asri Jl. Duku AQ/11\r\nRT.34 RW.10 Pekarungan - Sukodono', 'L', 318021, '2019-05-11 10:02:16', '2019-05-11 10:02:16'),
(10, '318022', 1, 'Bintang Dhanayaksa P', 'Citra City D3/28\r\nRT.25 RW.07 Sukodono', 'L', 318022, '2019-05-11 10:02:16', '2019-05-11 10:02:16'),
(11, '318002', 2, 'Achmad Wildan Mudakkir', 'Cemengkalang RT.02 RW.01 Cemeng Kalang - Sidoarjo', 'L', 318002, '2019-05-11 11:02:00', '2019-05-11 11:02:00'),
(12, '318003', 2, 'Achmad Zidan Arkana Rachman', 'Perum Barungu C/9 RT.21 RW.05 Ngaresrejo - Sukodono', 'L', 318003, '2019-05-11 11:02:00', '2019-05-11 11:02:00'),
(13, '318005', 2, 'Aditya Rayyan Dimas Aryanto', 'Graha Asri Sukodono CM 20\r\nRT.52 RW.14 Pekarungan - Sukodono', 'L', 318005, '2019-05-11 11:02:00', '2019-05-11 11:02:00'),
(14, '318001', 3, 'Achmad Januar Ardiyanto', 'Perum Graha Arsi Sukodono\r\nRT.52 RW.14 ', 'L', 318001, '2019-05-18 15:21:06', '2019-05-18 15:21:06'),
(15, '318004', 3, 'Ade Lien Sevi Aisyah Aqillah', 'Balong Biru RT. 13 RW. 04 ', 'P', 318004, '2019-05-18 15:21:06', '2019-05-18 15:21:06'),
(16, '318008', 3, 'Akbara Michael Subhan', 'Pondok Trosobo Indah  ', 'L', 318008, '2019-05-18 15:21:06', '2019-05-18 15:21:06'),
(17, '317005', 4, 'Adis Fadhil Rafasya', 'Dusun Sidorogo RT 03 RW 06 Taman - Sidoarjo', 'L', 317005, '2019-05-18 15:32:08', '2019-05-18 15:32:08'),
(18, '317009', 4, 'Alfiyah Batrisya Sunaryo Putri', 'Desa Anggaswangi RT 06 RW 03 ', 'P', 317009, '2019-05-18 15:50:47', '2019-05-18 15:50:47'),
(19, '317011', 4, 'Arafa Zizi Setiabudi', 'Perum Mutiara Residence Blok D1 No. 12 ', 'P', 317011, '2019-05-18 15:50:47', '2019-05-18 15:50:47'),
(20, '317001', 5, 'Abelia Cintha Anandasena', 'Graha Asri Sukodono Blok AQ No. 01 ', 'P', 317001, '2019-05-18 15:50:47', '2019-05-18 15:50:47'),
(21, '317008', 5, 'Alfan Arham Putra', 'Perum Bhayangkara Permai Blok CC No. 12 ', 'L', 317008, '2019-05-18 15:50:47', '2019-05-18 15:50:47'),
(22, '317075', 5, 'Anindya Nabila Subrani', 'Perum Surya Asri 2 Blok EG no. 27 ', 'P', 317075, '2019-05-18 15:50:47', '2019-05-18 15:50:47'),
(23, '317002', 6, 'Achmad Amir Alisyahbana', 'Jl. Nangka RT 01 RW 01 Pekarungan Sukodono - Sidoarjo', 'L', 317002, '2019-05-18 15:50:47', '2019-05-18 15:50:47'),
(24, '317003', 6, 'Achmad Wafa Kurniawan', 'Perum Surya Asri 1 Blok A2 No. 05 ', 'L', 317003, '2019-05-18 15:50:47', '2019-05-18 15:50:47'),
(25, '317004', 6, 'Adelia Istigfarina Zidni Syarifah', 'Sumput RT 09 RW 03 sidoarjo', 'p', 317004, '2019-05-18 15:50:47', '2019-05-18 15:50:47'),
(26, '316003', 7, 'Adwa Ismail Arasyah', 'Graha Mutiara Blok B6 No. 7 ', 'L', 316003, '2019-05-19 07:26:58', '2019-05-19 07:26:58'),
(27, '316004', 7, 'Aldino Mikail Fayyadh', 'Griyaloka Blok F1 No. 22 \r\n', 'L', 316004, '2019-05-19 07:26:58', '2019-05-19 07:26:58'),
(28, '316008', 7, 'Anabella Aurel Dhyajaya', 'Graha Mutiara C2 No. 21 ', 'P', 316008, '2019-05-19 07:26:58', '2019-05-19 07:26:58'),
(29, '316002', 8, 'Achmad Rafiif', 'Sambung Rejo Ngares Rejo, Sukodono - Sidoarjo\r\n', 'L', 316002, '2019-05-19 07:26:58', '2019-05-19 07:26:58'),
(30, '316007', 8, 'Ameera Natashafira', 'Pademonegoro RT 07 RW 02 \r\n', 'P', 316007, '2019-05-19 07:26:58', '2019-05-19 07:26:58'),
(31, '316010', 8, 'Arva Athallah', 'Graha Asri Sukodono BJ No. 9 \r\n', 'L', 316010, '2019-05-19 07:26:58', '2019-05-19 07:26:58'),
(32, '316001', 9, 'Abelya Maalil Fazrina ', 'Jl. KH. Mansyur RT 18 RW 06 \r\n', 'P', 316001, '2019-05-19 07:26:58', '2019-05-19 07:26:58'),
(33, '316005', 9, 'Alif Brilyan Pahlevi', 'Citra Surya Mas Blok B4 No. 1 \r\n', 'L', 316005, '2019-05-19 07:26:58', '2019-05-19 07:26:58'),
(34, '316006', 9, 'Almira Tsabitah Candrarini', 'Citra Harmoni Blok I 8 No. 42 \r\n', 'P', 316006, '2019-05-19 07:26:58', '2019-05-19 07:26:58'),
(35, '315003', 10, 'Achmad Rizqy Aldiansyah Wahyudi', 'Perum. Graha Mutiara Blok C2 No. 14-C \r\n', 'L', 315003, '2019-05-19 07:40:02', '2019-05-19 07:40:02'),
(36, '315004', 10, 'Affrel Muhadzdzib', 'Graha Asri Sukodono Blok E No. 10 \r\n', 'L', 315004, '2019-05-19 07:40:02', '2019-05-19 07:40:02'),
(37, '315009', 10, 'Alfathir Aghna Kaffa', 'Graha Mutiara Blok C1 No. 1 \r\n', 'L', 315009, '2019-05-19 07:40:02', '2019-05-19 07:40:02'),
(38, '315001', 11, 'Abdiela Faviansyah', 'Sukodono Dian Regency Jl. Anugerah V No. 20 - 22 \r\n', 'L', 315001, '2019-05-19 07:40:02', '2019-05-19 07:40:02'),
(39, '315002', 11, 'Abrisam Zafrano Farozdaq', 'Istana Mentari Blok C2 No. 6 Cemengkalang Rt. 11 Rw. 02 \r\n', 'L', 315002, '2019-05-19 07:40:02', '2019-05-19 07:40:02'),
(40, '315013', 11, 'Alyssa Nabilatus Shifa', 'Permata Sukodono Raya Blok C2 No. 40 \r\n', 'P', 315013, '2019-05-19 07:40:02', '2019-05-19 07:40:02'),
(41, '315006', 12, 'Aidin Irsyad Arief', 'Jl. Raya Ngelom No. 62 Rt. 02 Rw. 03 \r\n', 'L', 315006, '2019-05-19 07:40:02', '2019-05-19 07:40:02'),
(42, '315007', 12, 'Aisyah Nayla Safira', 'Suko Biting Rt. 10 Rw. 03 \r\n', 'P', 315007, '2019-05-19 07:40:02', '2019-05-19 07:40:02'),
(43, '315008', 12, 'Aladdin Haidar Abiy Dlaha', 'Graha Asri Sukodono Blok Ah No. 39 \r\n', 'L', 315008, '2019-05-19 07:40:02', '2019-05-19 07:40:02'),
(44, '314005', 13, 'Ahmad Zikra Sasputra', 'Surya Asri 1 A10/14 Rt.36 Rw.03 Buduran Sidoarjo \r\n', 'L', 314005, '2019-05-19 07:51:29', '2019-05-19 07:51:29'),
(45, '314010', 13, 'Al Barraday Laxmana Nala', 'Citra Harmoni F1/28a Rt.35 Rw.07 Sidodadi Taman Sda \r\n', 'L', 314010, '2019-05-19 07:51:29', '2019-05-19 07:51:29'),
(46, '314012', 13, 'Aldenendra Nirwan', 'Villa Jasmine Rt.15 Rw.02 Sumberrejo Wonoayu Sda \r\n', 'L', 314012, '2019-05-19 07:51:29', '2019-05-19 07:51:29'),
(47, '314003', 14, 'Achmad Fahmi Ulumuddin', 'Graha Asri Sukodono J-7rt.26 Rw.08 Pekarungan Sukodono Sda \r\n', 'L', 314003, '2019-05-19 07:51:29', '2019-05-19 07:51:29'),
(48, '314016', 14, 'Ayunda Anindita Azzahra Prihantianta', 'Jatikalang Rt.05 Rw.01 Krian Sidoarjo \r\n', 'P', 314016, '2019-05-19 07:51:29', '2019-05-19 07:51:29'),
(49, '314021', 14, 'Bunga Araminta Anandasena', 'Graha Asri Sukodono Aq/1 Rt.34 Rw.10 Pekarungan Sukodono Sda \r\n', 'P', 314021, '2019-05-19 07:51:29', '2019-05-19 07:51:29'),
(50, '314002', 15, 'Abyan Akram Al-Hakim', 'Pondok Marinir B/6 Masangankulon Rt.19 Rw.07 Sukodono Sda \r\n', 'L', 314002, '2019-05-19 07:51:29', '2019-05-19 07:51:29'),
(51, '314006', 15, 'Ahnaf Abyan Athai Nurillah', 'Sambibulu Rt.01 Rw.01 Taman Sidoarjo \r\n', 'L', 314006, '2019-05-19 07:51:29', '2019-05-19 07:51:29'),
(52, '314009', 15, 'Akihiko Rama Listyanto', 'Oma Pesona Buduran I3/7 Rt.38 Rw.7 Buduran Sda \r\n', 'L', 314009, '2019-05-19 07:51:29', '2019-05-19 07:51:29'),
(53, '313038', 16, 'Achmad Tri Alfariz', 'Karangnongko Rt.01 Rw.01 Pekarungan Sukodono Sda \r\n', 'L', 313038, '2019-05-19 07:57:36', '2019-05-19 07:57:36'),
(54, '313044', 16, 'Alexandra Dear Maris', 'Pekarungan Rt.22 Rw.07 No.29 Sukodono Sidoarjo \r\n', 'P', 313044, '2019-05-19 07:57:36', '2019-05-19 07:57:36'),
(55, '313023', 16, 'Alifia Gusmiar', 'Graha Mutiara B2-43 Sukodono Sidoarjo \r\n', 'P', 313023, '2019-05-19 07:57:36', '2019-05-19 07:57:36'),
(56, '313013', 17, 'Abiyyu Yusuf Alfarizi', 'Jl.Kedungasem Ix/15k Kedungbaruk Rungkut Surabaya \r\n', 'L', 313013, '2019-05-19 07:57:36', '2019-05-19 07:57:36'),
(57, '317007', 17, 'Al Harits Hendra Prayoga', 'Perum Bratang Gede Blok 3I No. 32 \r\n', 'L', 317007, '2019-05-19 07:57:36', '2019-05-19 07:57:36'),
(58, '313046', 17, 'Andhika Tri Wahyu Dinata', 'Jl. Nangka 1 Kav. C/15 Pekarungan Sukodono Sidoarjo \r\n', 'L', 313046, '2019-05-19 07:57:36', '2019-05-19 07:57:36');

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `ID_USER` int(11) NOT NULL,
  `NOMOR_INDUK` varchar(255) NOT NULL,
  `KELAS` int(11) DEFAULT NULL,
  `NAMA_USER` varchar(100) DEFAULT NULL,
  `PASSWORD` int(11) DEFAULT NULL,
  `ALAMAT` varchar(100) DEFAULT NULL,
  `LEVEL` varchar(20) DEFAULT NULL,
  `CREATE_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UPDATE_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`ID_USER`, `NOMOR_INDUK`, `KELAS`, `NAMA_USER`, `PASSWORD`, `ALAMAT`, `LEVEL`, `CREATE_AT`, `UPDATE_AT`) VALUES
(1, '1', NULL, 'Edy Rahmawan,ST', 12345, 'Perum Graha Mutiara A1 No. 05 Kebonagung \r\n   Sukodono-Sidoarjo	\r\n', 'Kepala Sekolah', '2019-05-14 15:26:04', '2019-05-14 15:26:04'),
(2, '2', NULL, 'Nanik Pristiwanti', 12345, 'Desa Sidokepung RT 8 RW 2 \r\nBuduran \r\n', 'Kepala Sekolah', '2019-05-18 14:05:51', '2019-05-18 14:05:51'),
(3, '3', NULL, 'Riyadhotun Ni\'mah', 12345, 'Perum Graha Mutiara A1/05 Sidoarjo', 'Administrator', '2019-05-14 15:38:58', '2019-05-14 15:38:58'),
(4, '4', NULL, 'Nur Choiriyah', 12345, 'Gg. Mawar Sidopurno Sidokepung Buduran - Sidoarjo\r\n', 'Administrator', '2019-05-18 11:36:24', '2019-05-18 11:36:24'),
(5, '5', NULL, 'Sifa Fauziah Permatasari', 12345, 'Perum Citra Surya Mas F-18', 'Administrator', '2019-05-18 11:36:24', '2019-05-18 11:36:24'),
(6, '6', NULL, 'M. Hasyim Asy\'ari', 12345, 'Jl Kolonel Sugiono Rt 2 Rw 1 Kepuhkiriman, Waru-Sidoarjo \r\n', 'Administrator', '2019-05-18 11:36:24', '2019-05-18 11:36:24'),
(7, '7', 1, 'Dwi Indah Puspitasari, S.Hum	', 12345, 'Dsn. Kesemen RT 23 RW 06 Cangkringsari Sukodono	\r\n', 'Guru', '2019-05-14 15:26:04', '2019-05-14 15:26:04'),
(8, '8', 1, 'Uswatun Yusroh', 12345, 'Duran-Karangpuri RT 03  RW 01', 'Guru', '2019-05-18 11:41:40', '2019-05-18 11:41:40'),
(9, '9', 2, 'Mei Yeni', 12345, 'Ds Gelam Gg. kelurahan RT 2 RW 1 \r\nCandi-Sidoarjo\r\n', 'Guru', '2019-05-18 13:28:39', '2019-05-18 13:28:39'),
(10, '10', 2, 'Devita Ayuningtyas ', 12345, 'Bogi RT 6 RW 2 Pademonegoro Sukodono Sidoarjo 	\r\n', 'Guru', '2019-05-18 13:28:39', '2019-05-18 13:28:39'),
(11, '11', 3, 'Rokhima fitri fujiati', 12345, 'Ds. Wonokasian RT 03 RW 01 Sidoarjo', 'Guru', '2019-05-18 13:28:39', '2019-05-18 13:28:39'),
(12, '12', 3, 'Ela Zakiyatun Naja', 12345, 'Jl. Industri Ds. Sidokepung Buduran-Sidoarjo\r\n', 'Guru', '2019-05-18 13:28:39', '2019-05-18 13:28:39'),
(13, '13', 4, 'Linda Listyowati', 12345, 'Ds. Gelang RT 06, RW 04 Tulangan, Sidoarjo\r\n', 'Guru', '2019-05-18 13:28:39', '2019-05-18 13:28:39'),
(14, '14', 4, 'Ainun Anugrah Aprillia', 12345, 'Jl. Raya Semambung RT 5 RW 1', 'Guru', '2019-05-18 13:28:39', '2019-05-18 13:28:39'),
(15, '15', 5, 'Siti amalus sholihah', 12345, 'Dsn.Pejagalan, Ds.Simo Angin-Angin RT12 RW 04, wonoayu\r\n', 'Guru', '2019-05-18 13:45:23', '2019-05-18 13:45:23'),
(16, '16', 5, 'Oktavia Santoso', 12345, 'Trosobo RT 5 RW 2 ', 'Guru', '2019-05-18 13:45:23', '2019-05-18 13:45:23'),
(17, '17', 6, 'Siti Nur Masrufah Dewi', 12345, 'Duran Karangpuri Rt 03  Rw 01', 'Guru', '2019-05-18 13:45:23', '2019-05-18 13:45:23'),
(18, '18', 6, 'Erliana Harurita Maryani', 12345, 'Desa plumbungan RT 11 RW 04', 'Guru', '2019-05-18 13:45:23', '2019-05-18 13:45:23'),
(19, '19', 7, 'Indah khurotul Ainia', 12345, 'Wringinanom, Gresik	', 'Guru', '2019-05-18 13:45:23', '2019-05-18 13:45:23'),
(20, '20', 7, 'Rosana Primantari Putri', 12345, 'Jl Ketegan Barat Gang 3 RT 05 RW 01, Sepanjang-Taman-Sidoarjo\r\n', 'Guru', '2019-05-18 13:45:23', '2019-05-18 13:45:23'),
(21, '21', 8, 'Dewi Izzah', 12345, 'Luwung RT 01 RW 03 Sidomojo Kec. Krian Kab.Sidoarjo\r\n', 'Guru', '2019-05-18 13:45:23', '2019-05-18 13:45:23'),
(22, '22', 8, 'Lia Dwi Purwanti', 12345, 'Jl. KI Hajar Dewantoro Rt.03/ Rw.01 Tulangan-Sidoarjo\r\n', 'Guru', '2019-05-18 13:45:23', '2019-05-18 13:45:23'),
(23, '23', 9, 'Juwita Fitri Lestari KS', 12345, 'Pekarungan Rt.13/04 Sukodono', 'Guru', '2019-05-18 13:45:23', '2019-05-18 13:45:23'),
(24, '24', 9, 'Afrilia Dwi Haryani', 12345, 'Ds. Pekarungan, RT 11 RW 04', 'Guru', '2019-05-18 13:45:23', '2019-05-18 13:45:23'),
(25, '25', 10, 'Niken Dewi Hestari', 12345, 'Ds Keboharan RT 02 RW 01 Krian-Sidoarjo\r\n', 'Guru', '2019-05-18 13:45:23', '2019-05-18 13:45:23'),
(26, '26', 10, 'Moch. Fauzan', 12345, 'Kepuhkiriman Dalam Masjid III/21 RT 04 RW 01 Waru Sidoarjo\r\n', 'Guru', '2019-05-18 13:45:23', '2019-05-18 13:45:23'),
(27, '27', 11, 'Susyani Halimatus Syadia', 12345, 'Ds.Sruni Jl.Jambu Rt.17 Rw.03 \r\nNo:465, Gedangan-Sidoarjo\r\n', 'Guru', '2019-05-18 13:45:23', '2019-05-18 13:45:23'),
(28, '28', 11, 'Ucik Lailatul Maghfiroh', 12345, 'Jl.Mangkurejo 2 Kwangsan \r\nSedati Sidoarjo\r\n', 'Guru', '2019-05-18 13:45:23', '2019-05-18 13:45:23'),
(29, '29', 12, 'Eka Putri Rakhmawati', 12345, 'Pademonegoro Rt 09 Rw 03 \r\nSukodono\r\n', 'Guru', '2019-05-18 14:05:51', '2019-05-18 14:05:51'),
(30, '30', 13, 'Adining Widya K.W.S', 12345, 'Ds. Plumbungan RT. 10 RW. 04 \r\nSukodono-Sidoarjo\r\n', 'Guru', '2019-05-18 14:05:51', '2019-05-18 14:05:51'),
(31, '31', 13, 'Arif Kurniawan', 12345, 'Dsn. Jedong RT 05/RW 02\r\nKec. Prambon, Kab. Sidoarjo\r\n', 'Guru', '2019-05-18 14:05:51', '2019-05-18 14:05:51'),
(32, '32', 14, 'Anis Setiyowati', 12345, 'Somban lor 1/111 Kebonsari \r\nCandi-Sidoarjo\r\n', 'Guru', '2019-05-18 14:05:51', '2019-05-18 14:05:51'),
(33, '33', 14, 'Tika Elok Octaviani', 12345, 'Medaeng Tengah RT 11 RW 05 \r\nKedungturi - Taman - Sidoarjo\r\n', 'Guru', '2019-05-18 14:05:51', '2019-05-18 14:05:51'),
(34, '34', 15, 'Jeny Andriyati', 12345, 'Ds. Semawut RT 11 RW04	', 'Guru', '2019-05-18 14:05:51', '2019-05-18 14:05:51'),
(35, '35', 16, 'Ali Akbar', 12345, 'Ds. Sidokepung RT 27 RW 07\r\nBuduran-Sidoarjo\r\n', 'Guru', '2019-05-18 14:05:51', '2019-05-18 14:05:51'),
(36, '36', 16, 'Miftakul Jannah', 12345, 'Ds. Lambangan 01/01 \r\nWonoayu-Sidoarjo\r\n', 'Guru', '2019-05-18 14:05:51', '2019-05-18 14:05:51'),
(37, '37', 17, 'Muhammad Sariyono', 12345, 'Plumbungan RT 11 RW 04 \r\nSukodono-Sidoarjo\r\n', 'Guru', '2019-05-18 14:05:51', '2019-05-18 14:05:51'),
(38, '38', 17, 'Ervina Dinul Khoyimah', 12345, 'Dusun Japanan RT 04 RW 01. \r\nDesa Popoh. Kecamatan Wonoayu\r\n', 'Guru', '2019-05-18 14:05:51', '2019-05-18 14:05:51');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_detail_presensi`
--
ALTER TABLE `tb_detail_presensi`
  ADD PRIMARY KEY (`ID_DETAIL_PRESENSI`),
  ADD KEY `ID_SISWA` (`ID_SISWA`),
  ADD KEY `ID_PRESENSI` (`ID_PRESENSI`);

--
-- Indexes for table `tb_gambar`
--
ALTER TABLE `tb_gambar`
  ADD PRIMARY KEY (`ID_GAMBAR`);

--
-- Indexes for table `tb_kelas`
--
ALTER TABLE `tb_kelas`
  ADD PRIMARY KEY (`ID_KELAS`);

--
-- Indexes for table `tb_m_indikator`
--
ALTER TABLE `tb_m_indikator`
  ADD PRIMARY KEY (`ID_INDIKATOR`),
  ADD KEY `ID_KARAKTER` (`ID_KARAKTER`);

--
-- Indexes for table `tb_m_karakter`
--
ALTER TABLE `tb_m_karakter`
  ADD PRIMARY KEY (`ID_KARAKTER`);

--
-- Indexes for table `tb_nilai_karakter`
--
ALTER TABLE `tb_nilai_karakter`
  ADD PRIMARY KEY (`ID_NILAI`),
  ADD KEY `ID_USER` (`ID_USER`),
  ADD KEY `ID_SISWA` (`ID_KELAS`),
  ADD KEY `ID_KARAKTER` (`ID_KARAKTER`);

--
-- Indexes for table `tb_nilai_point`
--
ALTER TABLE `tb_nilai_point`
  ADD PRIMARY KEY (`ID_POINT`),
  ADD KEY `ID_USER` (`ID_USER`),
  ADD KEY `ID_SISWA` (`ID_SISWA`),
  ADD KEY `ID_KELAS` (`ID_KELAS`);

--
-- Indexes for table `tb_presensi`
--
ALTER TABLE `tb_presensi`
  ADD PRIMARY KEY (`ID_PRESENSI`),
  ADD KEY `ID_USER` (`ID_USER`),
  ADD KEY `ID_KELAS` (`ID_KELAS`);

--
-- Indexes for table `tb_siswa`
--
ALTER TABLE `tb_siswa`
  ADD PRIMARY KEY (`ID_SISWA`),
  ADD KEY `ID_KELAS` (`ID_KELAS`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`ID_USER`),
  ADD UNIQUE KEY `NOMOR_INDUK` (`NOMOR_INDUK`),
  ADD KEY `KELAS` (`KELAS`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_detail_presensi`
--
ALTER TABLE `tb_detail_presensi`
  MODIFY `ID_DETAIL_PRESENSI` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_gambar`
--
ALTER TABLE `tb_gambar`
  MODIFY `ID_GAMBAR` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_kelas`
--
ALTER TABLE `tb_kelas`
  MODIFY `ID_KELAS` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `tb_m_indikator`
--
ALTER TABLE `tb_m_indikator`
  MODIFY `ID_INDIKATOR` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `tb_m_karakter`
--
ALTER TABLE `tb_m_karakter`
  MODIFY `ID_KARAKTER` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tb_nilai_karakter`
--
ALTER TABLE `tb_nilai_karakter`
  MODIFY `ID_NILAI` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=207;

--
-- AUTO_INCREMENT for table `tb_nilai_point`
--
ALTER TABLE `tb_nilai_point`
  MODIFY `ID_POINT` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_presensi`
--
ALTER TABLE `tb_presensi`
  MODIFY `ID_PRESENSI` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_siswa`
--
ALTER TABLE `tb_siswa`
  MODIFY `ID_SISWA` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `ID_USER` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tb_detail_presensi`
--
ALTER TABLE `tb_detail_presensi`
  ADD CONSTRAINT `tb_detail_presensi_ibfk_1` FOREIGN KEY (`ID_SISWA`) REFERENCES `tb_siswa` (`ID_SISWA`),
  ADD CONSTRAINT `tb_detail_presensi_ibfk_2` FOREIGN KEY (`ID_PRESENSI`) REFERENCES `tb_presensi` (`ID_PRESENSI`);

--
-- Constraints for table `tb_m_indikator`
--
ALTER TABLE `tb_m_indikator`
  ADD CONSTRAINT `tb_m_indikator_ibfk_1` FOREIGN KEY (`ID_KARAKTER`) REFERENCES `tb_m_karakter` (`ID_KARAKTER`);

--
-- Constraints for table `tb_nilai_karakter`
--
ALTER TABLE `tb_nilai_karakter`
  ADD CONSTRAINT `tb_nilai_karakter_ibfk_1` FOREIGN KEY (`ID_KELAS`) REFERENCES `tb_kelas` (`ID_KELAS`),
  ADD CONSTRAINT `tb_nilai_karakter_ibfk_2` FOREIGN KEY (`ID_USER`) REFERENCES `tb_user` (`ID_USER`),
  ADD CONSTRAINT `tb_nilai_karakter_ibfk_3` FOREIGN KEY (`ID_KARAKTER`) REFERENCES `tb_m_karakter` (`ID_KARAKTER`);

--
-- Constraints for table `tb_nilai_point`
--
ALTER TABLE `tb_nilai_point`
  ADD CONSTRAINT `tb_nilai_point_ibfk_1` FOREIGN KEY (`ID_SISWA`) REFERENCES `tb_siswa` (`ID_SISWA`),
  ADD CONSTRAINT `tb_nilai_point_ibfk_2` FOREIGN KEY (`ID_KELAS`) REFERENCES `tb_kelas` (`ID_KELAS`),
  ADD CONSTRAINT `tb_nilai_point_ibfk_3` FOREIGN KEY (`ID_USER`) REFERENCES `tb_user` (`ID_USER`);

--
-- Constraints for table `tb_presensi`
--
ALTER TABLE `tb_presensi`
  ADD CONSTRAINT `tb_presensi_ibfk_1` FOREIGN KEY (`ID_USER`) REFERENCES `tb_user` (`ID_USER`),
  ADD CONSTRAINT `tb_presensi_ibfk_2` FOREIGN KEY (`ID_KELAS`) REFERENCES `tb_kelas` (`ID_KELAS`);

--
-- Constraints for table `tb_siswa`
--
ALTER TABLE `tb_siswa`
  ADD CONSTRAINT `tb_siswa_ibfk_1` FOREIGN KEY (`ID_KELAS`) REFERENCES `tb_kelas` (`ID_KELAS`);

--
-- Constraints for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD CONSTRAINT `tb_user_ibfk_1` FOREIGN KEY (`KELAS`) REFERENCES `tb_kelas` (`ID_KELAS`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
