var nilaiKarakter = $.ajax({
  async: false,
  url: './config/commit/query.php?name=get-nilai-karakter',
  cache: false,
  success: function(msg) {
    return msg;
  }
});
var result = $('#first-card').css('height');
var res = parseFloat(result) - 55;
$('#LineChartNilaiKarakter').height(
  $('#LineChartNilaiKarakter').height() + res
);

var nilaiPoin = $.ajax({
  async: false,
  url: './config/commit/query.php?name=get-nilai-poin',
  cache: false,
  success: function(msg) {
    return msg;
  }
});
var KarakterDisiplin = nilaiKarakter.responseJSON.filter(e => e.karakter == 1);
var KarakterTanggungJawab = nilaiKarakter.responseJSON.filter(
  e => e.karakter == 2
);

var NilaiPoin1 = nilaiPoin.responseJSON
  .map(e => parseInt(e.nilai1, 10))
  .reduce((a, b) => a + b, 0);
var NilaiPoin2 = nilaiPoin.responseJSON
  .map(e => parseInt(e.nilai2, 10))
  .reduce((a, b) => a + b, 0);
var NilaiPoin3 = nilaiPoin.responseJSON
  .map(e => parseInt(e.nilai3, 10))
  .reduce((a, b) => a + b, 0);

const initialDate = KarakterDisiplin.date;
const arrayDate = KarakterDisiplin.map(e =>
  moment(e.date).format('DD-MM-YYYY')
);

const arrayNilaiDisiplin = KarakterDisiplin.map(e => {
  let score = 0;
  e.nilai.nilai.forEach(el => {
    el.nilai === 'Ya' ? (score += 1) : null;
  });
  return score;
});
const arrayNilaiTanggungJawab = KarakterTanggungJawab.map(e => {
  let score = 0;
  e.nilai.nilai.forEach(el => {
    el.nilai === 'Ya' ? (score += 1) : null;
  });
  return score;
});

const sumNilaiDisiplin = arrayNilaiDisiplin.reduce((a, b) => a + b, 0);
const sumNilaiTanggungJawab = arrayNilaiTanggungJawab.reduce(
  (a, b) => a + b,
  0
);

Highcharts.chart('LineChartNilaiKarakter', {
  title: {
    text: `Dashboard Nilai Karakter Siswa Bulan Ke ${moment().format('MM')}`
  },
  tooltip: {
    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
    pointFormat:
      '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
      '<td style="padding:0"><b>{point.y}</b></td></tr>',
    footerFormat: '</table>',
    shared: true,
    useHTML: true
  },
  xAxis: {
    categories: arrayDate
  },
  yAxis: {
    title: {
      text: 'Point'
    },
    allowDecimals: false
  },
  series: [
    {
      name: 'Karakter Disiplin',
      data: arrayNilaiDisiplin
    },
    {
      name: 'Karakter Tanggung Jawab',
      data: arrayNilaiTanggungJawab
    }
  ]

  /* responsive: {
    rules: [
      {
        condition: {
          maxWidth: 500
        },
        chartOptions: {
          legend: {
            layout: 'horizontal',
            align: 'center',
            verticalAlign: 'bottom'
          }
        }
      }
    ]
  } */
});

// Build the chart
Highcharts.chart('BarChartNilaiKarakter', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'Total Perolehan Nilai Karakter'
  },
  xAxis: {
    categories: ['Nilai Karakter'],
    crosshair: true
  },
  yAxis: {
    min: 0,
    title: {
      text: 'Total Poin'
    }
  },
  tooltip: {
    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
    pointFormat:
      '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
      '<td style="padding:0"><b>{point.y}</b></td></tr>',
    footerFormat: '</table>',
    shared: true,
    useHTML: true
  },
  plotOptions: {
    column: {
      pointPadding: 0.2,
      borderWidth: 0
    }
  },
  series: [
    {
      name: 'Karakter Disiplin',
      data: [sumNilaiDisiplin]
    },
    {
      name: 'Karakter Tanggung Jawab',
      data: [sumNilaiTanggungJawab]
    }
  ]
});

// Build the chart
Highcharts.chart('PieChartNilaiPoin', {
  chart: {
    plotBackgroundColor: null,
    plotBorderWidth: null,
    plotShadow: false,
    type: 'pie'
  },
  title: {
    text: 'Total Perolehan Nilai Poin'
  },
  tooltip: {
    pointFormat: '{series.name}: <b>{point.y}</b>'
  },
  plotOptions: {
    pie: {
      allowPointSelect: true,
      cursor: 'pointer',
      dataLabels: {
        enabled: false
      },
      showInLegend: true
    }
  },
  series: [
    {
      name: 'Nilai Poin',
      colorByPoint: true,
      data: [
        {
          name: 'Nilai 1',
          y: NilaiPoin1
        },
        {
          name: 'Nilai 2',
          y: NilaiPoin2
        },
        {
          name: 'Nilai 3',
          y: NilaiPoin3
        }
      ]
    }
  ]
});
