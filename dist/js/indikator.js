$(document).ready(function() {
  $('#validateIndikator').validate({
    rules: {
      namaKarakter: {
        required: true
      },
      namaIndikator: {
        required: true
      }
    },
    messages: {
      namaKarakter: {
        required: 'Pilih nama karakter'
      },
      namaIndikator: {
        required: 'Masukkan nama indikator'
      }
    },
    errorElement: 'div',
    errorPlacement: function(error, element) {
      var placement = $(element).data('error');
      if (placement) {
        $(placement).append(error);
      } else {
        error.insertAfter(element);
      }
    },
    submitHandler: function(form, e) {
      e.preventDefault();

      const idKarakter = $('#nama-karakter')
        .children('option:selected')
        .val();
      const namaIndikator = $('#nama-indikator').val();
      const body = {
        idKarakter,
        namaIndikator
      };

      $.ajax({
        url: './config/commit/commit.php?name=post-indikator',
        method: 'POST',
        data: body,
        success: data => {
          const decode = JSON.parse(data);
          if (decode.code === 'OK') {
            $('.btn-simpan').prop('disabled', true);
            M.toast({
              html: `Data indikator berhasil disimpan`,
              completeCallback: () => {
                $('#validateIndikator').trigger('reset');
                setTimeout(function() {
                  $('#table-get-indikator')
                    .DataTable()
                    .ajax.reload(null, false);
                }, 1000);
                $('.btn-simpan').prop('disabled', false);
              },
              classes: 'rounded'
            });
          } else {
            M.toast({
              html: `Terjadi kesalahan saat menyimpan`,
              classes: 'rounded'
            });
            $('.btn-simpan').prop('disabled', false);
            console.log('-->', decode.msg);
          }
        },
        error: error => {
          console.log('Error', error);
          $('.btn-simpan').prop('disabled', false);
        }
      });
    }
  });
});

$('#table-get-indikator').dataTable({
  processing: true,
  ajax: {
    url: './config/commit/query.php?name=get-indikator',
    type: 'GET',
    dataSrc: ''
  },
  columns: [
    { data: 'no' },
    { data: 'nama_karakter' },
    { data: 'nama_indikator' }
  ]
});
