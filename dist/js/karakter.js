$(document).ready(function() {
  $('#validateKarakter').validate({
    rules: {
      namaKarakter: {
        required: true
      }
    },
    messages: {
      namaKarakter: {
        required: 'Masukkan nama karakter'
      }
    },
    errorElement: 'div',
    errorPlacement: function(error, element) {
      var placement = $(element).data('error');
      if (placement) {
        $(placement).append(error);
      } else {
        error.insertAfter(element);
      }
    },
    submitHandler: function(form, e) {
      e.preventDefault();

      const namaKarakter = $('#nama-karakter').val();
      const body = {
        namaKarakter
      };
      console.log('nama karakter', body);

      $.ajax({
        url: './config/commit/commit.php?name=post-karakter',
        method: 'POST',
        data: body,
        success: data => {
          const decode = JSON.parse(data);
          if (decode.code === 'OK') {
            $('.btn-simpan').prop('disabled', true);
            M.toast({
              html: `Data Kelas berhasil disimpan`,
              completeCallback: () => {
                $('#validateKarakter').trigger('reset');
                setTimeout(function() {
                  $('#nama-karakter').val('');
                  $('#table-get-karakter')
                    .DataTable()
                    .ajax.reload(null, false);
                }, 1000);
                $('.btn-simpan').prop('disabled', false);
              },
              classes: 'rounded'
            });
          } else {
            M.toast({
              html: `Terjadi kesalahan saat menyimpan`,
              classes: 'rounded'
            });
            $('.btn-simpan').prop('disabled', false);
            console.log('-->', decode.msg);
          }
        },
        error: error => {
          console.log('Error', error);
          $('.btn-simpan').prop('disabled', false);
        }
      });
    }
  });

  $('#filterNilaiDisiplinGuru').change(() => {
    $('#table-master-nilai-displin')
      .dataTable()
      .fnDestroy();

    const month = $('#filterNilaiDisiplinGuru').val();

    $('#table-master-nilai-displin').dataTable({
      bLengthChange: false,
      processing: true,
      ajax: {
        url: `./config/commit/query.php?name=nilai-disiplin-by-kelas&month=${month}`,
        type: 'GET',
        dataSrc: ''
      },
      columns: [
        { data: 'no' },
        { data: 'tanggal' },
        { data: 'nama_kelas' },
        // { data: 'aksi' }
        {
          mData: 'aksi',
          mRender: function(data, type, row) {
            return `
                <button  onclick="location.href='main?module=master-penilaian-disiplin&k=${
                  data[0]
                }&d=${data[1]}&n=${
              data[2]
            }'" class="btn blue waves-effect waves-light left" type="button">Detail</button>
                `;
          }
        }
      ]
    });

    // const month = $('#filterNilaiTanggungJawab').val();
    // const format = moment(month, 'MM').format('MMMM');
    // $('.bulan').text(format);
    // window.location.replace(
    //   `main?module=master-penilaian-tanggung-jawab&month=${month}`
    // );
  });

  $('#filterNilaiTanggungJawabGuru').change(() => {
    debugger;
    $('#table-master-nilai-tanggung-jawab')
      .dataTable()
      .fnDestroy();

    const month = $('#filterNilaiTanggungJawabGuru').val();

    $('#table-master-nilai-tanggung-jawab').dataTable({
      bLengthChange: false,
      processing: true,
      ajax: {
        url: `./config/commit/query.php?name=nilai-tanggung-jawab-by-kelas&month=${month}`,
        type: 'GET',
        dataSrc: ''
      },
      columns: [
        { data: 'no' },
        { data: 'tanggal' },
        { data: 'nama_kelas' },
        // { data: 'aksi' }
        {
          mData: 'aksi',
          mRender: function(data, type, row) {
            return `
                <button  onclick="location.href='main?module=master-penilaian-tanggung-jawab&k=${
                  data[0]
                }&d=${data[1]}&n=${
              data[2]
            }'" class="btn blue waves-effect waves-light left" type="button">Detail</button>
                `;
          }
        }
      ]
    });
  });
});

$('#table-get-karakter').dataTable({
  processing: true,
  ajax: {
    url: './config/commit/query.php?name=get-karakter',
    type: 'GET',
    dataSrc: ''
  },
  columns: [{ data: 'no' }, { data: 'id' }, { data: 'nama' }]
});

$('#table-master-nilai-displin').dataTable({
  bLengthChange: false,
  processing: true,
  ajax: {
    url: './config/commit/query.php?name=nilai-disiplin-by-kelas',
    type: 'GET',
    dataSrc: ''
  },
  columns: [
    { data: 'no' },
    { data: 'tanggal' },
    { data: 'nama_kelas' },
    // { data: 'aksi' }
    {
      mData: 'aksi',
      mRender: function(data, type, row) {
        return `
          <button  onclick="location.href='main?module=master-penilaian-disiplin&k=${
            data[0]
          }&d=${data[1]}&n=${
          data[2]
        }'" class="btn blue waves-effect waves-light left" type="button">Detail</button>
          `;
      }
    }
  ]
});

$('#table-master-detail-nilai-displin').dataTable({
  processing: true,
  ajax: {
    data: {
      k: new URL(window.location.href).searchParams.get('k'),
      d: new URL(window.location.href).searchParams.get('d'),
      n: new URL(window.location.href).searchParams.get('n')
    },
    url: './config/commit/query.php?name=nilai-disiplin-by-date',
    type: 'GET',
    dataSrc: ''
  },
  columns: [{ data: 'no' }, { data: 'id' }]
});

$('#table-nilai-disiplin-by-siswa').dataTable({
  processing: true,
  ajax: {
    url: './config/commit/query.php?name=nilai-disiplin-by-siswa',
    type: 'GET',
    dataSrc: ''
  },
  columns: [
    { data: 'no' },
    { data: 'nis' },
    { data: 'nama' },
    { data: 'total' },
    {
      mData: 'aksi',
      mRender: (data, type, row) => {
        // return `<button  onclick="location.href='main?module=master-penilaian-tanggung-jawab&n" class="btn blue waves-effect waves-light left" type="button">Detail</button>`;
        return `
          <a class="waves-effect waves-light btn modal-trigger" onClick="setModal('${
            row.nama
          }', 1)" href="#myModal" data-num="${row.no}">Detail</a>
        `;
      }
    }
  ]
});

$('#table-master-nilai-tanggung-jawab').dataTable({
  bLengthChange: false,
  processing: true,
  ajax: {
    url: './config/commit/query.php?name=nilai-tanggung-jawab-by-kelas',
    type: 'GET',
    dataSrc: ''
  },
  columns: [
    { data: 'no' },
    { data: 'tanggal' },
    { data: 'nama_kelas' },
    // { data: 'aksi' }
    {
      mData: 'aksi',
      mRender: function(data, type, row) {
        return `
          <button  onclick="location.href='main?module=master-penilaian-tanggung-jawab&k=${
            data[0]
          }&d=${data[1]}&n=${
          data[2]
        }'" class="btn blue waves-effect waves-light left" type="button">Detail</button>
          `;
      }
    }
  ]
});

$('#table-nilai-tanggung-jawab-by-siswa').dataTable({
  processing: true,
  ajax: {
    url: './config/commit/query.php?name=nilai-tanggung-jawab-by-siswa',
    type: 'GET',
    dataSrc: ''
  },
  columns: [
    { data: 'no' },
    { data: 'nis' },
    { data: 'nama' },
    { data: 'total' },
    {
      mData: 'aksi',
      mRender: (data, type, row) => {
        debugger;
        return `
        <a class="waves-effect waves-light btn modal-trigger" onClick="setModal('${
          row.nama
        }', 2)" href="#myModal" data-num="${row.no}">Detail</a>
      `;
      }
    }
  ]
});

setModal = (nama, karakter) => {
  var nilaiKarakter = $.ajax({
    async: false,
    method: 'POST',
    data: { nama: nama, karakter: karakter },
    url: './config/commit/query.php?name=detail-nilai-karakter',
    cache: false,
    success: function(msg) {
      return msg;
    }
  });
  let header = '';
  if (karakter === 1) {
    header = `<div class="modal-content">
    <p>Nama Siswa: <b>${nama}</b></p>
    <table>
    <tr>
      <td>No</td><td>Tanggal</td>
      <td width="90px">
        <a class="tooltipped" data-position="top" data-delay="50" data-tooltip="Datang ke sekolah tepat dan masuk kelas pada waktunya.">A</a>
      </td>
      <td width="90px">
        <a class="tooltipped" data-position="top" data-delay="50" data-tooltip="Melaksanakan tugas-tugas kelas yang menjadi tanggung jawabnya.">B</a>
      </td>
      <td width="90px">
        <a class="tooltipped" data-position="top" data-delay="50" data-tooltip="Duduk pada tempat yang telah ditetapkan.">C</a>
      </td>
      <td width="90px">
        <a class="tooltipped" data-position="top" data-delay="50" data-tooltip="Menaati peraturan sekolah dan kelas.">D</a>
      </td>
      <td width="90px">
        <a class="tooltipped" data-position="top" data-delay="50" data-tooltip="Berpakaian sopan dan rapi.">E</a>
      </td>
      <td width="90px">
        <a class="tooltipped" data-position="top" data-delay="50" data-tooltip="Mematuhi aturan permainan.">F</a>
      </td>
      <td width="90px">
        <a class="tooltipped" data-position="top" data-delay="50" data-tooltip="Menyelesaikan tugas pada waktunya.">G</a>
      </td>
      <td width="90px">
        <a class="tooltipped" data-position="top" data-delay="50" data-tooltip="Saling menjaga dengan teman agar semua tugas-tugas kelas terlaksana dengan baik.">H</a>
      </td>
      <td width="90px">
        <a class="tooltipped" data-position="top" data-delay="50" data-tooltip="Selalu mengajak teman menjaga ketertiban kelas.">I</a>
      </td>
      <td width="90px">
        <a class="tooltipped" data-position="top" data-delay="50" data-tooltip="Mengingatkan teman yang melanggar peraturan dengan kata-kata sopan dan tidak menyinggung.">J</a>
      </td>
    </tr>`;
  } else {
    header = `<div class="modal-content">
    <p>Nama Siswa: <b>${nama}</b></p>
    <table>
    <tr>
      <td>No</td><td>Tanggal</td>
      <td width="90px">
        <a class="tooltipped" data-position="top" data-delay="50" data-tooltip="Datang ke sekolah tepat dan masuk kelas pada waktunya.">A</a>
      </td>
      <td width="90px">
        <a class="tooltipped" data-position="top" data-delay="50" data-tooltip="Melaksanakan tugas-tugas kelas yang menjadi tanggung jawabnya.">B</a>
      </td>
      <td width="90px">
        <a class="tooltipped" data-position="top" data-delay="50" data-tooltip="Duduk pada tempat yang telah ditetapkan.">C</a>
      </td>
      <td width="90px">
        <a class="tooltipped" data-position="top" data-delay="50" data-tooltip="Menaati peraturan sekolah dan kelas.">D</a>
      </td>
      <td width="90px">
        <a class="tooltipped" data-position="top" data-delay="50" data-tooltip="Berpakaian sopan dan rapi.">E</a>
      </td>
      <td width="90px">
        <a class="tooltipped" data-position="top" data-delay="50" data-tooltip="Mematuhi aturan permainan.">F</a>
      </td>
      <td width="90px">
        <a class="tooltipped" data-position="top" data-delay="50" data-tooltip="Menyelesaikan tugas pada waktunya.">G</a>
      </td>
      <td width="90px">
        <a class="tooltipped" data-position="top" data-delay="50" data-tooltip="Saling menjaga dengan teman agar semua tugas-tugas kelas terlaksana dengan baik.">H</a>
      </td>
      <td width="90px">
        <a class="tooltipped" data-position="top" data-delay="50" data-tooltip="Selalu mengajak teman menjaga ketertiban kelas.">I</a>
      </td>
      <td width="90px">
        <a class="tooltipped" data-position="top" data-delay="50" data-tooltip="Mengingatkan teman yang melanggar peraturan dengan kata-kata sopan dan tidak menyinggung.">J</a>
      </td>
      <td width="90px">
        <a class="tooltipped" data-position="top" data-delay="50" data-tooltip="Mengingatkan teman yang melanggar peraturan dengan kata-kata sopan dan tidak menyinggung.">K</a>
      </td>
    </tr>`;
  }

  let content = '';
  let nilai = '';
  nilaiKarakter.responseJSON.forEach((e, index) => {
    nilai = '';
    e.nilai.nilai.forEach(x => {
      console.log('x', x);
      return (nilai += `<td>${x.nilai}</td>`);
    });
    debugger;
    return (content += `
      <tr>
        <td>${index + 1}.</td>
        <td>${e.date}</td>
        ${nilai}
      </tr>
      `);
  });
  let footer = '</table></div>';

  let formattedModal = header + content + footer;
  $('.modal-content').remove();
  $('.modal').append(formattedModal);
  $('.modal').modal();
};
