$(document).ready(function() {
  $('#validateTahunAjaran').validate({
    rules: {
      namaTahunAjaran: {
        required: true
      },
      awalTahunAjaran: {
        required: true
      },
      akhirTahunAjaran: {
        required: true
      }
    },
    messages: {
      namaTahunAjaran: {
        required: 'Masukkan nama tahun ajaran'
      },
      awalTahunAjaran: {
        required: 'Masukkan periode awal tahun ajaran'
      },
      akhirTahunAjaran: {
        required: 'Masukkan periode akhir tahun ajaran'
      }
    },
    errorElement: 'div',
    errorPlacement: function(error, element) {
      var placement = $(element).data('error');
      if (placement) {
        $(placement).append(error);
      } else {
        error.insertAfter(element);
      }
    },
    submitHandler: function(form, e) {
      e.preventDefault();

      const nama = $('#namaTahunAjaran').val();
      const awal = $('#awalTahunAjaran').val();
      const akhir = $('#akhirTahunAjaran').val();
      const checkDate = moment(akhir).isAfter(awal);

      if (!checkDate) {
        return M.toast({
          html: 'Periode tahun ajaran tidak valid',
          classes: 'rounded'
        });
      }

      const body = {
        nama,
        awal,
        akhir
      };

      $.ajax({
        url: './config/commit/commit.php?name=post-periode',
        method: 'POST',
        data: body,
        success: data => {
          const decode = JSON.parse(data);
          if (decode.code === 'OK') {
            $('.btn-simpan').prop('disabled', true);
            M.toast({
              html: `Data periode berhasil disimpan`,
              completeCallback: () => {
                $('#validateTahunAjaran').trigger('reset');
                setTimeout(function() {
                  $('#namaTahunAjaran').val('');
                  $('#awalTahunAjaran').val('');
                  $('#akhirTahunAjaran').val('');
                  $('#table-get-tahun-ajaran')
                    .DataTable()
                    .ajax.reload(null, false);
                }, 1000);
                $('.btn-simpan').prop('disabled', false);
              },
              classes: 'rounded'
            });
          } else {
            M.toast({
              html: `Terjadi kesalahan saat menyimpan`,
              classes: 'rounded'
            });
            $('.btn-simpan').prop('disabled', false);
            console.log('-->', decode.msg);
          }
        },
        error: error => {
          console.log('Error', error);
          $('.btn-simpan').prop('disabled', false);
        }
      });
    }
  });
});

$('#table-get-tahun-ajaran').dataTable({
  processing: true,
  ajax: {
    url: './config/commit/query.php?name=get-tahun-ajaran',
    type: 'GET',
    dataSrc: ''
  },
  columns: [
    { data: 'no' },
    { data: 'nama' },
    { data: 'periode_awal' },
    { data: 'periode_akhir' },
    { data: 'status' },
    {
      mData: 'aksi',
      mRender: (data, type, row) => {
        if (row.status == 'Aktif') {
          return `<button class="btn blue waves-effect waves-light left" type="button" disabled>Set Aktif</button>`;
        } else {
          return `
          <button class="btn green waves-effect waves-light left btn-tahun-ajaran" type="button" onclick="setTahunAktif(${data})">Set Aktif</button>
          `;
        }
      }
    }
  ]
});

setTahunAktif = id => {
  $('.btn-tahun-ajaran').prop('disabled', true);
  $.ajax({
    url: './config/commit/commit.php?name=set-aktif-periode',
    method: 'POST',
    data: { id: id },
    success: function(data) {
      const decode = JSON.parse(data);
      if (decode.code === 'OK') {
        M.toast({
          html: 'Status berhasil diperbarui',
          completeCallback: () => {
            setTimeout(function() {
              $('#table-get-tahun-ajaran')
                .DataTable()
                .ajax.reload(null, false);
            }, 1000);
          },
          classes: 'rounded'
        });
      } else {
        M.toast({
          html: `Terjadi kesalahan saat memperbarui status`,
          classes: 'rounded'
        });
        console.log('-->', decode.msg);
      }
    }
  });
};
