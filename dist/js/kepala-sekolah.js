$(document).ready(function() {
  $('#table-master-nilai-displin-kepsek').dataTable({
    bLengthChange: false
  });

  $('#getNilaiDisiplinKepsek').change(() => {
    debugger;
    $('#table-master-nilai-displin-kepsek')
      .dataTable()
      .fnDestroy();

    const kelas = $('#getNilaiDisiplinKepsek').val();

    $('#table-master-nilai-displin-kepsek').dataTable({
      bLengthChange: false,
      processing: true,
      ajax: {
        data: { id_kelas: kelas },
        url: './config/commit/query.php?name=nilai-disiplin-kepsek',
        type: 'POST',
        dataSrc: ''
      },
      columns: [
        { data: 'no' },
        { data: 'nis' },
        { data: 'nama' },
        { data: 'total' },
        {
          mData: 'aksi',
          mRender: (data, type, row) => {
            return `
          <a class="waves-effect waves-light btn modal-trigger" onClick="setModal('${
            row.nama
          }', 1)" href="#myModal" data-num="${row.no}">Detail</a>
        `;
          }
        }
      ]
    });

    // const month = $('#filterNilaiTanggungJawab').val();
    // const format = moment(month, 'MM').format('MMMM');
    // $('.bulan').text(format);
    // window.location.replace(
    //   `main?module=master-penilaian-tanggung-jawab&month=${month}`
    // );
  });
});

var nilaiKarakter = $.ajax({
  async: false,
  url: './config/commit/query.php?name=get-nilai-karakter-kepala-sekolah',
  cache: false,
  success: function(msg) {
    return msg;
  }
});

const nilai = nilaiKarakter.responseJSON[0];
const kelas = nilaiKarakter.responseJSON[1];
const value = kelas.map(e => {
  let obj = {};
  let disiplin = 0;
  let tanggungJawab = 0;
  obj.name = `Kelas ${e}`;
  obj.disiplin = nilai
    .map(x => {
      return e === x.kelas ? (disiplin += x.total_disiplin) : (disiplin += 0);
    })
    .pop();
  obj.tanggungJawab = nilai
    .map(x => {
      return e === x.kelas
        ? (tanggungJawab += x.total_tanggung_jawab)
        : (tanggungJawab += 0);
    })
    .pop();
  obj.totalSiswa = nilai.filter(x => x.kelas === e).length;

  return obj;
});
const formattedValue = value.map(e => ({
  name:
    e.disiplin > e.tanggungJawab
      ? `Nilai Karakter Disiplin Kelas ${e.name}`
      : `Nilai Karakter Tanggung Jawab Kelas ${e.name}`,
  y:
    e.disiplin > e.tanggungJawab
      ? e.disiplin / e.totalSiswa
      : e.tanggungJawab / e.totalSiswa,
  drilldown: e.name,
  win: e.disiplin > e.tanggungJawab ? 'total_disiplin' : 'total_tanggung_jawab'
}));

const subValue = kelas.map(e => {
  let obj = {};
  obj.name = `Kelas ${e}`;
  obj.id = `Kelas ${e}`;
  let { win } = formattedValue.find(e => e.drilldown === obj.id);
  let namaSiswa = nilai.filter(x => x.kelas === e).map(x => x.nama);
  let nilaiSiswa = nilai.filter(x => x.kelas === e).map(x => x[win]);
  obj.data = namaSiswa.map((y, index) => {
    return [y, nilaiSiswa[index]];
  });

  return obj;
});
debugger;

/*
series: [
  {
    name: 'Kelas 6As',
    id: 'Kelas 6A',
    data: [
      ['v65.0', 0.1],
      ['v65.0', 0.1],
      ['v65.0', 0.1],
      ['v65.0', 0.1],
  }
]
*/

debugger;
/*
{
  name: 'Chrome',
  y: 62.74,
  drilldown: 'Chrome'
}
*/

Highcharts.chart('dashboard-kepala-sekolah', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'Rata-rata Nilai Karakter Tiap Kelas'
  },
  subtitle: {
    text: ''
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Total Nilai Karakter Per Kelas'
    }
  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      dataLabels: {
        enabled: true,
        format: '{point.y} Poin'
      }
    }
  },

  tooltip: {
    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
    pointFormat:
      '<span style="color:{point.color}">Kelas {point.name}</span>: <b>{point.y} Poin<br/>'
  },

  series: [
    {
      name: 'Nilai Karakter',
      colorByPoint: true,
      data: formattedValue
      // data: [
      //   {
      //     name: 'Chrome',
      //     y: 62.74,
      //     drilldown: 'Chrome'
      //   }
      // ]
    }
  ],
  drilldown: {
    series: subValue
    /* series: [
      {
        name: 'Kelas 6As',
        id: 'Kelas 6A',
        data: [
          ['v65.0', 0.1],
          ['v64.0', 1.3],
          ['v63.0', 53.02],
          ['v62.0', 1.4],
          ['v61.0', 0.88],
          ['v60.0', 0.56],
          ['v59.0', 0.45],
          ['v58.0', 0.49],
          ['v57.0', 0.32],
          ['v56.0', 0.29],
          ['v55.0', 0.79],
          ['v54.0', 0.18],
          ['v51.0', 0.13],
          ['v49.0', 2.16],
          ['v48.0', 0.13],
          ['v47.0', 0.11],
          ['v43.0', 0.17],
          ['v29.0', 0.26]
        ]
      },
      {
        name: 'Firefox',
        id: 'Firefox',
        data: [
          ['v58.0', 1.02],
          ['v57.0', 7.36],
          ['v56.0', 0.35],
          ['v55.0', 0.11],
          ['v54.0', 0.1],
          ['v52.0', 0.95],
          ['v51.0', 0.15],
          ['v50.0', 0.1],
          ['v48.0', 0.31],
          ['v47.0', 0.12]
        ]
      },
      {
        name: 'Internet Explorer',
        id: 'Internet Explorer',
        data: [['v11.0', 6.2], ['v10.0', 0.29], ['v9.0', 0.27], ['v8.0', 0.47]]
      },
      {
        name: 'Safari',
        id: 'Safari',
        data: [
          ['v11.0', 3.39],
          ['v10.1', 0.96],
          ['v10.0', 0.36],
          ['v9.1', 0.54],
          ['v9.0', 0.13],
          ['v5.1', 0.2]
        ]
      },
      {
        name: 'Edge',
        id: 'Edge',
        data: [['v16', 2.6], ['v15', 0.92], ['v14', 0.4], ['v13', 0.1]]
      },
      {
        name: 'Opera',
        id: 'Opera',
        data: [['v50.0', 0.96], ['v49.0', 0.82], ['v12.1', 0.14]]
      }
    ] */
  }
});
