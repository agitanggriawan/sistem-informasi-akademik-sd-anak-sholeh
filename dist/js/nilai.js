$('#table-nilai-poin-by-siswa').dataTable({
  processing: true,
  ajax: {
    url: './config/commit/query.php?name=nilai-poin-by-siswa',
    type: 'GET',
    dataSrc: ''
  },
  columns: [
    { data: 'no' },
    { data: 'tgl' },
    { data: 'nama' },
    { data: 'nilai1' },
    { data: 'nilai2' },
    { data: 'nilai3' }
  ]
});

var nilaiPoin = $.ajax({
  async: false,
  url: './config/commit/query.php?name=nilai-poin-by-guru',
  cache: true,
  success: function(msg) {
    return msg;
  }
});

// const IDs = Array.from(new Set(nilaiPoin.responseJSON.map(a => a.id))).map(
//   id => {
//     return nilaiPoin.responseJSON.find(a => a.id === id);
//   }
// );

const dataNilaiPoin = nilaiPoin.responseJSON.map((e, i) => ({
  No: `${i + 1}.`,
  ['Nomor Induk Siswa']: e.nis,
  Nama: e.nama,
  ['Nilai 1']: e.nilai1,
  ['Nilai 2']: e.nilai2,
  ['Nilai 3']: e.nilai3
}));

var nilaiPoint = [];

$.each(dataNilaiPoin[0], (key, value) => {
  var item = {};
  item.data = key;
  item.title = key;
  nilaiPoint.push(item);
});

$('#table-nilai-poin-by-guru').dataTable({
  data: dataNilaiPoin,
  columns: nilaiPoint
});
