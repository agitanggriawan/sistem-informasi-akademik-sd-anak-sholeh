$(document).ready(function() {
  moment.locale('id');
  moment.updateLocale('id', {
    weekdays: ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'],
    months: [
      'Januari',
      'Februari',
      'Maret',
      'April',
      'Mei',
      'Juni',
      'Juli',
      'Agustus',
      'September',
      'Oktober',
      'November',
      'Desember'
    ]
  });

  $('.modal').modal();
  $('select').formSelect();
  $('.datepicker').datepicker({
    format: 'yyyy-mm-dd',
    i18n: {
      months: [
        'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'
      ],
      weekdaysShort: ['Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab', 'Min'],
      cancel: 'Batal'
    }
  });

  $('select[required]').css({
    display: 'inline',
    position: 'absolute',
    float: 'left',
    padding: 0,
    margin: 0,
    border: '1px solid rgba(255,255,255,0)',
    height: 0,
    width: 0,
    top: '2em',
    left: '3em',
    opacity: 0,
    pointerEvents: 'none'
  });
});
$('#input-nilai-point').dataTable({
  processing: true,
  ajax: {
    url: './config/commit/query.php?name=nilai-point',
    type: 'GET',
    dataSrc: ''
  },
  columns: [
    { data: 'no' },
    { data: 'nis' },
    { data: 'nama' },
    { data: 'kelas' },
    { data: 'nilai1' },
    { data: 'nilai2' },
    { data: 'nilai3' },
    {
      mData: 'aksi',
      mRender: function(data, type, row) {
        debugger;
        return `
          <button class="btn blue waves-effect waves-light left" id="button_${data}" type="button" onclick="simpanNilaiPoint('${
          row['id']
        }', ${data}, ${row.kelas})">Simpan</button>
          `;
      }
    }
  ]
});

saveNilaiKarakter = () => {
  const totalSiswa = $('#totalSiswa')[0].textContent;
  const idGuru = $('#idGuru')[0].textContent;
  const idKelas = $('#idKelas')[0].textContent;
  const idKarakter = $('#idKarakter')[0].textContent;
  const idTahunAjaran = $('#idTahunAjaran')[0].textContent;
  let textKarakter;

  switch (parseInt(idKarakter, 10)) {
    case 1:
      textKarakter = 'Disiplin';
      break;
    case 2:
      textKarakter = 'Tanggung Jawab';
      break;
    case 3:
      textKarakter = 'Jujur';
      break;
    case 4:
      textKarakter = 'Toleransi';
      break;
    case 5:
      textKarakter = 'Religius';
      break;
    default:
      textKarakter = null;
  }
  let namaSiswa = [];
  for (let i = 1; i <= totalSiswa; i++) {
    let siswa = $(`#siswa${i}`).find(`#namaSiswa${i}`);
    namaSiswa.push(siswa[0].textContent);
  }
  // console.log('Nama Siswa', namaSiswa);

  let nilaiKarakter = [];
  let arrayNilai = [];
  for (let i = 1; i <= totalSiswa; i++) {
    let nilai = $(`#siswa${i}`).find('.nilaiKarakter');
    for (let j = 0; j < nilai.length; j++) {
      arrayNilai.push({
        id_indikator: j + 1,
        nilai: $(nilai[j]).val() === null ? 'Tidak' : $(nilai[j]).val()
      });
      // console.log('==>', $(nilai[j]).val());
    }
    nilaiKarakter.push({
      id_siswa: namaSiswa[i - 1],
      id_karakter: 1,
      nilai: arrayNilai
    });
    arrayNilai = [];
  }

  const body = {
    nilaiKarakter: nilaiKarakter,
    idGuru: idGuru,
    idKelas: idKelas,
    idKarakter: idKarakter,
    idTahunAjaran: idTahunAjaran
  };
  let sentData = new FormData();
  sentData.append('body', JSON.stringify(body));

  try {
    $.ajax({
      type: 'POST',
      url: './config/commit/commitNilaiKarakter.php',
      data: sentData,
      contentType: false,
      processData: false,
      cache: false,
      async: false,
      success: data => {
        $('button').prop('disabled', true);
        const decode = JSON.parse(data);

        if (decode.code === 'OK') {
          M.toast({
            html: `Nilai ${textKarakter} berhasil disimpan`,
            completeCallback: () => {
              window.location = 'main?module=dashboard';
            },
            classes: 'rounded'
          });
        } else {
          M.toast({
            html: `Terjadi kesalahan saat menyimpan nilai`,
            classes: 'rounded'
          });
          console.log('-->', decode.msg);
        }
      },
      error: error => {
        console.log('Error', error);
      }
    });
  } catch (error) {
    console.log('Error =>', error);
  }
};

simpanNilaiPoint = (a, b, c) => {
  const checkboxes = $(`.nilai_${b}`);
  let nilaiPoint = [];
  // let idGuru = parseInt($('#idGuru')[0].textContent, 10);
  let idGuru = parseInt($('#idGuru').val());
  let idTahunAjaran = parseInt($('#idTahunAjaran').val());
  for (let index = 0; index < checkboxes.length; index++) {
    nilaiPoint.push({
      nilai: checkboxes[index].checked
    });
  }

  const body = {
    idSiswa: parseInt(a, 10),
    idGuru: parseInt(idGuru, 10),
    idTahunAjaran: parseInt(idTahunAjaran, 10),
    idKelas: parseInt(c, 10),
    nilai1: nilaiPoint[0].nilai,
    nilai2: nilaiPoint[1].nilai,
    nilai3: nilaiPoint[2].nilai
  };
  let sentData = new FormData();
  sentData.append('body', JSON.stringify(body));

  try {
    $.ajax({
      type: 'POST',
      url: './config/commit/commit.php?name=nilai-poin',
      data: sentData,
      contentType: false,
      processData: false,
      cache: false,
      async: false,
      success: data => {
        $(`.nilai_${b}`).prop('disabled', true);
        $(`#button_${b}`).prop('disabled', true);
        const decode = JSON.parse(data);
        if (decode.code === 'OK') {
          M.toast({
            html: `Nilai Poin berhasil disimpan`,
            // completeCallback: () => {
            //   window.location = 'main?module=dashboard';
            // },
            classes: 'rounded'
          });
        } else {
          console.log('Error', decode);
        }
      },
      error: error => {
        console.log('Error', error);
      }
    });
  } catch (error) {
    console.log('Error =>', error);
  }
};
