$(document).ready(function() {});

$('#table-presensi').dataTable({
  processing: true,
  searching: false,
  paging: false,
  info: false,
  ordering: false,
  ajax: {
    url: './config/commit/query.php?name=get-siswa',
    type: 'GET',
    dataSrc: ''
  },
  columns: [
    { data: 'no' },
    { data: 'no_induk' },
    { data: 'nama' },
    {
      mData: 'aksi',
      mRender: (data, type, row, index) => {
        return `
          <p>
            <label>
              <input class="with-gap cb-presensi" id='["Hadir", "${
                row.no_induk
              }"]' name="cb-presensi-${index.row}" type="radio"  />
              <span>Hadir</span>
            </label>
            <label>
              <input class="with-gap cb-presensi" id='["Sakit", "${
                row.no_induk
              }"]' name="cb-presensi-${index.row}" type="radio"  />
              <span>Sakit</span>
            </label>
            <label>
              <input class="with-gap cb-presensi" id='["Izin", "${
                row.no_induk
              }"]' name="cb-presensi-${index.row}" type="radio"  />
              <span>Izin</span>
            </label>
            <label>
              <input class="with-gap cb-presensi" id='["Alpha", "${
                row.no_induk
              }"]' name="cb-presensi-${index.row}" type="radio"  />
              <span>Alpha</span>
            </label>
          </p>
        `;
      }
    }
  ]
});

$('#table-current-presensi').dataTable({
  processing: true,
  ajax: {
    url: './config/commit/query.php?name=get-current-presence',
    type: 'GET',
    dataSrc: ''
  },
  columns: [
    { data: 'no' },
    { data: 'nis' },
    { data: 'nama' },
    {
      mData: 'status',
      mRender: (data, type, row, index) => {
        const value = JSON.parse(row.status);
        return value ? value.status : 'Belum presensi hari ini';
      }
    }
  ]
});

$('#table-current-presensi-detail').dataTable({
  processing: true,
  ajax: {
    url: './config/commit/query.php?name=get-current-presence-detail',
    type: 'GET',
    dataSrc: ''
  },
  columns: [
    { data: 'no' },
    { data: 'nis' },
    { data: 'nama' },
    {
      mData: 'status',
      mRender: (data, type, row, index) => {
        const value = JSON.parse(row.status);
        return value.status;
      }
    }
  ]
});

$('#table-date-presensi').dataTable({
  processing: true,
  ajax: {
    url: './config/commit/query.php?name=get-date-presence',
    type: 'GET',
    dataSrc: ''
  },
  columns: [
    { data: 'no' },
    { data: 'tanggal' },
    { data: 'nama_kelas' },
    {
      mData: 'aksi',
      mRender: function(data, type, row) {
        return `
          <button  onclick="location.href='main?module=master-presensi&k=${
            data[0]
          }&d=${
          data[1]
        }'" class="btn blue waves-effect waves-light left" type="button">Detail</button>
          `;
      }
    }
  ]
});

savePresensi = () => {
  const checkBox = $('.cb-presensi');
  const presensi = Array();
  const id_user = $('#idGuru').text();
  const id_kelas = $('#idKelas').text();
  for (let index = 0; index < checkBox.length; index++) {
    const tmp = {};

    if ($('.cb-presensi')[index].checked === true) {
      let id = JSON.parse($('.cb-presensi')[index].id);
      tmp.id = id[1];
      tmp.status = id[0];
      tmp.checked = true;

      presensi.push(tmp);
    }
  }

  const body = { presensi, id_user, id_kelas };
  let sentData = new FormData();
  sentData.append('body', JSON.stringify(body));

  try {
    $.ajax({
      type: 'POST',
      url: './config/commit/commit.php?name=presensi',
      data: sentData,
      contentType: false,
      processData: false,
      cache: false,
      async: false,
      success: data => {
        $('#btn-presensi').prop('disabled', true);
        M.toast({
          html: `Nilai Presensi berhasil disimpan`,
          completeCallback: () => {
            window.location = 'main?module=presensi';
          },
          classes: 'rounded'
        });
      },
      error: error => {
        console.log('Error', error);
      }
    });
  } catch (error) {
    console.log('Error =>', error);
  }
};
