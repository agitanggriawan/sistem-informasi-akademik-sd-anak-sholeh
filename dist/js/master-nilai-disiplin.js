$(document).ready(function() {
  $('#filterNilaiKarakter').change(() => {
    const month = $('#filterNilaiKarakter').val();
    const format = moment(month, 'MM').format('MMMM');
    $('.bulan').text(format);
    window.location.replace(
      `main?module=master-penilaian-disiplin&month=${month}`
    );
  });

  $('#filterNilaiTanggungJawab').change(() => {
    const month = $('#filterNilaiTanggungJawab').val();
    const format = moment(month, 'MM').format('MMMM');
    $('.bulan').text(format);
    window.location.replace(
      `main?module=master-penilaian-tanggung-jawab&month=${month}`
    );
  });
});

/* $('#master-nilai-disiplins').dataTable({
  processing: true,
  ajax: {
    url: './config/commit/query.php?name=get-nilai-disiplin',
    type: 'GET',
    dataSrc: ''
  },
  columns: [
    { data: 'no' },
    { data: 'tanggal' },
    {
      mData: 'nilai',
      mRender: (data, type, row) => {
        return `
        <tr><td>1</td></tr>
        <td>1</td>
        <td>1</td>
        <td>1</td>
        `;
      }
    }
  ]
}); */
