$(document).ready(function() {
  $('#validateKelas').validate({
    rules: {
      jenisKelas: {
        required: true
      },
      tingkatKelas: {
        required: true
      }
    },
    messages: {
      jenisKelas: {
        required: 'Masukkan jenis kelas'
      },
      tingkatKelas: {
        required: 'Masukkan tingkat kelas'
      }
    },
    errorElement: 'div',
    errorPlacement: function(error, element) {
      var placement = $(element).data('error');
      if (placement) {
        $(placement).append(error);
      } else {
        error.insertAfter(element);
      }
    },
    submitHandler: function(form, e) {
      e.preventDefault();

      const tingkatKelas = $('#tingkat-kelas')
        .children('option:selected')
        .val();
      const jenisKelas = $('#jenis-kelas')
        .children('option:selected')
        .val();

      const body = {
        kelas: `${tingkatKelas}${jenisKelas}`
      };

      $.ajax({
        url: './config/commit/commit.php?name=post-kelas',
        method: 'POST',
        data: body,
        success: data => {
          const decode = JSON.parse(data);
          if (decode.code === 'OK') {
            $('.btn-simpan').prop('disabled', true);
            M.toast({
              html: `Data Kelas berhasil disimpan`,
              completeCallback: () => {
                $('#validateKelas').trigger('reset');
                setTimeout(function() {
                  $('#tingkat-kelas').val('0');
                  $('#jenis-kelas').val('0');
                  $('#table-get-kelas')
                    .DataTable()
                    .ajax.reload(null, false);
                }, 1000);
                $('.btn-simpan').prop('disabled', false);
              },
              classes: 'rounded'
            });
          } else {
            M.toast({
              html: `Terjadi kesalahan saat menyimpan`,
              classes: 'rounded'
            });
            $('.btn-simpan').prop('disabled', false);
            console.log('-->', decode.msg);
          }
        },
        error: error => {
          console.log('Error', error);
          $('.btn-simpan').prop('disabled', false);
        }
      });
    }
  });
});

$('#table-get-kelas').dataTable({
  processing: true,
  ajax: {
    url: './config/commit/query.php?name=get-kelas',
    type: 'GET',
    dataSrc: ''
  },
  columns: [{ data: 'no' }, { data: 'id' }, { data: 'nama' }]
});
