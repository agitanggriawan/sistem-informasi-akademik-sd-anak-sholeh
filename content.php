<?php
  include 'config/config.php';

  if (isset($_GET['module'])) {
    if ($_SESSION['level'] === base64_encode('Administrator')) {
      if ($_GET['module'] == 'dashboard') {
        include 'layout/dashboard/dashboard.php';
      } else if ($_GET['module'] == 'siswa') {
        include 'layout/siswa/formSiswa.php';
      } else if ($_GET['module'] == 'kelas') {
        include 'layout/kelas/form-kelas.php';
      } else if ($_GET['module'] == 'nilai-disiplin') {
        include 'layout/nilai/nilaiDisiplin.php';
      } else if ($_GET['module'] == 'karakter') {
        include 'layout/master/karakter.php';
      } else if ($_GET['module'] == 'indikator') {
        include 'layout/master/indikator.php';
      } else if ($_GET['module'] == 'tahun-ajaran') {
        include 'layout/master/tahun-ajaran.php';
      } else if ($_GET['module'] == 'logout') {
        include 'config/logout.php';
      } else {
        include '404.php';
      }
    } else if ($_SESSION['level'] === base64_encode('Siswa')) {
      if ($_GET['module'] == 'dashboard') {
        include 'layout/siswa/dashboard.php';
      } else if ($_GET['module'] == 'master-penilaian-disiplin') {
        include 'layout/siswa/master-nilai-disiplin.php';
      } else if ($_GET['module'] == 'master-penilaian-tanggung-jawab') {
        include 'layout/siswa/master-nilai-tanggung-jawab.php';
      } else if ($_GET['module'] == 'master-penilaian-poin') {
        include 'layout/siswa/master-nilai-poin.php';
      } else if ($_GET['module'] == 'logout') {
        include 'config/logout.php';
      } else {
        include '404.php';
      }
    } else if ($_SESSION['level'] === base64_encode('Guru')) {
      if ($_GET['module'] == 'dashboard') {
        include 'layout/dashboard/dashboard-guru.php';
      } else if ($_GET['module'] == 'penilaian-disiplin') {
        include 'layout/nilai/penilaianDisiplin.php';
      } else if ($_GET['module'] == 'nilai-disiplin') {
        include 'layout/nilai/nilaiDisiplin.php';
      } else if ($_GET['module'] == 'penilaian-tanggung-jawab') {
        include 'layout/nilai/penilaianTanggungJawab.php';
      } else if ($_GET['module'] == 'penilaian-poin') {
        include 'layout/nilai/penilaianPoint.php';
      } else if ($_GET['module'] == 'master-penilaian-disiplin') {
        include 'layout/nilai/master-penilaian-disiplin.php';
      } else if ($_GET['module'] == 'master-penilaian-tanggung-jawab') {
        include 'layout/nilai/master-penilaian-tanggung-jawab.php';
      } else if ($_GET['module'] == 'master-penilaian-poin') {
        include 'layout/nilai/master-penilaian-poin.php';
      } else if ($_GET['module'] == 'presensi') {
        include 'layout/presensi/presensi.php';
      } else if ($_GET['module'] == 'master-presensi') {
        include 'layout/presensi/master-presensi.php';
      } else if ($_GET['module'] == 'logout') {
        include 'config/logout.php';
      } else {
        include '404.php';
      }
    } else if ($_SESSION['level'] === base64_encode('Kepala Sekolah')) {
      if ($_GET['module'] == 'dashboard') {
        include 'layout/kepala-sekolah/dashboard.php';
      } else if ($_GET['module'] == 'master-nilai-disiplin') {
        include 'layout/kepala-sekolah/master-nilai-disiplin.php';
      } else if ($_GET['module'] == 'logout') {
        include 'config/logout.php';
      } else {
        include '404.php';
      }
    }
  }
?>