<?php
  session_start();
  if(!isset($_SESSION['id'])) {
    header('location:index');
  } else {
    include 'config/middleware.php';
    $md = new Middleware();
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon.png">
    <title>Sistem Informasi Akademik SD Mutiara Anak Sholeh</title>
    <link href="dist/css/style.css" rel="stylesheet">
    <link href="dist/css/pages/data-table.css" rel="stylesheet">
    <link href="assets/extra-libs/prism/prism.css" rel="stylesheet">
  </head>

  <body>
    <div class="main-wrapper" id="main-wrapper">
      <div class="preloader">
        <div class="loader">
          <div class="loader__figure"></div>
          <p class="loader__label">Menunggu</p>
        </div>
      </div>
      <header class="topbar">
        <nav>
          <div class="nav-wrapper">
            <a href="javascript:void(0)" class="brand-logo">
              <span class="icon">
                <img class="light-logo" src="assets/images/logo-light-icon.png">
                <img class="dark-logo" src="assets/images/logo-icon.png">
              </span>
              <span class="text">
                <img class="light-logo" src="assets/images/logo-light-text.png">
                <img class="dark-logo" src="assets/images/logo-text.png">
              </span>
            </a>
            <ul class="left">
              <li class="hide-on-med-and-down">
                <a href="javascript: void(0);" class="nav-toggle">
                  <span class="bars bar1"></span>
                  <span class="bars bar2"></span>
                  <span class="bars bar3"></span>
                </a>
              </li>
              <li class="hide-on-large-only">
                <a href="javascript: void(0);" class="sidebar-toggle">
                  <span class="bars bar1"></span>
                  <span class="bars bar2"></span>
                  <span class="bars bar3"></span>
                </a>
              </li>
            </ul>
            <!-- ============================================================== -->
            <!-- Right topbar icon scss in header.scss -->
            <!-- ============================================================== -->
          </div>
        </nav>
        <!-- ============================================================== -->
        <!-- Navbar scss in header.scss -->
        <!-- ============================================================== -->
      </header>
      <!-- ============================================================== -->
      <!-- Sidebar scss in sidebar.scss -->
      <!-- ============================================================== -->
      <aside class="left-sidebar">
        <?php
          include 'sidebar.php';
        ?>
      </aside>
      <!-- ============================================================== -->
      <!-- Sidebar scss in sidebar.scss -->
      <!-- ============================================================== -->
      <!-- ============================================================== -->
      <!-- Page wrapper scss in scafholding.scss -->
      <!-- ============================================================== -->
      <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Container fluid scss in scafholding.scss -->
        <!-- ============================================================== -->
        <?php
          include 'content.php';
        ?>
        <!-- ============================================================== -->
        <!-- Container fluid scss in scafholding.scss -->
        <!-- ============================================================== -->
        <!-- <div style="position:fixed; bottom:0; left:50%; margin-left:-200px;"> -->
        <!-- </div> -->
      </div>
      <footer class="center-align m-b-30">All Rights Reserved by Me. Designed and Developed by <a href="#">Achmad Farizal</a>.</footer>
    </div>
    <!-- <script src="assets/libs/jquery/dist/jquery.min.js"></script> -->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="dist/js/materialize.min.js"></script>
    <script src="assets/libs/perfect-scrollbar/dist/js/perfect-scrollbar.jquery.min.js"></script>
    <script src="dist/js/app.js"></script>
    <script src="dist/js/app.init.mini-sidebar.js"></script>
    <script src="dist/js/app-style-switcher.js"></script>
    <script src="dist/js/custom.min.js"></script>
    <script src="assets/extra-libs/DataTables/datatables.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.0/dist/jquery.validate.js"></script>
    <script src="assets/extra-libs/prism/prism.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.js"></script>
    <script src="dist/js/index.js"></script>
    <script src="dist/js/kelas.js"></script>
    <script src="dist/js/karakter.js"></script>
    <script src="dist/js/indikator.js"></script>
    <script src="dist/js/presensi.js"></script>
    <script src="dist/js/nilai.js"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/series-label.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    <script src="https://code.highcharts.com/modules/data.js"></script>
    <script src="https://code.highcharts.com/modules/drilldown.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/lodash@4.17.15/lodash.min.js"></script>
    <script src="dist/js/dashboard-siswa.js"></script>
    <script src="dist/js/tahun-ajaran.js"></script>
    <script src="dist/js/master-nilai-disiplin.js"></script>
    <script src="dist/js/kepala-sekolah.js"></script>
  </body>
</html>

<?php
}
?>