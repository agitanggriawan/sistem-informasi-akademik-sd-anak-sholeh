<ul id="slide-out" class="sidenav">
  <li>
  <ul class="collapsible">
    <?php
    if ($_SESSION['level'] === base64_encode('Administrator')) {
    ?>
    <li class="small-cap"><span class="hide-menu">Home</span></li>
    <li>
      <a href="javascript: void(0);" class="collapsible-header has-arrow"><i class="material-icons">dashboard</i><span class="hide-menu"> Master</span></a>
      <div class="collapsible-body">
        <ul>
          <li><a href="main?module=kelas"><i class="material-icons">chat</i><span class="hide-menu">Kelas</span></a></li>
          <li><a href="main?module=karakter"><i class="material-icons">chat</i><span class="hide-menu">Karakter</span></a></li>
          <li><a href="main?module=indikator"><i class="material-icons">chat</i><span class="hide-menu">Indikator</span></a></li>
          <li><a href="main?module=tahun-ajaran"><i class="material-icons">chat</i><span class="hide-menu">Tahun Ajaran</span></a></li>
          <!-- <li><a href="main?module=nilai-disiplin"><i class="material-icons">chat</i><span class="hide-menu">Nilai Disiplin</span></a></li> -->
        </ul>
      </div>
    </li>
    <?php
    } else if ($_SESSION['level'] === base64_encode('Kepala Sekolah')) {
    ?>
    <li>
      <a href="javascript: void(0);" class="collapsible-header has-arrow"><i class="material-icons">dashboard</i><span class="hide-menu"> Master</span></a>
      <div class="collapsible-body">
        <ul>
          <li><a href="main?module=master-nilai-disiplin"><i class="material-icons">chat</i><span class="hide-menu">Karakter Disiplin</span></a></li>
        </ul>
      </div>
    </li>
    <?php
    } else if ($_SESSION['level'] === base64_encode('Guru')) {
    ?>
    <li class="small-cap"><span class="hide-menu">Menu</span></li>
    <li><a href="main?module=dashboard" class="collapsible-header"><i class="material-icons">dashboard</i><span class="hide-menu">Dashboard</span></a></li>
    <li><a href="main?module=presensi" class="collapsible-header"><i class="material-icons">dashboard</i><span class="hide-menu">Presensi</span></a></li>
    <li>
      <a href="javascript: void(0);" class="collapsible-header has-arrow"><i class="material-icons">dashboard</i><span class="hide-menu"> Penilaian</span></a>
      <div class="collapsible-body">
        <ul>
          <li><a href="main?module=penilaian-disiplin"><i class="material-icons">chat</i><span class="hide-menu">Karakter Disiplin</span></a></li>
          <li><a href="main?module=penilaian-tanggung-jawab"><i class="material-icons">chat</i><span class="hide-menu">Karakter Tanggung Jawab</span></a></li>
          <li><a href="main?module=penilaian-poin"><i class="material-icons">chat</i><span class="hide-menu">Poin</span></a></li>
        </ul>
      </div>
    </li>
    <li>
      <a href="javascript: void(0);" class="collapsible-header has-arrow"><i class="material-icons">dashboard</i><span class="hide-menu"> Master</span></a>
      <div class="collapsible-body">
        <ul>
          <li><a href="main?module=master-presensi"><i class="material-icons">chat</i><span class="hide-menu">Presensi Kelas</span></a></li>
          <li><a href="main?module=master-penilaian-disiplin"><i class="material-icons">chat</i><span class="hide-menu">Karakter Disiplin</span></a></li>
          <li><a href="main?module=master-penilaian-tanggung-jawab"><i class="material-icons">chat</i><span class="hide-menu">Karakter Tanggung Jawab</span></a></li>
          <li><a href="main?module=master-penilaian-poin"><i class="material-icons">chat</i><span class="hide-menu">Poin</span></a></li>
        </ul>
      </div>
    </li>
    <?php
    } else if ($_SESSION['level'] === base64_encode('Siswa')) {
    ?>
    <li class="small-cap"><span class="hide-menu">Menu</span></li>
    <li><a href="main?module=dashboard" class="collapsible-header"><i class="material-icons">dashboard</i><span class="hide-menu">Dashboard</span></a></li>
    <li>
      <a href="main?module=master-penilaian-disiplin" class="collapsible-header"><i class="material-icons">notes</i><span class="hide-menu"> Nilai Karakter Disiplin </span></a>
    </li>
    <li>
      <a href="main?module=master-penilaian-tanggung-jawab" class="collapsible-header"><i class="material-icons">notes</i><span class="hide-menu"> Nilai Karakter Tanggung Jawab </span></a>
    </li>
    <li>
      <a href="main?module=master-penilaian-poin" class="collapsible-header"><i class="material-icons">notes</i><span class="hide-menu"> Nilai Poin </span></a>
    </li>
    <?php
    }
    ?>
    <li>
      <a href="main?module=logout" class="collapsible-header"><i class="material-icons">perm_contact_calendar</i><span class="hide-menu"> Logout </span></a>
    </li>
  </ul>
  </li>
</ul>