<?php
  include "../config.php";

  $res = new \stdClass();
  $body = json_decode($_POST['body']);
  $nilaiKarakter = $body->nilaiKarakter;
  $idGuru = $body->idGuru;
  $idKelas = $body->idKelas;
  $idKarakter = $body->idKarakter;
  $idTahunAjaran = $body->idTahunAjaran;

  for ($i=0; $i < count($nilaiKarakter); $i++) {
    $insertTransaksi = mysqli_query($connect, "INSERT INTO tb_nilai_karakter(id_kelas, id_user, id_karakter, nilai, id_tahun_ajaran) VALUES($idKelas, $idGuru, $idKarakter, '" .json_encode([$nilaiKarakter[$i]]). "', $idTahunAjaran)");
  }

  if ($insertTransaksi) {
    $res->code = "OK";
    $res->msg = "Success";
    $res->id = mysqli_insert_id($connect);
  } else {
    $res->code = "FAIL";
    $res->msg = mysqli_error($connect);
  }

  echo json_encode($res);
?>
