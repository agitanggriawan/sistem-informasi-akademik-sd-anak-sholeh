<?php
  include "../config.php";

  $res = new \stdClass();

  if ($_GET['name'] == "nilai-poin") {
    $body = json_decode($_POST['body']);
    $idSiswa = $body->idSiswa;
    $idGuru = $body->idGuru;
    $idTahunAjaran = $body->idTahunAjaran;
    $idKelas = $body->idKelas;
    $nilai1 = $body->nilai1 ? $body->nilai1 : 0;
    $nilai2 = $body->nilai2 ? $body->nilai2 : 0;
    $nilai3 = $body->nilai3 ? $body->nilai3 : 0;

    $insertTransaksi = mysqli_query($connect, "INSERT INTO tb_nilai_point(id_siswa, id_user, id_kelas, nilai1, nilai2, nilai3, id_tahun_ajaran) VALUES($idSiswa, $idGuru, $idKelas, $nilai1, $nilai2, $nilai3, $idTahunAjaran )");

    if ($insertTransaksi) {
      $res->code = "OK";
      $res->msg = "Success";
      $res->id = mysqli_insert_id($connect);
    } else {
      $res->code = "FAIL";
      $res->error = "INSERT INTO tb_nilai_point(id_siswa, id_user, nilai1, nilai2, nilai3, id_tahun_ajaran) VALUES($idSiswa, $idGuru, $nilai1, $nilai2, $nilai3, $idTahunAjaran )";
      $res->msg = mysqli_error($connect);
    }

    echo json_encode($res);
  } else if ($_GET['name'] == "post-kelas") {

    $query = mysqli_query($connect, "INSERT INTO tb_kelas(nama_kelas) VALUES('".$_POST['kelas']."')");

    if ($query) {
      $res->code = "OK";
      $res->msg = "Success";
      $res->id = mysqli_insert_id($connect);
    } else {
      $res->code = "FAIL";
      $res->msg = mysqli_error($connect);
    }

    echo json_encode($res);
  } else if ($_GET['name'] == "post-karakter") {
    $query = mysqli_query($connect, "INSERT INTO tb_m_karakter(nama_karakter) VALUES('".$_POST['namaKarakter']."')");

    if ($query) {
      $res->code = "OK";
      $res->msg = "Success";
      $res->body = '-->'.$body;
      $res->id = mysqli_insert_id($connect);
    } else {
      $res->code = "FAIL";
      $res->body = '-->'.$body;
      $res->msg = mysqli_error($connect);
    }

    echo json_encode($res);

  } else if ($_GET['name'] == "post-indikator") {
    $query = mysqli_query($connect, "INSERT INTO tb_m_indikator(id_karakter, nama_indikator) VALUES(".$_POST['idKarakter'].", '".$_POST['namaIndikator']."')");

    if ($query) {
      $res->code = "OK";
      $res->msg = "Success";
      $res->id = mysqli_insert_id($connect);
    } else {
      $res->code = "FAIL";
      $res->msg = mysqli_error($connect);
    }

    echo json_encode($res);
  } else if ($_GET['name'] == "presensi") {
    $body = json_decode($_POST['body']);
    $presensi = $body->presensi;
    $id_user = $body->id_user;
    $id_kelas = $body->id_kelas;
    $date = date("Y-m-d");

    for ($i=0; $i < count($presensi); $i++) {
      $insertTransaksi = mysqli_query($connect, "INSERT INTO tb_presensi(id_user, id_kelas, value, tgl) VALUES($id_user, $id_kelas, '".json_encode($presensi[$i])."', '$date')");
    }

    if ($insertTransaksi) {
      $res->code = "OK";
      $res->msg = "Success";
    } else {
      $res->code = "FAIL";
      $res->msg = mysqli_error($connect);
    }
    echo json_encode($res);
  } else if ($_GET['name'] == "post-periode") {
    $query = mysqli_query($connect, "INSERT INTO tb_tahun_ajaran(nama, periode_awal, periode_akhir, aktif) VALUES('".$_POST['nama']."', '".$_POST['awal']."', '".$_POST['akhir']."', false)");

    if ($query) {
      $res->code = "OK";
      $res->msg = "Success";
    } else {
      $res->code = "FAIL";
      $res->msg = mysqli_error($connect);
    }

    echo json_encode($res);

  } else if ($_GET['name'] == "set-aktif-periode") {
    $query = mysqli_query($connect, "UPDATE tb_tahun_ajaran SET aktif = false");
    $query = mysqli_query($connect, "UPDATE tb_tahun_ajaran SET aktif = true WHERE id = '".$_POST['id']."'");

    if ($query) {
      $res->code = "OK";
      $res->msg = "Success";
    } else {
      $res->code = "FAIL";
      $res->msg = mysqli_error($connect);
    }

    echo json_encode($res);

  }
?>
