<?php
  session_start();
  include "../config.php";

  if ($_GET['name'] == "nilai-point") {
    $query = mysqli_query($connect, "SELECT * FROM tb_siswa ORDER BY id_kelas ASC");
    $arr = [];
    $i = 0;
    $date = date("Y-m-d");

    while($data = mysqli_fetch_array($query)){
      $check = mysqli_query($connect, "SELECT id_siswa FROM tb_nilai_point WHERE id_siswa = $data[0] AND DATE(create_at) = ('$date')");
      if (!mysqli_num_rows($check)) {
        $arr[$i]['no'] = $i + 1;
        $arr[$i]['nis'] = $data[1];
        $arr[$i]['nama'] = $data[3];
        $arr[$i]['kelas'] = $data[2];
        $arr[$i]['nilai1'] = '<p><label><input type="checkbox" class="nilai_'.$data[1].'" id="nilai1_'.$data[1].'" class="filled-in"/><span></span></label></p>';
        $arr[$i]['nilai2'] = '<p><label><input type="checkbox" class="nilai_'.$data[1].'" id="nilai2_'.$data[1].'" class="filled-in"/><span></span></label></p>';
        $arr[$i]['nilai3'] = '<p><label><input type="checkbox" class="nilai_'.$data[1].'" id="nilai3_'.$data[1].'" class="filled-in"/><span></span></label></p>';
        $arr[$i]['aksi'] = $data[1];
        $arr[$i]['id'] = $data[0];
        $i++;
      }
    }
    header('Content-Type: application/json');
    echo json_encode($arr);
  } else if ($_GET['name'] == "get-siswa") {
		$query = mysqli_query($connect, "SELECT * FROM tb_siswa WHERE id_kelas = ".base64_decode($_SESSION['kelas'])." ORDER BY 1 DESC");
		$arr = [];
		$i = 0;
		while($data = mysqli_fetch_array($query)){
			$arr[$i]['no'] = $i + 1 . '.';
			$arr[$i]['no_induk'] = $data[1];
			$arr[$i]['nama'] = $data[3];
			$i++;
		}
		header('Content-Type: application/json');
		echo json_encode($arr);
	} else if ($_GET['name'] == "get-kelas") {
		$query = mysqli_query($connect, "SELECT * FROM tb_kelas");
		$arr = [];
		$i = 0;
		while($data = mysqli_fetch_array($query)){
			$arr[$i]['no'] = $i + 1 . '.';
			$arr[$i]['id'] = $data[0];
			$arr[$i]['nama'] = $data[1];
			$i++;
		}
		header('Content-Type: application/json');
		echo json_encode($arr);
	} else if ($_GET['name'] == "get-karakter") {
		$query = mysqli_query($connect, "SELECT * FROM tb_m_karakter");
		$arr = [];
		$i = 0;
		while($data = mysqli_fetch_array($query)){
			$arr[$i]['no'] = $i + 1 . '.';
			$arr[$i]['id'] = $data[0];
			$arr[$i]['nama'] = $data[1];
			$i++;
		}
		header('Content-Type: application/json');
		echo json_encode($arr);
	} else if ($_GET['name'] == "get-indikator") {
		$query = mysqli_query($connect, "SELECT * FROM tb_m_karakter k JOIN tb_m_indikator i ON k.id_karakter=i.id_karakter ORDER BY 1 ASC");
		$arr = [];
		$i = 0;
		while($data = mysqli_fetch_array($query)){
			$arr[$i]['no'] = $i + 1 . '.';
			$arr[$i]['nama_karakter'] = $data[1];
			$arr[$i]['nama_indikator'] = $data[7];
			$i++;
		}
		header('Content-Type: application/json');
		echo json_encode($arr);
	} else if ($_GET['name'] == "nilai-disiplin-by-kelas") {
		$id_tahun_ajaran = base64_decode($_SESSION['tahunAjaran']);
		if (isset($_GET['month'])) {
			$month = $_GET['month'];
			$query = mysqli_query($connect, "SELECT DISTINCT(DATE(n.create_at)) as tanggal, k.nama_kelas  FROM tb_nilai_karakter n JOIN tb_kelas k ON k.id_kelas = n.id_kelas WHERE MONTH(n.create_at) = $month AND YEAR(n.create_at) = YEAR(CURRENT_DATE()) AND n.id_kelas = ".base64_decode($_SESSION['kelas'])." AND n.id_karakter = 1 AND n.id_tahun_ajaran = $id_tahun_ajaran ORDER BY tanggal ASC");
		} else {
			$query = mysqli_query($connect, "SELECT DISTINCT(DATE(n.create_at)) as tanggal, k.nama_kelas  FROM tb_nilai_karakter n JOIN tb_kelas k ON k.id_kelas = n.id_kelas WHERE MONTH(n.create_at) = MONTH(CURRENT_DATE()) AND YEAR(n.create_at) = YEAR(CURRENT_DATE()) AND n.id_kelas = ".base64_decode($_SESSION['kelas'])." AND n.id_karakter = 1 AND n.id_tahun_ajaran = $id_tahun_ajaran ORDER BY tanggal ASC");
		}
		$arr = [];
		$i = 0;
		while($data = mysqli_fetch_array($query)){
			$arr[$i]['no'] = $i + 1 . '.';
			$arr[$i]['tanggal'] = $data[0];
			$arr[$i]['nama_kelas'] = $data[1];
			$arr[$i]['aksi'] = [base64_decode($_SESSION['kelas']), $data[0], 1];
			$i++;
		}
		header('Content-Type: application/json');
		echo json_encode($arr);
	} else if ($_GET['name'] == "nilai-disiplin-by-date") {
		$kelas = $_GET['k'];
		$date = $_GET['d'];
		$karakter = $_GET['n'];

		$query = mysqli_query($connect, "SELECT id, nilai FROM tb_nilai_karakter WHERE id_kelas = $kelas AND id_karakter = $karakter AND DATE(create_at) = '$date'");
		$arr = [];
		$i = 0;
		$total = 0;
		while($data = mysqli_fetch_array($query)){
			$arr[$i]['no'] = $i + 1 . '.';
			foreach (json_decode($data['nilai']) as $el) {
				echo '
				<tr>
					<td>'.$i.'</td>
					<td>'.$el->id_siswa.'</td>
				';
				foreach($el->nilai as $elChild) {
					$elChild->nilai === 'Ya' ? $total = $total + 1 : null;
					echo '<td>'. $elChild->nilai . '</td>';
				}
				echo '<td>'. $total .'</td>';
				echo '
				</tr>
				';
				$i++;
			}
			$i++;
		}
		header('Content-Type: application/json');
		echo json_encode($arr);
	} else if ($_GET['name'] == "nilai-disiplin-by-siswa") {
		$id_tahun_ajaran = base64_decode($_SESSION['tahunAjaran']);
		$query = mysqli_query($connect, "SELECT nomor_induk, nama_siswa FROM tb_siswa WHERE id_kelas = ".base64_decode($_SESSION['kelas'])." ");

		$arr = [];
		$i = 0;
		while($data = mysqli_fetch_array($query)){
			$total = 0;
			$arr[$i]['no'] = $i + 1 . '.';
			$arr[$i]['nis'] = $data[0];
			$arr[$i]['nama'] = $data[1];

			$qNilai = mysqli_query($connect, "SELECT nilai from tb_nilai_karakter WHERE JSON_CONTAINS(nilai, '{\"id_siswa\": \"$data[1]\"}') AND MONTH(create_at) = MONTH(CURRENT_DATE()) AND YEAR(create_at) = YEAR(CURRENT_DATE()) AND id_kelas = ".base64_decode($_SESSION["kelas"])." AND id_karakter = 1 AND id_tahun_ajaran = $id_tahun_ajaran");

			while($result = mysqli_fetch_array($qNilai)){
				foreach (json_decode($result['nilai']) as $el) {
					foreach($el->nilai as $elChild) {
						$elChild->nilai === 'Ya' ? $total = $total + 1 : null;
					}
				}
			}

			$arr[$i]['total'] = $total;
			$i++;
		}
		header('Content-Type: application/json');
		echo json_encode($arr);
	} else if ($_GET['name'] == "get-current-presence") {
		$id_user = base64_decode($_SESSION['id']);
		$id_kelas = base64_decode($_SESSION['kelas']);
		$date = date("Y-m-d");

		$query = mysqli_query($connect, "SELECT nomor_induk, nama_siswa FROM tb_siswa WHERE id_kelas = $id_kelas ");

		$arr = [];
		$i = 0;
		while($data = mysqli_fetch_array($query)){
			$total = 0;
			$arr[$i]['no'] = $i + 1 . '.';
			$arr[$i]['nis'] = $data[0];
			$arr[$i]['nama'] = $data[1];

			$qPresence = mysqli_query($connect, "SELECT value from tb_presensi WHERE JSON_CONTAINS(value, '{\"id\": \"$data[0]\"}') AND DATE(tgl) = ('$date')");
			$result = mysqli_fetch_array($qPresence);

			$arr[$i]['status'] = $result[0];

			$i++;
		}
		header('Content-Type: application/json');
		echo json_encode($arr);
	} else if ($_GET['name'] == "get-date-presence") {
		$query = mysqli_query($connect, "SELECT DISTINCT(DATE(n.tgl)) as tanggal, k.nama_kelas  FROM tb_presensi n JOIN tb_kelas k ON k.id_kelas = n.id_kelas WHERE MONTH(n.tgl) = MONTH(CURRENT_DATE()) AND YEAR(n.tgl) = YEAR(CURRENT_DATE()) AND n.id_kelas = ".base64_decode($_SESSION['kelas'])."  ORDER BY tanggal ASC");
		$arr = [];
		$i = 0;
		while($data = mysqli_fetch_array($query)){
			$arr[$i]['no'] = $i + 1 . '.';
			$arr[$i]['tanggal'] = $data[0];
			$arr[$i]['nama_kelas'] = $data[1];
			$arr[$i]['aksi'] = [base64_decode($_SESSION['kelas']), $data[0], 1];
			$i++;
		}
		header('Content-Type: application/json');
		echo json_encode($arr);
	} else if ($_GET['name'] == "get-current-presence-detail") {
		$date = $_GET['d'];
		$id_kelas = base64_decode($_SESSION['kelas']);

		$query = mysqli_query($connect, "SELECT nomor_induk, nama_siswa FROM tb_siswa WHERE id_kelas = $id_kelas ");

		$arr = [];
		$i = 0;
		while($data = mysqli_fetch_array($query)){
			$total = 0;
			$arr[$i]['no'] = $i + 1 . '.';
			$arr[$i]['nis'] = $data[0];
			$arr[$i]['nama'] = $data[1];

			$qPresence = mysqli_query($connect, "SELECT value from tb_presensi WHERE JSON_CONTAINS(value, '{\"id\": \"$data[0]\"}') AND DATE(tgl) = ('$date')");
			$result = mysqli_fetch_array($qPresence);

			$arr[$i]['status'] = $result[0];

			$i++;
		}
		header('Content-Type: application/json');
		echo json_encode($arr);
	} else if ($_GET['name'] == "nilai-poin-by-siswa") {
		$id_siswa = base64_decode($_SESSION['id']);
		$id_tahun_ajaran = base64_decode($_SESSION['tahunAjaran']);

		$query = mysqli_query($connect, "SELECT u.nama_user, n.nilai1, n.nilai2, n.nilai3, n.create_at FROM tb_nilai_point n JOIN tb_user u ON u.id_user = n.id_user WHERE n.id_siswa = $id_siswa AND id_tahun_ajaran = $id_tahun_ajaran");

		$arr = [];
		$i = 0;
		while($data = mysqli_fetch_array($query)){
			$total = 0;
			$arr[$i]['no'] = $i + 1 . '.';
			$arr[$i]['tgl'] = $data[4];
			$arr[$i]['nama'] = $data[0];
			$arr[$i]['nilai1'] = $data[1];
			$arr[$i]['nilai2'] = $data[2];
			$arr[$i]['nilai3'] = $data[3];
			$i++;
		}
		header('Content-Type: application/json');
		echo json_encode($arr);
	} else if ($_GET['name'] == "get-nilai-karakter") {
		$id_siswa = base64_decode($_SESSION['id']);
		$nama = base64_decode($_SESSION['nama']);
		$id_tahun_ajaran = base64_decode($_SESSION['tahunAjaran']);
		$query = mysqli_query($connect, "SELECT nilai, create_at, id_karakter FROM tb_nilai_karakter WHERE JSON_CONTAINS(nilai, '{\"id_siswa\": \"$nama\"}') AND id_tahun_ajaran = $id_tahun_ajaran AND MONTH(create_at) = MONTH(CURRENT_DATE()) ORDER BY 2 ASC");
		$arr = [];
		$i = 0;
		while($data = mysqli_fetch_array($query)){
			$arr[$i]['nilai'] = @end(json_decode($data[0]));
			$arr[$i]['karakter'] = $data[2];
			$arr[$i]['date'] = $data[1];
			$i++;
		}
		header('Content-Type: application/json');
		echo json_encode($arr);
	} else if ($_GET['name'] == "get-tahun-ajaran") {
		$query = mysqli_query($connect, "SELECT * FROM tb_tahun_ajaran ORDER BY 5 DESC");
		$arr = [];
		$i = 0;
		while($data = mysqli_fetch_array($query)){
			$arr[$i]['no'] = $i + 1 . '.';
			$arr[$i]['nama'] = $data[1];
			$arr[$i]['periode_awal'] = $data[2];
			$arr[$i]['periode_akhir'] = $data[3];
			$arr[$i]['status'] = $data[4] == 1 ? 'Aktif' : 'Tidak Aktif';
			$arr[$i]['aksi'] = $data[0];
			$i++;
		}
		header('Content-Type: application/json');
		echo json_encode($arr);
	} else if ($_GET['name'] == "nilai-tanggung-jawab-by-kelas") {
		if (isset($_GET['month'])) {
			$month = $_GET['month'];
			$query = mysqli_query($connect, "SELECT DISTINCT(DATE(n.create_at)) as tanggal, k.nama_kelas  FROM tb_nilai_karakter n JOIN tb_kelas k ON k.id_kelas = n.id_kelas WHERE MONTH(n.create_at) = $month AND YEAR(n.create_at) = YEAR(CURRENT_DATE()) AND n.id_kelas = ".base64_decode($_SESSION['kelas'])." AND n.id_karakter = 2 AND n.id_tahun_ajaran = ".base64_decode($_SESSION['tahunAjaran'])." ORDER BY tanggal ASC");
		} else {
			$query = mysqli_query($connect, "SELECT DISTINCT(DATE(n.create_at)) as tanggal, k.nama_kelas  FROM tb_nilai_karakter n JOIN tb_kelas k ON k.id_kelas = n.id_kelas WHERE MONTH(n.create_at) = MONTH(CURRENT_DATE()) AND YEAR(n.create_at) = YEAR(CURRENT_DATE()) AND n.id_kelas = ".base64_decode($_SESSION['kelas'])." AND n.id_karakter = 2 AND n.id_tahun_ajaran = ".base64_decode($_SESSION['tahunAjaran'])." ORDER BY tanggal ASC");
		}
		$arr = [];
		$i = 0;
		while($data = mysqli_fetch_array($query)){
			$arr[$i]['no'] = $i + 1 . '.';
			$arr[$i]['tanggal'] = $data[0];
			$arr[$i]['nama_kelas'] = $data[1];
			$arr[$i]['aksi'] = [base64_decode($_SESSION['kelas']), $data[0], 2];
			$i++;
		}
		header('Content-Type: application/json');
		echo json_encode($arr);
	} else if ($_GET['name'] == "nilai-tanggung-jawab-by-siswa") {
		$id_tahun_ajaran = base64_decode($_SESSION['tahunAjaran']);
		$query = mysqli_query($connect, "SELECT nomor_induk, nama_siswa FROM tb_siswa WHERE id_kelas = ".base64_decode($_SESSION['kelas'])." ");

		$arr = [];
		$i = 0;
		while($data = mysqli_fetch_array($query)){
			$total = 0;
			$arr[$i]['no'] = $i + 1 . '.';
			$arr[$i]['nis'] = $data[0];
			$arr[$i]['nama'] = $data[1];

			$qNilai = mysqli_query($connect, "SELECT nilai from tb_nilai_karakter WHERE JSON_CONTAINS(nilai, '{\"id_siswa\": \"$data[1]\"}') AND MONTH(create_at) = MONTH(CURRENT_DATE()) AND YEAR(create_at) = YEAR(CURRENT_DATE()) AND id_kelas = ".base64_decode($_SESSION["kelas"])." AND id_karakter = 2 AND id_tahun_ajaran = $id_tahun_ajaran");

			while($result = mysqli_fetch_array($qNilai)){
				foreach (json_decode($result['nilai']) as $el) {
					foreach($el->nilai as $elChild) {
						$elChild->nilai === 'Ya' ? $total = $total + 1 : null;
					}
				}
			}

			$arr[$i]['total'] = $total;
			$i++;
		}
		header('Content-Type: application/json');
		echo json_encode($arr);
	} else if ($_GET['name'] == "get-nilai-poin") {
		$id_siswa = base64_decode($_SESSION['id']);
		$id_tahun_ajaran = base64_decode($_SESSION['tahunAjaran']);

		$query = mysqli_query($connect, "SELECT nilai1, nilai2, nilai3 FROM tb_nilai_point WHERE id_siswa = $id_siswa");
		$arr = [];
		$i = 0;
		while($data = mysqli_fetch_array($query)){
			$arr[$i]['nilai1'] = $data[0];
			$arr[$i]['nilai2'] = $data[1];
			$arr[$i]['nilai3'] = $data[2];
			$i++;
		}
		header('Content-Type: application/json');
		echo json_encode($arr);
	} else if ($_GET['name'] == "get-nilai-disiplin") {
		$kelas = base64_decode($_SESSION['kelas']);
		$nama = base64_decode($_SESSION['nama']);
		$id_tahun_ajaran = base64_decode($_SESSION['tahunAjaran']);

		$dataKarakter = mysqli_query($connect, "SELECT nilai, create_at FROM tb_nilai_karakter  WHERE id_kelas = $kelas AND id_karakter = 1 AND JSON_CONTAINS(nilai, '{\"id_siswa\": \"$nama\"}') AND id_tahun_ajaran = $id_tahun_ajaran AND MONTH(create_at) = MONTH(CURRENT_DATE()) ORDER BY 2 ASC");

		$arr = [];
		$i = 0;
		$grandTotal = 0;
		while($result = mysqli_fetch_array($dataKarakter)){
			$total = 0;
			$date = strtotime($result['create_at']);
			$current_date = date( 'd - m - Y', $date );

			$arr[$i]['no'] = $i + 1 . '.';
			$arr[$i]['tanggal'] = $current_date;
			$arr[$i]['nilai'] = json_decode($result['nilai']);

			$i++;
		}

		header('Content-Type: application/json');
		echo json_encode($arr);
	} else if ($_GET['name'] == "detail-nilai-karakter") {
		$nama = $_POST['nama'];
		$karakter = $_POST['karakter'];
		$id_tahun_ajaran = base64_decode($_SESSION['tahunAjaran']);
		$query = mysqli_query($connect, "SELECT nilai, create_at FROM tb_nilai_karakter WHERE id_karakter = $karakter AND JSON_CONTAINS(nilai, '{\"id_siswa\": \"$nama\"}') AND id_tahun_ajaran = $id_tahun_ajaran AND MONTH(create_at) = MONTH(CURRENT_DATE()) AND YEAR(create_at) = YEAR(CURRENT_DATE()) ORDER BY 2 ASC");
		$arr = [];
		$i = 0;
		while($data = mysqli_fetch_array($query)){
			$arr[$i]['nilai'] = @end(json_decode($data[0]));
			$arr[$i]['date'] = $data[1];
			$i++;
		}
		header('Content-Type: application/json');
		echo json_encode($arr);
	} else if ($_GET['name'] == "nilai-poin-by-guru") {
		$id_kelas = base64_decode($_SESSION['kelas']);
		$id_tahun_ajaran = base64_decode($_SESSION['tahunAjaran']);

		$querySiswa = mysqli_query($connect, "SELECT id_siswa, nama_siswa from tb_siswa where id_kelas = $id_kelas");
		$arr = [];
			$i = 0;
		while($dataSiswa = mysqli_fetch_array($querySiswa)){
			$query = mysqli_query($connect, "SELECT s.nama_siswa, SUM(n.nilai1), SUM(n.nilai2), SUM(n.nilai3), s.id_siswa, s.nomor_induk FROM tb_nilai_point n JOIN tb_siswa s ON s.id_siswa = n.id_siswa WHERE n.id_kelas = $id_kelas AND id_tahun_ajaran = $id_tahun_ajaran AND n.id_siswa = $dataSiswa[0]");
			$n1 = 0;
			$n2 = 0;
			$n3 = 0;
			while($data = mysqli_fetch_array($query)){
				$n1 += $data[1];
				$n2 += $data[2];
				$n3 += $data[3];
				$arr[$i]['no'] = $i + 1 . '.';
				$arr[$i]['id'] = $data[4];
				$arr[$i]['nama'] = $data[0];
				$arr[$i]['nis'] = $data[5];
				$arr[$i]['nilai1'] = $data[1] ? $n1 : 0;
				$arr[$i]['nilai2'] = $data[2] ? $n2 : 0;
				$arr[$i]['nilai3'] = $data[3] ? $n3 : 0;
				$i++;
			}
		}
		header('Content-Type: application/json');
		echo json_encode($arr);
	} else if ($_GET['name'] == "nilai-disiplin-kepsek") {
		$id_tahun_ajaran = base64_decode($_SESSION['tahunAjaran']);
		$id_kelas = $_POST['id_kelas'];
		$query = mysqli_query($connect, "SELECT nomor_induk, nama_siswa FROM tb_siswa WHERE id_kelas = $id_kelas ");

		$arr = [];
		$i = 0;
		while($data = mysqli_fetch_array($query)){
			$total = 0;
			$arr[$i]['no'] = $i + 1 . '.';
			$arr[$i]['nis'] = $data[0];
			$arr[$i]['nama'] = $data[1];

			$qNilai = mysqli_query($connect, "SELECT nilai from tb_nilai_karakter WHERE JSON_CONTAINS(nilai, '{\"id_siswa\": \"$data[1]\"}') AND MONTH(create_at) = MONTH(CURRENT_DATE()) AND YEAR(create_at) = YEAR(CURRENT_DATE()) AND id_kelas = $id_kelas AND id_karakter = 1 AND id_tahun_ajaran = $id_tahun_ajaran");

			while($result = mysqli_fetch_array($qNilai)){
				foreach (json_decode($result['nilai']) as $el) {
					foreach($el->nilai as $elChild) {
						$elChild->nilai === 'Ya' ? $total = $total + 1 : null;
					}
				}
			}

			$arr[$i]['total'] = $total;
			$i++;
		}
		header('Content-Type: application/json');
		echo json_encode($arr);
	} else if ($_GET['name'] == "get-nilai-karakter-kepala-sekolah") {
		$id_tahun_ajaran = base64_decode($_SESSION['tahunAjaran']);
		$arr = [];
		$arrA = [];
		$i = 0;
		$querya = mysqli_query($connect, "SELECT n.id_kelas, k.nama_kelas FROM tb_nilai_karakter n JOIN tb_kelas k ON k.id_kelas=n.id_kelas group by 1");
		while($qKelas = mysqli_fetch_array($querya)) {
			// $arrA.push($qKelas['nama_kelas']);
			array_push($arrA, $qKelas['nama_kelas']);
			$query = mysqli_query($connect, "SELECT nomor_induk, nama_siswa FROM tb_siswa WHERE id_kelas = $qKelas[0] ");

			while($data = mysqli_fetch_array($query)){
				$totalDisiplin = 0; // ID Karakter 1
				$totalTanggungJawab = 0; // ID Karakter 2
				$arr[$i]['no'] = $i + 1 . '.';
				$arr[$i]['nis'] = $data[0];
				$arr[$i]['kelas'] = $qKelas[1];
				$arr[$i]['nama'] = $data[1];

				$qNilai = mysqli_query($connect, "SELECT nilai, id_karakter from tb_nilai_karakter WHERE JSON_CONTAINS(nilai, '{\"id_siswa\": \"$data[1]\"}') AND id_kelas = $qKelas[0] AND id_tahun_ajaran = $id_tahun_ajaran");

				while($result = mysqli_fetch_array($qNilai)){
					foreach (json_decode($result['nilai']) as $el) {
						foreach($el->nilai as $elChild) {
							if ($result['id_karakter'] == 1) {
								$elChild->nilai === 'Ya' ? $totalDisiplin = $totalDisiplin + 1 : null;
							} else if ($result['id_karakter'] == 2) {
								$elChild->nilai === 'Ya' ? $totalTanggungJawab = $totalTanggungJawab + 1 : null;
							}
						}
					}
				}

				$arr[$i]['total_disiplin'] = $totalDisiplin ? $totalDisiplin : 0;
				$arr[$i]['total_tanggung_jawab'] = $totalTanggungJawab ? $totalTanggungJawab : 0;
				$i++;
			}
		}
		$combinedArray = [];
		$combinedArray[0] = $arr;
		$combinedArray[1] = $arrA;
		header('Content-Type: application/json');
		echo json_encode($combinedArray);
	}
?>
