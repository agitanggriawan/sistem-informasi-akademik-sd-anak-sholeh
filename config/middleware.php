<?php
  class Middleware {
    function __contruct() {
      session_start();
    }

    function __destruct() {

    }

    function namaKelas($idKelas) {
      global $connect;

      $query = mysqli_query($connect, "SELECT nama_kelas FROM tb_kelas WHERE id_kelas = $idKelas");
      $res = mysqli_fetch_array($query);

      return $res[0];
    }

    function isInput($idGuru, $idKarakter) {
      global $connect;
      $date = date("Y-m-d");

      $query = mysqli_query($connect, "SELECT id_nilai FROM tb_nilai_karakter WHERE id_user = $idGuru AND id_karakter = $idKarakter AND DATE(create_at) = ('$date')");
      if (mysqli_num_rows($query) > 0) {
        return false;
      }
      return true;
    }

    function siswaKelas($idKelas) {
      global $connect;

      $query = mysqli_query($connect, "SELECT nama_siswa from tb_siswa WHERE id_kelas = $idKelas ORDER BY id_siswa ASC");
      $data = array();
      while($res = mysqli_fetch_array($query)) {
        array_push($data, $res['nama_siswa']);
      }

      return $data;
    }

    function jumlahSiswa($idKelas) {
      global $connect;

      $query = mysqli_query($connect, "SELECT COUNT(id_kelas) from tb_siswa WHERE id_kelas = $idKelas");
      $res = mysqli_fetch_array($query);

      return $res[0];
    }

    function currentNilai($idKelas, $idGuru, $idKarakter) {
      global $connect;
      $date = date("Y-m-d");

      $query = mysqli_query($connect, "SELECT nilai FROM tb_nilai_karakter WHERE id_user = $idGuru AND id_karakter = $idKarakter AND DATE(create_at) = ('$date')");

      return $query;
    }

    function myProfile($idGuru) {
      global $connect;

      $query = mysqli_query($connect, "SELECT u.*, k.* FROM tb_user u JOIN tb_kelas k ON k.id_kelas=u.kelas WHERE u.id_user = $idGuru");
      $res = mysqli_fetch_array($query);

      return $res;
    }

    function presence($idGuru) {
      global $connect;
      $date = date("Y-m-d");

      $query = mysqli_query($connect, "SELECT id_presensi FROM tb_nilai_karakter WHERE id_user = $idGuru AND id_karakter = $idKarakter AND DATE(create_at) = ('$date')");
      if (mysqli_num_rows($query) > 0) {
        return false;
      }
      return true;
    }

    function getIndikator($idKarakter) {
      global $connect;
      $value = '';
      $alphabets =  array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
      $index = 0;
      $query = mysqli_query($connect, "SELECT id_indikator, nama_indikator FROM tb_m_indikator WHERE id_karakter = $idKarakter");
      while($k = mysqli_fetch_array($query)){
        $value .= '<td><a class="tooltipped" data-position="top" data-delay="50" data-tooltip="'.$k['nama_indikator'].'">'.$alphabets[$index].'</a></td>';
        $index++;
      }
      return $value;
    }

    function getCountIndikator($idKarakter) {
      global $connect;

      $query = mysqli_query($connect, "SELECT count(id_indikator) FROM tb_m_indikator WHERE id_karakter = $idKarakter");
      $result = mysqli_fetch_array($query);
      return $result[0];
    }

    function getKarakter() {
      global $connect;
      $value = '';

      $query = mysqli_query($connect, "SELECT id_karakter, nama_karakter FROM tb_m_karakter");
      while($k = mysqli_fetch_array($query)){
        $value .= '<option value="'.$k['id_karakter'].'">'.$k['nama_karakter'].'</option>';
      }
      return $value;
    }

    function isPresence($idGuru, $idKelas) {
      global $connect;
      $date = date("Y-m-d");
      $query = mysqli_query($connect, "SELECT id_presensi FROM tb_presensi WHERE id_user = $idGuru AND id_kelas = $idKelas AND DATE(tgl) = ('$date')");
      if (mysqli_num_rows($query) > 0) {
        return false;
      }
      return true;
    }

    function myProfileSiswa($idSiswa) {
      global $connect;

      $query = mysqli_query($connect, "SELECT s.nomor_induk, s.nama_siswa, s.alamat, s.jenis_kelamin, k.nama_kelas FROM tb_siswa s JOIN tb_kelas k ON k.id_kelas = s.id_kelas WHERE s.id_siswa = $idSiswa");
      $res = mysqli_fetch_array($query);

      return $res;
    }

    function getDataNilaiByMonth($kelas, $id_tahun_ajaran, $nama, $id_karakter) {
      global $connect;
      $value = "";

      $query = mysqli_query($connect, "SELECT DISTINCT(MONTH(create_at)) as bulan FROM tb_nilai_karakter  WHERE id_kelas = $kelas AND id_karakter = $id_karakter AND JSON_CONTAINS(nilai, '{\"id_siswa\": \"$nama\"}') AND id_tahun_ajaran = $id_tahun_ajaran");

      while($k = mysqli_fetch_array($query)){
        $value .= '<option value="'.$k['bulan'].'">'.$k['bulan'].'</option>';
      }
      return $value;
    }

    function getMonth($m) {
      $months = array (1=>'Januari',2=>'Februari',3=>'Maret',4=>'April',5=>'Maret',6=>'Juni',7=>'Juli',8=>'Agustus',9=>'September',10=>'Oktober',11=>'November',12=>'Desember');
      return $months[(int)$m];
    }

    function getNilaiByMonth($kelas, $id_tahun_ajaran, $id_karakter) {
      global $connect;
      $value = "";

      $query = mysqli_query($connect, "SELECT DISTINCT(MONTH(create_at)) as bulan FROM tb_nilai_karakter  WHERE id_kelas = $kelas AND id_karakter = $id_karakter AND id_tahun_ajaran = $id_tahun_ajaran");

      while($k = mysqli_fetch_array($query)){
        $value .= '<option value="'.$k['bulan'].'">'.$k['bulan'].'</option>';
      }
      return $value;
    }

    function getClass() {
      global $connect;
      $value = "";

      $query = mysqli_query($connect, "SELECT id_kelas, nama_kelas FROM tb_kelas ORDER BY 1 ASC");

      while($k = mysqli_fetch_array($query)){
        $value .= '<option value="'.$k['id_kelas'].'">'.$k['nama_kelas'].'</option>';
      }
      return $value;
    }
  }
?>
